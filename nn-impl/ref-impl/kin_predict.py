"get predictions for a test set"

import numpy as np
import _pickle as pickle

from math import sqrt
from pybrain.datasets.supervised import SupervisedDataSet as SDS
from sklearn.metrics import mean_squared_error as MSE

test_x_file = '../data/test_x.csv'
test_y_file = '../data/test_y.csv'
model_file = 'train-model.pkl'
output_predictions_file = 'predictions_robot_linear.txt'

# load model

net = pickle.load(open(model_file, 'rb'))
for c in [connection for connections in net.connections.values() for connection in connections]:
    print("{} -> {}".format(c.inmod.name, c.outmod.name))
    print("{}".format(c.params))

# load data

test_x = np.loadtxt(test_x_file, delimiter=',')
test_y = np.loadtxt(test_y_file, delimiter=',')
x_test = test_x[:, 0:-1]
y_test = test_y[:]
y_test = y_test.reshape(-1, 1)

# you'll need labels. In case you don't have them...
#y_test_dummy = np.zeros(y_test.shape)

input_size = x_test.shape[1]
target_size = y_test.shape[1]

assert (net.indim == input_size)
assert (net.outdim == target_size)

# prepare dataset

ds = SDS(input_size, target_size)
ds.setField('input', x_test)
ds.setField('target', y_test)

# predict

p = net.activateOnDataset(ds)

mse = MSE(y_test, p)
rmse = sqrt(mse)

print("testing RMSE:", rmse)

np.savetxt(output_predictions_file, p, fmt='%.6f')
