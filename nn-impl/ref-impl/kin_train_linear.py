"train a regression MLP"

import numpy as np
import _pickle as pickle
from math import sqrt
from pybrain.datasets.supervised import SupervisedDataSet as SDS
#from pybrain.structure.modules.sigmoidlayer import SigmoidLayer
from pybrain.structure.modules.linearlayer import LinearLayer
from pybrain.tools.shortcuts import buildNetwork
from pybrain.supervised.trainers import BackpropTrainer
import random as random
from io import StringIO
import timeit


def main():
    train_x_file = '../data/train_x.csv'
    train_y_file = '../data/train_y.csv'
    validation_file = '../data/validation.csv'
    output_model_file = 'train-model.pkl'

    hidden_size = 100
    epochs = 1
    learningrate = 0.01
    #
    random.seed(1)
    np.random.seed(1)

    # load data
    # np.seterr(all='ignore')

    train_x = np.loadtxt(train_x_file, delimiter=',')
    train_y = np.loadtxt(train_y_file, delimiter=',')

    x_train = train_x[:, 0:-1]
    y_train = train_y[:]
    y_train = y_train.reshape(-1, 1)

    input_size = x_train.shape[1]
    target_size = y_train.shape[1]

    # prepare dataset

    ds = SDS(input_size, target_size)
    ds.setField('input', x_train)
    ds.setField('target', y_train)

    # init and train

    net = buildNetwork(input_size, hidden_size, target_size, outclass=LinearLayer, bias=True)

    with open('../data/weights.txt', 'w') as f:
        for c in [connection for connections in net.connections.values() for connection in connections]:
            f.write("{} => {}\n".format(c.inmod.name, c.outmod.name))
            for index in range(len(c.params)):
                f.write("{} => {}\n".format(c.whichBuffers(index), c.params[index]))
    f.close()

    trainer = BackpropTrainer(net, ds, learningrate)

    print("training for {} epochs...".format(epochs))
    #
    for i in range(epochs):
        mse = trainer.train()
        rmse = sqrt(mse)
        print("training RMSE, epoch {}: {}".format(i + 1, rmse))

    with open('../data/weights-debug.txt', 'w') as f:
        for c in [connection for connections in net.connections.values() for connection in connections]:
            f.write("{} => {}\n".format(c.inmod.name, c.outmod.name))
            for index in range(len(c.params)):
                f.write("{} => {}\n".format(c.whichBuffers(index), c.params[index]))
    f.close()

    pickle.dump(net, open(output_model_file, 'wb'))


exec_time = '{:.2f}s'.format(timeit.timeit("main()", setup="from __main__ import main", number=1))
print(exec_time)
