import sys

weight_bias_hid = 'bias/weight_bias_hid'
weight_bias_out = 'bias/weight_bias_out'
weight_hid_out = 'bias/weight_hid_out'
weight_in_hid = 'bias/weight_in_hid'
robot_in_hid = 'bias/robot_in_hid'
robot_hid_out = 'bias/robot_hid_out'

in_hid = []
bias_hid = []
hid_out = []
bias_out = []
hidden_no = 100
value_0 = 0.0
value_100 = 100.0

with open(weight_bias_hid, 'r') as one:
    for line in one:
        bias_hid.append(line)

with open(weight_bias_out, 'r') as one:
    for line in one:
        bias_out.append(line)

with open(weight_in_hid, 'r') as one:
    count = 0
    num = 8
    i = 0
    for line in one:
        if count % num == 0 and count >= num:
            in_hid.append('(' + str(num) + ', ' + str(i) + ') => ' + bias_hid[i].split(' ')[-1])
            i += 1
            in_hid.append(line)
        else:
            in_hid.append(line)
        count += 1
for i in range(8):
    in_hid.append('(' + str(i) + ', ' + str(hidden_no) + ') => ' + str(value_0) + '\n')
in_hid.append('(' + str(i + 1) + ', ' + str(hidden_no) + ') => ' + str(value_100))

print(in_hid)

with open(weight_hid_out, 'r') as one:
    for line in one:
        hid_out.append(line)
hid_out.append('(' + str(hidden_no) + ', ' + str(0) + ') => ' + bias_out[0].split(' ')[-1])

print(in_hid)
print(hid_out)

with open(robot_in_hid, 'w') as one:
    for val in in_hid:
        one.write(val)

with open(robot_hid_out, 'w') as two:
    for val in hid_out:
        two.write(val)
