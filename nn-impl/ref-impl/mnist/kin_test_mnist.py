"get predictions for a test set"

import numpy as np
import _pickle as pickle

from math import sqrt
from pybrain.datasets.supervised import SupervisedDataSet as SDS
from sklearn.metrics import mean_squared_error as MSE

test_x_file = '../../data/classification/mnist/new-sigma-test-nor-10000.csv'
test_y_file = '../../data/classification/mnist/new-sigma-test-y.csv'
model_file = './train-mnist-model.pkl'
output_predictions_file = '../../data/classification/mnist/predictions-mnist.txt'
predictions_class = '../../data/classification/mnist/predictions-class.txt'
orig_output = '../../data/classification/mnist/orig_test_output.txt'

# load model

net = pickle.load(open(model_file, 'rb'))
for c in [connection for connections in net.connections.values() for connection in connections]:
    print("{} -> {}".format(c.inmod.name, c.outmod.name))
    print("{}".format(c.params))

# load data

test_x = np.loadtxt(test_x_file, delimiter=',')
test_y = np.loadtxt(test_y_file, delimiter=',')
x_test = test_x[:, :-1]
y_test = test_y[:, :]
#x_test /= 255
# test = y_test.reshape(-1, 1)

# you'll need labels. In case you don't have them...
y_test_dummy = np.zeros(y_test.shape)

input_size = x_test.shape[1]
target_size = y_test.shape[1]

assert (net.indim == input_size)
assert (net.outdim == target_size)

# prepare dataset

ds = SDS(input_size, target_size)
ds.setField('input', x_test)
ds.setField('target', y_test_dummy)

# predict

p = net.activateOnDataset(ds)
#
# mse = MSE(y_test, p)
# rmse = sqrt(mse)
#
# print("testing RMSE:", rmse)

np.savetxt(output_predictions_file, p, fmt='%.6f')

wr = open(predictions_class, 'w');
with open(output_predictions_file) as read:
    for line in read:
        line = line.strip()
        val = line.split()
        predict = val.index(max(val))
        wr.write('{}\n'.format(predict))
read.close()
wr.close()

# correctly classified instances

diffcount = 0
with open(orig_output) as base:
    with open(predictions_class) as predicted:
        for line1, line2 in zip(base, predicted):
            if line1 != line2:
                diffcount += 1

print('{} out of 10000 instances wrongly classified'.format(diffcount))
base.close()
predicted.close()
