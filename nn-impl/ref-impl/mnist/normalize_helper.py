import sys
import csv

train_file = '../../data/classification/mnist/sigma-train-y.csv'
test_file = '../../data/classification/mnist/sigma-test-y.csv'
train_normalized = '../../data/classification/mnist/sigma-train-y.csv'
test_normalized = '../../data/classification/mnist/sigma-test-y.csv'
check1 = '../../data/classification/mnist/mnist_train_y.csv'
check2 = '../../data/classification/mnist/sigma-train.csv'
predictions_class = '../../data/classification/mnist/sigma-results.txt'
orig_output = '../../data/classification/mnist/original_op_2000.txt'
python_test_file = '../../data/classification/mnist/orig_train_output.txt'

# values

normalized_train_file = '../../data/classification/iris/train_x_nor.csv'
normalized_test_file = '../../data/classification/iris/test_x_nor.csv'

train_sigma_new = '../../data/classification/mnist/new-sigma-train-nor-10000.csv'
test_sigma_new = '../../data/classification/mnist/new-sigma-test-nor-10000.csv'

unnorm_train_new = '../../data/classification/mnist/mnist_train.csv'
unnorm_test_new = '../../data/classification/mnist/mnist_test.csv'

unnorm_train_file = '../../data/classification/iris/train_x_clas.csv'
unnorm_test_file = '../../data/classification/iris/test_x_clas.csv'

train = []
test = []


def write_array(arr, read):
    for line in read:
        line = line.strip()
        if int(line) == 0:
            arr.append('1,0,0,0,0,0,0,0,0,0')
        elif int(line) == 1:
            arr.append('0,1,0,0,0,0,0,0,0,0')
        elif int(line) == 2:
            arr.append('0,0,1,0,0,0,0,0,0,0')
        elif int(line) == 3:
            arr.append('0,0,0,1,0,0,0,0,0,0')
        elif int(line) == 4:
            arr.append('0,0,0,0,1,0,0,0,0,0')
        elif int(line) == 5:
            arr.append('0,0,0,0,0,1,0,0,0,0')
        elif int(line) == 6:
            arr.append('0,0,0,0,0,0,1,0,0,0')
        elif int(line) == 7:
            arr.append('0,0,0,0,0,0,0,1,0,0')
        elif int(line) == 8:
            arr.append('0,0,0,0,0,0,0,0,1,0')
        elif int(line) == 9:
            arr.append('0,0,0,0,0,0,0,0,0,1')


#
# with open(python_test_file) as tr:
#     write_array(test, tr)
# tr.close()
#
# # with open(test_file) as te:
# #     write_array(test, te)
# # te.close()
# i
# with open(check1, 'w') as tr_n:
#     for val in test:
#         tr_n.write('{}\n'.format(val))
#     tr_n.truncate(tr_n.tell() - 1)
#
# tr_n.close()
# # with open(test_normalized, 'w') as te_n:
# #     for val in test:
# #         te_n.write('{}\n'.format(val))
# #     te_n.truncate(te_n.tell() - 1)
# #
# # te_n.close()

#
# diffcount = 0
# with open(orig_output) as base:
#     with open(predictions_class) as predicted:
#         for line1, line2 in zip(base, predicted):
#             if line1 != line2:
#                 diffcount += 1
#
# print('{} out of 2000 instances wrongly classified'.format(diffcount))
# base.close()
# predicted.close()
# #
# #
# with open(unnorm_test_file) as one:
#     for line in one:
#         value = line.split(',')
#         value = [float(x.strip())/255 for x in value]
#         test.append(value)
#
#
#
with open(unnorm_test_file) as two:
    count = 0
    for line in two:
        count += 1
        if count <= 10000:
            value = line.split(',')
            value = [float(value[x].strip()) / 10.0 for x in range(4)]
            #value = value[1:] + value[:1]
            value.append(1)
            train.append(value)


with open(normalized_test_file, 'w') as tr:
    writer = csv.writer(tr)
    writer.writerows(train)
tr.close()
#
# with open(train_sigma_new, 'w') as te:
#     writer = csv.writer(te)
#     writer.writerows(train)
# te.close()

# with open(test_file) as te:
#     write_array(test, te)
# te.close()
#

