"train a regression MLP"

import numpy as np
import _pickle as pickle
from math import sqrt
from pybrain.datasets.supervised import SupervisedDataSet as SDS
from pybrain.structure.modules.sigmoidlayer import SigmoidLayer
from pybrain.tools.shortcuts import buildNetwork
from pybrain.supervised.trainers import BackpropTrainer
import random as random
from io import StringIO

train_file = '../data/train.csv'
validation_file = '../data/validation.csv'
output_model_file = 'train-model.pkl'
weight1_file = '../data/sigma-in-hid'
weight2_file = '../data/sigma-hid-out'
in_weight_arr = []
out_weight_arr = []
in_chunk = []
out_chunk=[]

hidden_size = 100
epochs = 1
learningrate=0.1
#
random.seed(1)
np.random.seed(1)

# load data
#np.seterr(all='ignore')

train = np.loadtxt(train_file, delimiter=',')
# train_str = np.array_repr(train, max_line_width=100000)
# train_str = train_str.replace('\n','')
#print(train.dtype)
#train = train.rstrip('\n')
validation = np.loadtxt(validation_file, delimiter=',')
train = np.vstack((train, validation))

# for x in np.nditer(train, op_flags=['readwrite']):
#     x[...] = x.replace('\n', '')

x_train = train[:, 0:-1]
y_train = train[:, -1]
y_train = y_train.reshape(-1, 1)

input_size = x_train.shape[1]
target_size = y_train.shape[1]
print(input_size)
print(target_size)

# prepare dataset

ds = SDS(input_size, target_size)
ds.setField('input', x_train)
ds.setField('target', y_train)

# init and train

net = buildNetwork(input_size, hidden_size, target_size, outclass=SigmoidLayer, bias=False)

# assign weights

with open(weight1_file) as f1:
    for line in f1:
        str = line.split(',')
        in_weight_arr.append(float(str[-1]))
f1.close()

for i in range(0, len(in_weight_arr), hidden_size):
    in_chunk.append(in_weight_arr[i : i+hidden_size])

mat = list(zip(*in_chunk))

with open(weight2_file) as f2:
    for line in f2:
        str = line.split(',')
        out_weight_arr.append(float(str[-1]))
f2.close()

for i in range(0, len(out_weight_arr), hidden_size):
    out_chunk.append(out_weight_arr[i : i+hidden_size])

#mat.append(tuple(out_chunk[0]))
mat_array = np.asarray(mat)
out_array = np.asarray(out_chunk)


# new_params = np.array([
#     4.8771477, -1.5627968, -1.7249054, -2.1479702, 0.8937037, -4.123393, 1.9177723, -1.0005713, 4.254505, -4.730583,
#     -2.6263352, -3.8303738, -1.0520095, -1.5069574,  -3.351224, 4.0932837, -3.1164294, 4.8775096,  3.0151182, 0.19197643,
#     -4.675022, 1.958794, -0.7141268, -3.8649166, -2.3644762, -3.5236347, 4.1534243, -4.7546573, 4.577916,  1.9168818,
#     -1.8229938, -2.8524976, -4.8375216, 0.78268886, -3.3650475, 2.778141,  3.8840092, -2.3809922, 3.4082902, 2.9688912,
#     -3.3299513,  -1.203723, 3.930806,  3.8991185,  3.2425583, -0.79325915, -4.0759854, -2.0560158, -0.36708534, 3.6830192,
#     -2.1948676, -3.5681992, 2.4544418, -3.052544, 2.9972167,  0.3711766, 0.99286736, -3.8790095,  -3.219583, 3.5205417,
#     2.9775567, -1.933248, -0.23258269, 1.9365531, 0.37820876, -3.7794667, -3.9617598,  -2.208962, -4.4233074, -2.4513479,
#     -1.8766761, 0.17580629, -1.025843, 1.0125387,  2.4342256, -4.146552, 2.6447792, -4.4038296, -2.9915066, 4.905791,
#     -4.92922, -3.1730228, 1.3970196, -2.005406, 1.3132364, -2.3661363, -1.1653221, 2.3813873,-4.9247546, 0.13837338])

net._setParameters(np.append(mat_array, out_array))

with open('../data/weights.txt', 'w') as f:
    for c in [connection for connections in net.connections.values() for connection in connections]:
        f.write("{} => {}\n".format(c.inmod.name, c.outmod.name))
        for index in range(len(c.params)):
            f.write("{} => {}\n".format(c.whichBuffers(index), c.params[index]))
f.close()

trainer = BackpropTrainer(net, ds, learningrate)

print("training for {} epochs...".format(epochs))

for i in range(epochs):
    mse = trainer.train()
    rmse = sqrt(mse)
    print("training RMSE, epoch {}: {}".format(i + 1, rmse))

pickle.dump(net, open(output_model_file, 'wb'))

