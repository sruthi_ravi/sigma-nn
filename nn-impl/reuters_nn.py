import numpy as np
import _pickle as pickle
from math import sqrt
from pybrain.datasets.supervised import SupervisedDataSet as SDS
from pybrain.tools.shortcuts import buildNetwork
from pybrain.supervised.trainers import backprop

train_x_file = '../reuters_x_train_no_header_short.csv'
train_y_file = '../reuters_y_train_no_header_short.csv'
#validation_file = '../reuters_y_train_no_header.csv'
output_model_file = 'model.pkl'

hidden_size = 512
epochs = 1
learningrate=0.1

# load data

train_x = np.loadtxt(train_x_file, delimiter=',')
print(len(train_x))
print(len(train_x[0]))
train_y = np.loadtxt(train_y_file, delimiter=',')
# validation = np.loadtxt(validation_file, delimiter=',')
# train = np.vstack((train, validation))

x_train = train_x[:, 0:]
y_train = train_y[:, 0:]
#y_train = y_train.reshape(-1, 1)



input_size = x_train.shape[1]
target_size = y_train.shape[1]
print(input_size)
print(target_size)

# prepare dataset

ds = SDS(input_size, target_size)
ds.setField('input', x_train)
ds.setField('target', y_train)

# init and train

net = buildNetwork(input_size, hidden_size, target_size, bias=True)
net.randomize()
trainer = backprop.BackpropTrainer(net, ds, learningrate)

print("training for {} epochs...".format(epochs))

for i in range(epochs):
    mse = trainer.train()
    rmse = sqrt(mse)
    print("training RMSE, epoch {}: {}".format(i + 1, rmse))
for c in [connection for connections in net.connections.values() for connection in connections]:
    print("{} -> {} => {}".format(c.inmod.name, c.outmod.name, c.params))

pickle.dump(net, open(output_model_file, 'wb'))

