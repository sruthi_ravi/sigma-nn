; Create a new PLM that is the combination of two others
; op specifies which operation to perform
; The first plm may contain a subset of the variables of the second one
; If map exists, determines how to map variables of first PLM onto second
; Destructive-p1 is used to allow normalization (by division by p2) to be destructive
(defun combine-plms (p1 map p2 op &optional evidence argument keep-unneeded-slices destructive-p1 invert)
  (let* (np p1rarray nprarray p2rarray
            civ ; Slice cross-index vector
            nprank nr
            p1sizeone
            p1-origin
            cv ; Current index vector
            sizev-1 ; Dimension sizes -1
;            non-empty
            pc1
            pc2
            px
            piecewise-constant
            non-empty1
            non-empty2
            density-plm1
            density-plm2
            )
    (unless (plm-type-check p1 map p2)
      (error "Attempt to ~S two PLMs not of the same type: " op) (pplm p1) (pplm p2)
      )
    ; Under the right circumstances, swap p1 and p2
    (when (and (eq op 'product)
               (null map)
               (= (plm-rank p1) (plm-rank p2))
               (> (plm-size p1) (plm-size p2))
               )
      (setq px p1)
      (setq p1 p2)
      (setq p2 px)
      )
    (setq p1sizeone (= (plm-size p1) 1))
    (setq p1-origin (plm-origin p1))
    (setq pc1 (plm-piecewise-constant p1))
    (setq pc2 (plm-piecewise-constant p2))
    (setq piecewise-constant (and pc1 pc2))
    (cond ((and (eq op 'product) ; This is a product, and one constant region covers entire domain (or there is no domain because no variables)
                p1sizeone
                (region-is-constant p1-origin)
                )
           (when trace-combine
             (format trace-stream "~&~%PRODUCT(CONSTANT):")      
             (format trace-stream "~&~%P1: ") (print-plm p1 symbolic-trace trace-stream)
             (when map
               (format trace-stream "~&~%MAP: vfactor: ~S fvar: ~S omitted: ~S"
                       (smap-vfactor map) (smap-fvar map) (smap-omitted map)
                       )
               )
             (format trace-stream "~&~%P2: ") (print-plm p2 symbolic-trace trace-stream)
             )
           (setq np (if (zerop (region-constant p1-origin))
                        (empty-plm (plm-variables p2))
                      (if (= (region-constant p1-origin) 1)
                          p2
;                      (remove-unneeded-slices
                        (transform-plm #'scale-function p2 (region-constant p1-origin))
;                       )
                      )))
           (when trace-combine
             (format trace-stream "~&~%~S RESULT: " op) (print-plm np symbolic-trace trace-stream)
             )
           np)
          ((eq op 'product)
;           ((and sparse-product
;                 (setq non-empty (check-for-sparse-product p1 op))
;                 )
           (when trace-combine
             (format trace-stream "~&~%PRODUCT(SPARSE):")      
             (format trace-stream "~&~%P1: ") (print-plm p1 symbolic-trace trace-stream)
             (when map
               (format trace-stream "~&~%MAP: vfactor: ~S fvar: ~S omitted: ~S"
                       (smap-vfactor map) (smap-fvar map) (smap-omitted map)
                       )
               )
             (format trace-stream "~&~%P2: ") (print-plm p2 symbolic-trace trace-stream)
             )
           (multiple-value-setq (non-empty1 density-plm1) (plm-non-empty p1))  ;VOLKAN Sparse_Update - New function
           (multiple-value-setq (non-empty2 density-plm2) (plm-non-empty p2)) ;VOLKAN Sparse_Update - New function
           (if (< density-plm1 density-plm2)  ;VOLKAN Sparse_Update - New function
               (setq np (sparse-product-plms p1
;                                         non-empty
                                         ;(plm-non-empty p1)
                                         non-empty1
                                         map p2 evidence argument))
             (setq np (sparse-product-plms-reverse p1  ;VOLKAN Sparse_Update - New function
                                         non-empty2
                                         map p2 evidence argument))
             )
           (when trace-combine
             (format trace-stream "~&~%~S RESULT: " op) (print-plm np symbolic-trace trace-stream)
             )
           np)
          ((and destructive-p1 (member op '(divide divide-0)))
           (setq civ (cross-index-slice-vector (plm-slices p2) nil (plm-slices p1)))
           (setq np p1)
           (setq nprarray (plm-array np))
           (setq nprank (plm-rank np))
           (setq cv (init-vector nprank 0))
           (setq sizev-1 (vector-1 (array-dimensions-v nprarray)))
           (setq p2rarray (plm-array p2))
           (dotimes (i (array-total-size nprarray))
             (setq nr (row-major-aref nprarray i))
             (combine-regions np nr nil p2 (apply #'aref p2rarray (index-list-from-cross-index cv civ nil))
                              nr np op evidence argument pc1 pc2 piecewise-constant invert)
             (setq cv (next-index-vector cv sizev-1 nprank))
             )
           (setq np p1)
           )
          (t
           (when trace-combine
             (format trace-stream "~&~%~S:" op)
             (format trace-stream "~&~%P1: ") (print-plm p1 symbolic-trace trace-stream)
             (when map
               (format trace-stream "~&~%MAP: vfactor: ~S fvar: ~S omitted: ~S"
                       (smap-vfactor map) (smap-fvar map) (smap-omitted map)
                       )
               )
             (format trace-stream "~&~%P2: ") (print-plm p2 symbolic-trace trace-stream)
             )
           (setq p1rarray (plm-array p1))
           (setq np (if p1sizeone (copy-a-plm p2) (apply-slices (plm-slices p1) map (copy-a-plm p2))))
           (update-combine-active (plm-active p1) map (plm-active p2) (plm-active np)) ; Paul's new addition
           (setq nprarray (plm-array np))
           (unless p1sizeone
             (setq civ (cross-index-slice-vector (plm-slices p1) map (plm-slices np)))
             (setq nprank (plm-rank np))
             (setq cv (init-vector nprank 0))
             (setq sizev-1 (vector-1 (array-dimensions-v nprarray)))
             )
           (dotimes (i (array-total-size nprarray))
             (combine-regions p1 (if p1sizeone
                                     p1-origin
                                   (apply #'aref p1rarray (index-list-from-cross-index cv civ map)))
                              map np (row-major-aref nprarray i) (row-major-aref nprarray i) np op evidence argument pc1 pc2 piecewise-constant invert)
             (unless p1sizeone
               (setq cv (next-index-vector cv sizev-1 nprank))
               )
             )
           (unless keep-unneeded-slices
;             (setq np (remove-unneeded-slices np))
             )
           (when trace-combine
             (format trace-stream "~&~%~S RESULT: " op) (print-plm np symbolic-trace trace-stream)
             )
           np)
        )
    (setf (plm-piecewise-constant np) piecewise-constant)
    np)
  )