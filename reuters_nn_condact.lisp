; to run the model, check reuters-mlp function(defvar mse)
(defun reuters-mlp-condact-init (ins outs hids &optional relu)
  (init)
  (set-random-seed 10)
  (setq compute-progress t)
  (setf learning-rate 0.1)
  (setf trace-decisions NIL)
  (learn '(:gd))
;  (setq trace-messages t)
;  (setq trace-transform t)
  (setq trace-empty t)
  (when relu (setq trace-empty t))

  (new-type 'in :numeric t :discrete t :min 0 :max `,ins)
  (new-type 'out :numeric t :discrete t :min 0 :max `,outs)
  (new-type 'hid :numeric t :discrete t :min 0 :max `,hids)

  (predicate 'input :world 'open :arguments '((arg in %)) :no-normalize t :perception t)
  (predicate 'hidden :world 'open :arguments '((arg hid %)) :no-normalize t)
  (predicate 'output :world 'open :arguments '((arg out %)) :no-normalize t :perception t)
            ; :goal t)

  (conditional 'c-layer1
               :condacts `((input (arg (i)))
                           (hidden ,(if relu 'r 's) (arg (h))))
               :function-variable-names '(i h)
               :neural 'h
               :function `,(read-weight-in-hid-condact "/Users/sruthiravi/DR/sigma-nn/nn-impl/data/in-hidden-100")
              ; :function `,(create-random-function-2d-condact ins hids)
               )

  (conditional 'c-layer2
               :condacts `((hidden (arg (h)))
                           (output ,(if relu 'r 's) (arg (o))))
               :function-variable-names '(h o)
               :neural 'o
;               :function `((1.1 0 0) (3.1 0 1) (.1 1 0) (1.17 1 1))
;               :learning-rate 0
               :function `,(read-weight-hid-out-condact "/Users/sruthiravi/DR/sigma-nn/nn-impl/data/hid-out-100")
               ;:function `,(create-random-function-2d-condact hids outs)
               )
  t)
  
; Create a 2d conditional function with all random values
(defun create-random-function-2d-condact (s1 s2 &optional span)
  (let (f)
    (if (not span) (setq span 5.0))
    ; Create data elements for plm
    (dotimes (i s1)
      (dotimes (j s2)
        (push (list (random-weight span) i j) f)
        )
      )
    (print "Created function")
    (nreverse f))
  )

(defun read-weight-in-hid-condact (in-hid-file)
  (let
     ((layer1 (open in-hid-file :if-does-not-exist nil)))
     (setf w-in-hid ())
     (setf count 0)
     (when layer1
       (loop for line = (read-line layer1 nil)
              while line do
                  (if (<= count 79)
                      (push (list (hcl:parse-float (subseq line 10)) (parse-integer (subseq line 1 2)) (parse-integer (subseq line 4 5))) w-in-hid) 
                  (push (list (hcl:parse-float (subseq line 11)) (parse-integer (subseq line 1 2)) (parse-integer (subseq line 4 6))) w-in-hid))
                  (setf count (+ count 1))
       )
       (close layer1)

       )
      (format t "~a~%" w-in-hid)

  )
  w-in-hid

)


(defun read-weight-hid-out-condact (hid-out-file)
   (let
       ((layer2 (open hid-out-file :if-does-not-exist nil)))
       (setf w-hid-out ())
       (setf count 0)
       (when layer2
       (loop for line = (read-line layer2 nil)
            while line do
                (if (<= count 9)
                    (push (list (hcl:parse-float (subseq line 10)) (parse-integer (subseq line 1 2)) (parse-integer (subseq line 4 5))) w-hid-out)
                (push (list (hcl:parse-float (subseq line 11)) (parse-integer (subseq line 1 3)) (parse-integer (subseq line 5 6))) w-hid-out))
                (setf count (+ count 1))
       )
       (close layer2)
      )
      (format t "~a~%" w-hid-out)
    )
    w-hid-out
)

;This is how this model is run
;(reuters-mlp "/users/ustun/vu_sigma_projects/NN_In_Sigma/reuters_x_train_no_header.csv" "/users/ustun/vu_sigma_projects/NN_In_Sigma/reuters_y_train_no_header.csv" 1000 46 512)
(defun reuters-mlp-condact(x-train-file-name y-train-file-name ins outs hids epoch)
  (let (
        ;in-data-stream
        ;out-data-stream
        )

  (reuters-mlp-condact-init ins outs hids)
  (setf learning-rate 0.1)
  (setf max-decisions 100000000)
  (setq pre-t `((format trace-stream "~&~%   >>> Training Cycle <<<")
                (setq in-data-stream (open ,x-train-file-name))
                (setq out-data-stream (open ,y-train-file-name))
                (setq post-t '((close in-data-stream) (close out-data-stream))
                ))
        )

   (setq perceive-list `((unless (listen in-data-stream)
                            (format trace-stream "*** No more instances in file. ***")
                            (throw 'decide t)
                            )
                         ; Set up perception for an instance
                          (instance-reuters-condact (read-line in-data-stream) (read-line out-data-stream) ,ins ,outs)
                          )
          )

   (trials epoch)
   )
)
(defun reuters-mlp-resume-condact(x-train-file-name y-train-file-name ins outs epochs)
  (let (
        ;in-data-stream
        ;out-data-stream
        )
  ;(reuters-mlp-init ins outs hids)
  ;(setf max-decisions 10000)
  (learn '(:gd))
  (setf learning-rate 0.1)
  (setq pre-t `((format trace-stream "~&~%   >>> Training Cycle <<<")
                (setq in-data-stream (open ,x-train-file-name))
                (setq out-data-stream (open ,y-train-file-name))
                (setq post-t '((close in-data-stream) (close out-data-stream))
                ))
        )
  (setq post-d nil)

   (setq perceive-list `((unless (listen in-data-stream)
                            (format trace-stream "*** No more instances in file. ***")
                            (throw 'decide t)
                            )
                         ; Set up perception for an instance
                          (instance-reuters-condact (read-line in-data-stream) (read-line out-data-stream) ,ins ,outs)
                          )
          )

   (trials epochs)
   )
)
(defun reuters-mlp-test-condact(x-test-file-name y-test-file-name ins outs hids)
  (format trace-stream "~%~&~&******************Testing Phase ~%")
  (learn)
  (let (
        ;mse
        ;in-data-stream
        ;out-data-stream
        )
  ;(reuters-mlp-init ins outs hids)
    (setf max-decisions 1000000000)

  (setq pre-t `((format trace-stream "~&~%   >>> Testing Cycle <<<")
                (setq in-data-stream (open ,x-test-file-name))
                (setq out-data-stream (open ,y-test-file-name))
                (setf mse 0)
                (setf sum-dev 0.0)
                ;(setq post-t '((close in-data-stream) (close out-data-stream))
                ;      (setf mse 0)
                ;)
                )
        )

   (setq perceive-list `((unless (listen in-data-stream)
                            (format trace-stream "*** No more instances in file. ***")
                            (throw 'decide t)
                            )
                         ; Set up perception for an instance
                          (instance-reuters-test-condact (read-line in-data-stream) ,ins )
                          )
          )

   (setq post-d '(
                  (setf mse (mse-f-condact mse (region-constant (aref (plm-array (vnp 'output))0)) (read-line out-data-stream) 1))
                 (format trace-stream "~&~&Guess object: ~S~%"  (region-constant (aref (plm-array (vnp 'output))0))
                 )
                 (setf sum-dev (+ sum-dev mse))
                 )
         )
   (setq post-t '(
                  (setf rmse (sqrt (/ sum-dev 838)))
                 ; (setf rmse (sqrt (/ sum-dev 10)))
                  (format trace-stream "~&~& Root mean square error: ~S~%" rmse)
                  ))
   ;TODO divide mse by number of instances and take the square root
   (format trace-stream "~%~&~&******************Testing Phase ~%")
   (trials 1)
   )
)

(defun mse-f-condact (mse prediction out-line outs)
  (let (
        value  out-list comma-loc
              )
  ;(perceive '((output*goal 0 (arg 0))))
  (dotimes (i outs)
    (multiple-value-setq (value comma-loc) (read-from-string out-line))
    ;(push (list 'output*goal value `(arg ,i)) out-perceive)
    (push value out-list)
    (when (< comma-loc (length out-line)) ; There is another entry in the string
      (setq out-line (subseq out-line (+ comma-loc 1))) ; Move to next entry in the string
      )
    )
  (format trace-stream "~&~&out-list: ~S~%" out-list)
  (setf mse (expt (- (nth 0 out-list) prediction) 2))
  (format trace-stream "~&~&mse: ~S~%" mse)
  mse
)
)
(defun instance-reuters-condact (in-line out-line ins outs)
  (let (
        value in-perceive out-perceive comma-loc
              )
    ;(perceive '((input 0 (arg 0)) (input 0 (arg 1)) (input 1 (arg 2))))
    (dotimes (i ins)
      (multiple-value-setq (value comma-loc) (read-from-string in-line))
      (push (list 'input value `(arg ,i)) in-perceive)
      (when (< comma-loc (length in-line)) ; There is another entry in the string
        (setq in-line (subseq in-line (+ comma-loc 1))) ; Move to next entry in the string
        )
      )
    ;(nreverse in-perceive)
    (perceive in-perceive)

    ;(perceive '((output*goal 0 (arg 0))))
     (dotimes (i outs)
      (multiple-value-setq (value comma-loc) (read-from-string out-line))
      (push (list 'output value `(arg ,i)) out-perceive)
      (when (< comma-loc (length out-line)) ; There is another entry in the string
        (setq out-line (subseq out-line (+ comma-loc 1))) ; Move to next entry in the string
        )
      )
     ;(nreverse out-perceive)
     (perceive out-perceive)

  )
)
(defun instance-reuters-test-condact (in-line ins)
  (let (
        value in-perceive comma-loc
              )
    ;(perceive '((input 0 (arg 0)) (input 0 (arg 1)) (input 1 (arg 2))))
    (dotimes (i ins)
      (multiple-value-setq (value comma-loc) (read-from-string in-line))
      (push (list 'input value `(arg ,i)) in-perceive)
      (when (< comma-loc (length in-line)) ; There is another entry in the string
        (setq in-line (subseq in-line (+ comma-loc 1))) ; Move to next entry in the string
        )
      )
    ;(nreverse in-perceive)
    (perceive in-perceive)



  )
)