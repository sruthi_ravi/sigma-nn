; Test XOR via NNs with one hidden layer and bias weights
; Arc variant of xor6
(defun xor7-init-volkan ()
  (init)
  (setq compute-progress t)
  (setf learning-rate 0.1)
 (learn '(:gd))
;  (setq trace-messages t)
;  (setq trace-transform t)
  (setq trace-empty t)

  (new-type 'n4 :numeric t :discrete t :min 0 :max 4)
  (new-type 'n3 :numeric t :discrete t :min 0 :max 3)
  (new-type 'n1 :numeric t :discrete t :min 0 :max 1)

  (predicate 'input :world 'open :arguments '((arg n3 [])) :no-normalize t :perception t)
  (predicate 'hidden :world 'open :arguments '((arg n4 [])) :no-normalize t)
  (predicate 'output :world 'open :arguments '((arg n1 [])) :no-normalize t :perception t
              :goal t)
             ; :goal '((output*goal 0 (arg 0)))) ; Hack to get first decision goal right

  ; Predicates for abductive reasoning
  (predicate 'input*abduct :arguments '((arg n3 [])) :no-normalize t)
  (predicate 'hidden*abduct :arguments '((arg n4 [])) :no-normalize t)

  (conditional 'c-layer1
               :conditions '((input (arg (i))))
               :actions '((hidden s (arg (h))))
               :function-variable-names '(i h)
               :vector t
               :function `(
                      (0.154947426 0 0) (-0.88778574763 0 1) (-0.347912149326 0 2) (0 0 3)
                      (0.378162519602 1 0) (-1.98079646822 1 1) (0.156348969104 1 2) (0 1 3)
                      (-0.18718385 2 0) (1.532779214 2 1) (1.46935877 2 2) (100 2 3)
                           )
               )

  (conditional 'c-layer2
               :conditions '((hidden (arg (h))))
               :actions '((output s (arg (o))))
               :function-variable-names '(h o)
               :vector t
               :function `(
                           (1.230290681 0 0) (1.202379849 1 0) 
                           (-0.387326817 2 0) (0.045758517 3 0)
                           )
               )

  (conditional 'c-layer2-i
               :conditions '((output*difference-d (arg (o)))
                             (output s (arg (o))))
               :actions '((hidden*abduct (arg (h))))
               :vector t
               :forward-conditional 'c-layer2
               :exclude-forward-backward t
               )

  (conditional 'c-layer1-i
               :conditions '((hidden*abduct (arg (h)))
                             (hidden s (arg (h))))
               :actions '((input*abduct (arg (i))))
               :vector t
               :forward-conditional 'c-layer1
               :exclude-forward-backward t
               )

  t)

; Train xor7 over multiple examples
(defun train-xor7-volkan (cycles)
  ;(set-random-seed 2)
  (xor7-init-volkan)
  (let ((plv (vector
              '(perceive '((input 0 (arg 0)) (input 0 (arg 1)) (input 1 (arg 2))))
              '(perceive '((input 0 (arg 0)) (input 1 (arg 1)) (input 1 (arg 2))))
              '(perceive '((input 1 (arg 0)) (input 0 (arg 1)) (input 1 (arg 2))))
              '(perceive '((input 1 (arg 0)) (input 1 (arg 1)) (input 1 (arg 2))))
              ))
        (elv (vector
              '(perceive '((output*goal 0 (arg 0))))
              '(perceive '((output*goal 1 (arg 0))))
              '(perceive '((output*goal 1 (arg 0))))
              '(perceive '((output*goal 0 (arg 0))))
              ))
        )
    ;(setq trace-decisions nil)
    (setq trace-performance nil)
    (dotimes (i (1- cycles))
      (dotimes (i (length plv))
        (setq perceive-list (list (aref plv i)))
        (eval (aref elv i)) ; Set output goal
        (d 1)
        )
      )
    (setq trace-gdl t)
    (dotimes (i (length plv))
      (setq perceive-list (list (aref plv i))) ; Initiate input
      (eval (aref elv i)) ; Set output goal
      (d 1)
      (format t "~&~%~%------------------")
      (format t "~&Input:")
      (pwmb 'input t)
      (format t "~&~%Desired output:")
      (pwmb 'output*goal t)
      (format t "~&~%Computed output:")
      (pwmb 'output t)
      (format t "~&~%Output error derivative:")
      (pwmb 'output*difference-d t)
      (format t "~&------------------")
      )
    )
  t)

; Test XOR via NNs with one hidden layer and bias weights
; Arc variant of xor6
(defun xor7-init-volkan1 ()
  (init)
  (setq compute-progress t)
 ; (learn '(:gd))
;  (setq trace-gdl t)
;  (setq trace-messages t)
;  (setq trace-transform t)
  (setq trace-empty t)

  ;(new-type 'n4 :numeric t :discrete t :min 0 :max 4)
  (new-type 'n3 :numeric t :discrete t :min 0 :max 3)
  (new-type 'n1 :numeric t :discrete t :min 0 :max 1)

  (predicate 'input :world 'open :arguments '((arg n3 [])) :no-normalize t :perception t)
  (predicate 'hidden :world 'open :arguments '((arg n3 [])) :no-normalize t)
  (predicate 'output :world 'open :arguments '((arg n1 [])) :no-normalize t
              :goal '((output*goal 0 (arg 0)))) ; Hack to get first decision goal right

  ; Predicates for abductive reasoning
  ;(predicate 'input*abduct :arguments '((arg n3 [])) :no-normalize t)
  ;(predicate 'hidden*abduct :arguments '((arg n3 [])) :no-normalize t)

  (conditional 'c-layer1
               :conditions '((input (arg (i))))
               :actions '((hidden s (arg (h))))
               :function-variable-names '(i h)
               :vector t
               :function `(
                           (20 0 0) (-20 0 1) (0 0 2)
                           (20 1 0) (-20 1 1) (0 1 2)
                           (-10 2 0) (30 2 1) (100 2 2)
                           )
               )

  (conditional 'c-layer2
               :conditions '((hidden (arg (h))))
               :actions '((output s (arg (o))))
               :function-variable-names '(h o)
               :vector t
               :function `(
                           (20 0 0) (20 1 0) (-30 2 0)
                           )
               )

  #|(conditional 'c-layer2-i
               :conditions '((output*difference-d (arg (o)))
                             (output s (arg (o))))
               :actions '((hidden*abduct (arg (h))))
               :vector t
               :forward-conditional 'c-layer2
               :exclude-forward-backward t
               )

  (conditional 'c-layer1-i
               :conditions '((hidden*abduct (arg (h)))
                             (hidden s (arg (h))))
               :actions '((input*abduct (arg (i))))
               :vector t
               :forward-conditional 'c-layer1
               :exclude-forward-backward t
               )|#

  t)

(defun test-xor7-volkan (which)
(if which (xor7-init-volkan1) (xor7-init-volkan))
(perceive '((input 0 (arg 0)) (input 0 (arg 1)) (input 1 (arg 2))))
(d 1)
(pwmb 'output t)(perceive '((input 1 (arg 0)) (input 0 (arg 1)) (input 1 (arg 2))))
(d 1)
(pwmb 'output t)
(perceive '((input 0 (arg 0)) (input 1 (arg 1)) (input 1 (arg 2))))
(d 1)
(pwmb 'output t)
(perceive '((input 1 (arg 0)) (input 1 (arg 1)) (input 1 (arg 2))))
(d 1)
(pwmb 'output t)

)


(defun train-xor7-volkan1 (cycles)
  (set-random-seed 1)
  (xor7-init-volkan)
  (setf learning-rate 0.1)
  ;(setq trace-gdl t)
  (let ((plv (vector
              '(perceive '((input 0 (arg 0)) (input 0 (arg 1)) (input 1 (arg 2))))
              '(perceive '((input 0 (arg 0)) (input 1 (arg 1)) (input 1 (arg 2))))
              '(perceive '((input 1 (arg 0)) (input 0 (arg 1)) (input 1 (arg 2))))
              '(perceive '((input 1 (arg 0)) (input 1 (arg 1)) (input 1 (arg 2))))
              ))
        (elv (vector
              '(evidence '((output*goal 0 (arg 0))))
              '(evidence '((output*goal 1 (arg 0))))
              '(evidence '((output*goal 1 (arg 0))))
              '(evidence '((output*goal 0 (arg 0))))
              ))
        )
    ;(setq trace-decisions nil)
    ;(setq trace-performance nil)
    (dotimes (i (1- cycles))
      (dotimes (i (length plv))
        (setq perceive-list (list (aref plv i)))
        (eval (aref elv i)) ; Set output goal
        (d 1)
        )
      )
    (setq trace-gdl t)
    (dotimes (i (length plv))
      (setq perceive-list (list (aref plv i))) ; Initiate input
      (eval (aref elv i)) ; Set output goal
      (d 1)
      (format t "~&~%~%------------------")
      (format t "~&Input:")
      (pwmb 'input t)
      (format t "~&~%Desired output:")
      (pwmb 'output*goal t)
      (format t "~&~%Computed output:")
      (pwmb 'output t)
      (format t "~&~%Output error derivative:")
      (pwmb 'output*difference-d t)
      (format t "~&------------------")
      )
    )
  t)
(defun test-simple-model-volkan ()
(simple-model-volkan)
(setf trace-gdl t)
(setf learning-rate 0.9)  
(perceive '((input 1 (arg 0)) (input 0 (arg 1)) (input 1 (arg 2)) (input 1 (arg 3)) ))
;(evidence '((output*goal 0 (arg 1))))
(d 1)
(pwmb 'output t)

)

; Volkan NN example
(defun simple-model-volkan ()
  (init)
  (setq compute-progress t)
  (learn '(:gd))
;  (setq trace-messages t)
;  (setq trace-transform t)
  (setq trace-empty t)

  (new-type 'n4 :numeric t :discrete t :min 0 :max 4)
  (new-type 'n3 :numeric t :discrete t :min 0 :max 3)
  (new-type 'n1 :numeric t :discrete t :min 0 :max 1)

  (predicate 'input :world 'open :arguments '((arg n4 [])) :no-normalize t :perception t)
  (predicate 'hidden :world 'open :arguments '((arg n3 [])) :no-normalize t)
  (predicate 'output :world 'open :arguments '((arg n1 [])) :no-normalize t
              :goal '((output*goal 1 (arg 0)))) ; Hack to get first decision goal right

  ; Predicates for abductive reasoning
  (predicate 'input*abduct :arguments '((arg n4 [])) :no-normalize t)
  (predicate 'hidden*abduct :arguments '((arg n3 [])) :no-normalize t)

  (conditional 'c-layer1
               :conditions '((input (arg (i))))
               :actions '((hidden s (arg (h))))
               :function-variable-names '(i h)
               :vector t
               :function `(
                           (0.2 0 0) (-0.3 0 1) (0 0 2)
                           (0.4 1 0) (0.1 1 1) (0 1 2)
                           (-0.5 2 0) (0.2 2 1) (0 2 2)
                           (-0.4 3 0) (0.2 3 1) (100 3 2)
                           )
               )

  (conditional 'c-layer2
               :conditions '((hidden (arg (h))))
               :actions '((output s (arg (o))))
               :function-variable-names '(h o)
               :vector t
               :function `(
                           (-0.3 0 0) (-0.2 1 0) (0.1 2 0) 
                           )
               )

  (conditional 'c-layer2-i
               :conditions '((output*difference-d (arg (o)))
                             (output s (arg (o))))
               :actions '((hidden*abduct (arg (h))))
               :vector t
               :forward-conditional 'c-layer2
               :exclude-forward-backward t
               )

  (conditional 'c-layer1-i
               :conditions '((hidden*abduct (arg (h)))
                             (hidden s (arg (h))))
               :actions '((input*abduct (arg (i))))
               :vector t
               :forward-conditional 'c-layer1
               :exclude-forward-backward t
               )

  t)