; Delete exclude on link structure

; Following should be converted to global variables in sigma.lisp
; Symbol used to specify that a nonlinear tanh should be applied to a pattern
;(defvar tanh-symbol 't)
; Symbol used to specify that a nonlinear exponential should be applied to a pattern
;(defvar exp-symbol 'e)

; New functions
; -------------

; Convert any form of unique argument to selection
(defun make-unique-argument-distribution (arg)
  (if (> (length arg) 2)
      (list (car arg) (cadr arg) '%)
    arg)
  )
(defun make-unique-arguments-distribution (args)
  (mapcar #'make-unique-argument-distribution args)
  )

; Specify locations of datasets
(defvar nn-dataset-datapaths)
(setq nn-dataset-datapaths
      '(
        ; feedforward network from http://www.doc.ic.ac.uk/~sgc/teaching/pre2012/v231/lecture13.html
        (tff1 "../data/tff1.NN.data.txt")
        (y-s "../data/yellow-small.NN.data.txt")
        (y-s2 "../data/yellow-small2.NN.data.txt")
        (xor "../data/xor.NN.data.txt")
        ))

; Specify number of input and output units for data sets
(defvar nn-dataset-definitions)
(setq nn-dataset-definitions
      '(
        (tff1 3 2)
        (y-s 8 1)
        (y-s2 8 2)
        (xor 4 1)
        ))

; Create evidence for an NN instance specified as a comma-delimited string (one line from data set)
(defun nn-instance (inst-s inputs outputs)
  (let (value comma-loc (inputs-1 (1- inputs)) (outputs-1 (1- outputs)))
;    (format trace-stream "~&~%*** NN Instance~&Inputs: ")
    (empty-pers) ; Empty perceptual memories before adding the evidence
    (dotimes (i inputs)
      (multiple-value-setq (value comma-loc) (read-from-string inst-s))
;      (format trace-stream "~S" value)
      (perceive `((input ,value (arg ,i))))
      (when (< i inputs-1) ; There is another entry in the string
        (setq inst-s (subseq inst-s (+ comma-loc 1))) ; Move to next entry in the string
;        (format trace-stream ", ")
        )
      )
    (setq inst-s (subseq inst-s (+ comma-loc 1))) ; Move to next entry in the string
;    (format trace-stream "~&Target Outputs: ")
    (dotimes (o outputs)
      (multiple-value-setq (value comma-loc) (read-from-string inst-s))
;      (format trace-stream "~S" value)
      (evidence `((output*goal ,value (arg ,o))))
      (when (< o outputs-1) ; There is another entry in the string
        (setq inst-s (subseq inst-s (+ comma-loc 1))) ; Move to next entry in the string
;        (format trace-stream ", ")
        )
      )
    )
  )

(defun nn-setup-dataset (datapath inputs outputs &optional test)
  (setq data-stream (open datapath))
;    (setq post-d `((update-gd-test-results ',omit)))
  (learn (unless test '(:gd)))
;    (when test
;      (when category-name
;        (setq pre-d `((format trace-stream "~&~%") (ppvn ',category-name)))
;        )
;      (setq gd-test-results (mapcar #'init-gd-test-results omit))
;      )
  (setq perceive-list `((unless (listen data-stream)
;                          (format trace-stream "*** No more instances in file. ***")
                          (throw 'decide t)
                          )
                        (nn-instance (read-line data-stream) ,inputs ,outputs) ; Set up perception and evidence for an instance
                        )
        )
;  (when test
;    (setq post-d (append `((format trace-stream "~&Computed Outputs: ") (ppvn 'output)) post-d))
;    )
  t)

; Create a 2d conditional function with all random values
(defun random-function-2d (s1 s2)
  (let (f)
    ; Create data elements for plm
    (dotimes (i s1)
      (dotimes (j s2)
        (push (list (random-weight) i j) f)
        )
      )
    (nreverse f))
  )

; Define an NN data set with one conditional per layer with full arc for backpropagation
(defun nn-define-data-set (inputs outputs &optional hidden trace)
  (let (p1t p2t (hs 0) p1n p2n p1bn p2bn p1s p2s fcn)
    (init)
    (setq compute-progress t)
    (setq trace-trials trace)
    (setq trace-performance trace)
    (setq trace-decisions trace)
    (setq trace-gdl trace)
    ; Handle input layer
    (setq p1t (concat-symbols `(d ,inputs)))
    (new-type p1t :numeric t :discrete t :min 0 :max inputs)
    (predicate 'input :arguments `((arg ,p1t [])) :no-normalize t :perception t)
    (setq p1n 'input)
    (setq p1s inputs)
    ; Handle hidden layers
    (dolist (h hidden)
      (setq p2s h)
      (setq p2t (concat-symbols `(d ,h)))
      (unless (type-from-name p2t)
        (setq p2t (concat-symbols `(d ,h)))
        (new-type p2t :numeric t :discrete t :min 0 :max h)
        )
      (setq p2n (concat-symbols `(h ,hs)))
      (predicate p2n :arguments `((arg ,p2t [])) :no-normalize t) ; Forward predicate
      (setq p2bn (concat-symbols `(,p2n *back)))
      (predicate p2bn :arguments `((arg ,p2t [])) :no-normalize t) ; Backward predicate
      (setq fcn (concat-symbols `(,p1n ,p2n) t))
      ; Create forward conditional
      (conditional fcn
                   :conditions `((,p1n (arg (a))))
                   :actions `((,p2n s (arg (b))))
                   :vector t
                   :function-variable-names '(a b)
                   :function (random-function-2d p1s p2s)
                   )
      ; Create backward condtiional
        (conditional (concat-symbols `(,p2n ,p1n) t)
                     :conditions `((,p2bn (arg (b)))
                                   (,p2n s (arg (b))))
                     :actions (unless (eq p1n 'input) `((,p1bn (arg (a)))))
                     :vector t
                     :forward-conditional fcn
                     :exclude-forward-backward t
                     )
      (setq p1t p2t)
      (setq p1n p2n)
      (setq p1s p2s)
      (setq p1bn p2bn)
      (setq hs (1+ hs))
      )
    ; Handle output layer
    (setq p2t (concat-symbols `(d ,outputs)))
    (unless (type-from-name p2t)
      (setq p2t (concat-symbols `(d ,outputs)))
      (new-type p2t :numeric t :discrete t :min 0 :max outputs)
      )
    (predicate 'output :arguments `((arg ,p2t [])) :no-normalize t :goal t)
    (setq fcn (concat-symbols `(,p1n output) t))
    ; Create forward conditional
    (conditional fcn
                 :conditions `((,p1n (arg (a))))
                 :actions `((output s (arg (b))))
                 :vector t
                 :function-variable-names '(a b)
                 :function (random-function-2d p1s outputs)
                 )
    ; Create backward condtiional
    (conditional (concat-symbols `(output ,p1n) t)
                 :conditions `((output*difference-d (arg (b)))
                               (output s (arg (b))))
                 :actions `((,p1bn (arg (a))))
                 :vector t
                 :forward-conditional fcn
                 :exclude-forward-backward t
                 )
    t)
  )

; Learn from dataset via gradient descent learning
(defun nnl (train test &optional hidden training-cycles trace lr-fraction-sp)
  (let (path-dyad definition-dyad dataset datapath inputs outputs)
    ; Training
    (setq path-dyad (assoc train nn-dataset-datapaths))
    (unless path-dyad
      (error "Unknown NN training dataset version: ~S!" train)
      )
    (setq dataset (car path-dyad))
    (setq datapath (merge-pathnames (cadr path-dyad) dataset-path))
    (setq definition-dyad (assoc dataset nn-dataset-definitions))
    (unless definition-dyad
      (error "Unknown NN dataset: ~S!" dataset)
      )
    (setq inputs (cadr definition-dyad))
    (setq outputs (caddr definition-dyad))
    (nn-define-data-set inputs outputs hidden trace)
    (setq learning-rate-fraction-of-smoothing-parameter lr-fraction-sp)
    (setq pre-t `((nn-setup-dataset ',datapath ,inputs ,outputs)
                  (setq post-t '((close data-stream)))
                  ))
    (when trace
      (setq post-d `((format t "~&~%~%------------------")
                     (format t "~&Input:")
                     (pwmb 'input t)
                     (format t "~&~%Desired output:")
                     (pwmb 'output*goal t)
                     (format t "~&~%Computed output:")
                     (pwmb 'output t)
                     (format t "~&~%Output error derivative:")
                     (pwmb 'output*difference-d t)
                     (format t "~&------------------")
                     ))
      )
    (format trace-stream "~&~%   >>> BP Training <<<")
    (unless (eq (trials training-cycles) interrupt-symbol)
      ; Testing
      (setq pre-t '())
      (format trace-stream "~&~%   >>> BP Testing <<<")
      (setq path-dyad (assoc test nn-dataset-datapaths))
      (unless path-dyad
        (error "Unknown NN testing dataset version: ~S!" test)
        )
      (setq datapath (merge-pathnames (cadr path-dyad) dataset-path))
      (nn-setup-dataset datapath inputs outputs t)
;      (when (equal (stype-constants (type-from-name category)) '(t f))
;        (setq post-t `((close data-stream) (print-gd-test-results '(,category))))
;        )
      (setq post-d `((format t "~&~%~%------------------")
                     (format t "~&Input:")
                     (pwmb 'input t)
                     (format t "~&~%Desired output:")
                     (pwmb 'output*goal t)
                     (format t "~&~%Computed output:")
                     (pwmb 'output t)
                     (format t "~&~%Output error derivative:")
                     (pwmb 'output*difference-d t)
                     (format t "~&------------------")
                     ))
      (trials 1 t)
      (close data-stream)
;      (format trace-stream "~&~%") (pcfs t)
      )
    )
  )

; RELU
; ----

; RELU derivative
(defun d-relu (x)
  (if (> x 0) 1 0)
  )

; Transform a function by the derivative of the RELU function
(defun derivative-relu-function (f r &optional piecewise-constant)
  r ; dummy so no warning about r not being used
  (if piecewise-constant
      (d-relu f)
    (let ((nf (init-vector (length f) 0)))
      (setf (aref nf 0) (d-relu (aref f 0))) ; Assuming no actual dimension weights
      nf))
  )

; TANH
; ----

; tanh function already defined within Lisp

; Transform a constant function by a tanh function
(defun tanh-function (f r &optional piecewise-constant)
  r ; dummy so no warning about r not being used
  (if piecewise-constant
      (tanh f)
    (let ((nf (init-vector (length f) 0)))
      (setf (aref nf 0) (tanh (aref f 0)))
      nf))
  )

; Invert tanh
(defun inverse-tanh (x)
  (let ((1-x (- 1 x)))
    (if (zerop 1-x) ; Attempt to divide by 0
        (* .5 (log infinity))
      (let ((inside-log (/ (1+ x) 1-x)))
        (* .5 (if (zerop inside-log) negative-infinity (log inside-log)))
        ))
    )
  )

; Transform a function by the inverse of tanh
(defun inverse-tanh-function (f r &optional piecewise-constant)
  r ; dummy so no warning about r not being used
  (if piecewise-constant
      (inverse-tanh f)
    (let ((nf (init-vector (length f) 0)))
      (setf (aref nf 0) (inverse-tanh (aref f 0))) ; Assuming no actual dimension weights
      nf))
  )

; tanh derivative
(defun d-tanh (x)
  (- 1 (expt (tanh x) 2))
  )

; Transform a function by the derivative of the tanh function
(defun derivative-tanh-function (f r &optional piecewise-constant)
  r ; dummy so no warning about r not being used
  (if piecewise-constant
      (d-tanh f)
    (let ((nf (init-vector (length f) 0)))
      (setf (aref nf 0) (d-tanh (aref f 0))) ; Assuming no actual dimension weights
      nf))
  )

; EXP
; ---

; exp (e^x) function already defined within Lisp

; Transform a constant function by an exp function
(defun exp-function (f r &optional piecewise-constant)
  r ; dummy so no warning about r not being used
  (if piecewise-constant
      (exp f)
    (let ((nf (init-vector (length f) 0)))
      (setf (aref nf 0) (exp (aref f 0)))
      nf))
  )

; Invert exp (ln)
(defun inverse-exp (x)
  (if (<= x 0) ; Attempt to take log of 0 or a negative number
      0
    (log x))
  )

; Transform a function by the inverse of exp
(defun inverse-exp-function (f r &optional piecewise-constant)
  r ; dummy so no warning about r not being used
  (if piecewise-constant
      (inverse-exp f)
    (let ((nf (init-vector (length f) 0)))
      (setf (aref nf 0) (inverse-exp (aref f 0))) ; Assuming no actual dimension weights
      nf))
  )

; exp derivative
(defun d-exp (x)
  (exp x)
  )

; Transform a function by the derivative of the exp function
(defun derivative-exp-function (f r &optional piecewise-constant)
  r ; dummy so no warning about r not being used
  (if piecewise-constant
      (d-exp f)
    (let ((nf (init-vector (length f) 0)))
      (setf (aref nf 0) (d-exp (aref f 0))) ; Assuming no actual dimension weights
      nf))
  )

; Find the variable node that feeds a gamma factor of a conditional
; Assumes no condacts, so there is only an incoming link from the conditions
(defun pregamma-vn (c)
  (let ((gamma (conditional-gamma-node c))
        pgl)
    (unless gamma (error "No gamma node in conditional ~S in PREGAMMA-VN!" (conditional-name c)))
    (dolist (l (node-links gamma))
      (when (eq (node-pattern-type (link-var-node l)) 'condition)
        (setq pgl (link-var-node l))
        (return)
        )
      )
    (unless pgl (error "No pregamma variable node for node ~S in PREGAMMA-VN!" (node-name gamma)))
    pgl)
  )

; Find link at factor node with variable node
(defun link-with-vn (fn vn)
  (let (vl)
    (dolist (l (node-links fn))
      (when (eq (link-var-node l) vn)
        (setq vl l)
        (return)
        )
      )
    (unless vl (error "No link at factor node ~S for variable node ~S in LINK-WITH-VN!" (node-name fn) (node-name vn)))
    vl)
  )

; Add nodes to compute derivatives of differences for predicates that have them
(defun add-difference-derivatives nil
  (let (other-pred fn fun)
    (dolist (pred (graph-predicates cg))
      ; Set up derivative of difference if exists
      (when (predicate-difference-derivative-predicate pred)
        (setq fun nil)
        ; The predicate for the difference derivative is created in the goal predicate
        ; Create a WMVN for the goal predicate if necessary
        (setq other-pred (predicate-goal-predicate pred))
        (unless (predicate-outgoing-vn other-pred)
          (setf (predicate-outgoing-vn other-pred)
                (create-wm-variable-node other-pred (predicate-wm other-pred)
                                         (concat-symbols `(,(predicate-name other-pred) shared wm-vn) t)
                                         nil (predicate-wm-variables other-pred) nil t nil t nil))
          )
        ; Create a WMVN for the difference derivative predicate if necessary
        (setq other-pred (predicate-difference-derivative-predicate pred))
        (unless (predicate-outgoing-vn other-pred)
          (setf (predicate-outgoing-vn other-pred)
                (create-wm-variable-node other-pred (predicate-wm other-pred)
                                         (concat-symbols `(,(predicate-name other-pred) shared wm-vn) t)
                                         nil (predicate-wm-variables other-pred) t t t t nil))
          )
        ; Create factor node at which difference derivative is computed
        (setq fn (init-factor-node (concat-symbols (list (predicate-name pred) 'df) t) 'difference (node-variables (predicate-outgoing-vn pred))
                                   (list (predicate-outgoing-vn (predicate-goal-predicate pred))
                                         (predicate-outgoing-vn pred)
                                         (predicate-outgoing-vn (predicate-difference-derivative-predicate pred)))
                                   (list (predicate-outgoing-vn (predicate-difference-derivative-predicate pred)))  t nil nil nil nil))
        ; Find goal and state links and add to function specification in node
        (dolist (l (node-links fn))
          (if (eq (link-var-node l) (predicate-outgoing-vn (predicate-goal-predicate pred))) ; Goal link
              (setq fun (cons l fun)) ; Add goal to front
            (when (eq (link-var-node l) (predicate-outgoing-vn pred)) ; State link
              (setq fun (append fun (list l)))
              ))
          )
        (setf (node-function fn) (cons 'difference-derivative fun))
        )
      )
    )
  )      

; Logistic derivative
(defun d-logistic (x)
  (* x (- 1 x))
  )

; Transform a function by x(1-x), by the derivative of the sigmoid/logistic function
(defun derivative-sigmoid-function (f r &optional piecewise-constant)
  r ; dummy so no warning about r not being used
  (if piecewise-constant
      (d-logistic f)
    (let ((nf (init-vector (length f) 0)))
      (setf (aref nf 0) (d-logistic (aref f 0))) ; Assuming no actual dimension weights
      nf))
  )

; Test if the link has a message that should be used in the product
(defun use-link-in-product (other-type froml tol)
  (and (not (eq froml tol)) ; Message is not from target link
       (aref (link-contents froml) other-type) ; There is a message (not being blocked by node being evidence)
       (or
        (null (link-excludes tol))
        (null (aref (link-excludes tol) other-type))
        (not (member froml (aref (link-excludes tol) other-type) :test #'eq)) ; Not a message to be excluded in computing this messages
        )
       )
  )

; Test if two variables are the same (name and type)
(defun same-variable (v1 v2)
  (and (eq (svariable-name v1) (svariable-name v2))
       (eq (stype-name (svariable-type v1)) (stype-name (svariable-type v2)))
       )
  )

; Do two vectors, when viewed as sets, contain the same elements?
(defun same-variables (vs1 vs2)
  (and (= (length vs1) (length vs2))
       (reduce #'(lambda (a b) (and a b)) (map 'vector #'(lambda (e) (find e vs2 :test #'same-variable)) vs1))
       )
  )

; Determine variables to use in tied gamma node
(defun tied-variables (c vs)
  (let (tied-c ; Conditional to which function here is tied
        tied-g ; Gamma node to which function here is tied
        )
    (setq tied-c (conditional-from-name (conditional-function c)))
    (unless (conditional-p tied-c)
      (error "Attempt to tie to nonexistant conditional ~S from conditional ~S!" (conditional-function c) (conditional-name c))
      )
    (setq tied-g (conditional-gamma-node tied-c))
    (unless (node-p tied-g)
      (error "Attempt to tie to non-gamma conditional ~S from conditional ~S!" (conditional-function c) (conditional-name c))
      )
    (unless (same-variables vs (node-variables tied-g))
      (error "Attempt to tie to conditional ~S with different gamma variables from conditional ~S!"  (conditional-function c) (conditional-name c))
      )
    (node-variables tied-g))
  )

; Modified functions
; ------------------

; Print a predicate
(defun print-predicate (p &optional current-function stream)
  (unless stream (setq stream trace-stream))
  (format stream "~&(PREDICATE '~S :WORLD '~S" (predicate-name p) (predicate-world p))
  (when (predicate-persistent p)
    (format stream " :PERSISTENT ~S" (predicate-persistent p))
    )
  (when (predicate-unique p)
    (format stream " :UNIQUE '~S" (predicate-unique p))
    (when (predicate-select p)
      (format stream " :SELECT '~S" (predicate-select p))
      )
    )
  (when (predicate-perception p)
    (format stream " :PERCEPTION T")
    )
  (when (predicate-exponential p)
    (format stream " :EXPONENTIAL ~S" (predicate-exponential p))
    )
  (when (predicate-replace p)
    (format stream " :REPLACE ~S" (predicate-replace p))
    )
  (when (predicate-vector p)
    (format stream " :VECTOR ~S" (predicate-no-normalize p))
    )
  (when (predicate-no-normalize p)
    (format stream " :NO-NORMALIZE ~S" (predicate-no-normalize p))
    )
  (when (predicate-arguments p)
    (format stream " :ARGUMENTS '~S" (predicate-arguments p))
    )
  (when (or (predicate-learning-rate p) (predicate-smoothing-parameter p))
    (format stream "~&   ")
    )
  (when (predicate-learning-rate p)
    (format stream " :LEARNING-RATE ~S" (predicate-learning-rate p))
    )
  (when (predicate-smoothing-parameter p)
    (format stream " :SMOOTHING PARAMETER ~S" (predicate-smoothing-parameter p))
    )
  (when (predicate-function p)
    (format stream "~&    :FUNCTION ")
    (let ((fun (if current-function (plm-cpt (node-function (predicate-function-node p))) (predicate-function p))))
      (unless (numberp fun)
        (format stream "'")
        )
      (format stream "~S" fun)
      )
    )
  (format stream ")")
  t)

; Print a conditional
(defun print-conditional (c &optional current-function reordered-conditions stream)
  (unless stream (setq stream trace-stream))
  (let (fun)
    (format stream "~&(CONDITIONAL '~S" (conditional-name c))
    (if reordered-conditions
        (when (conditional-reordered-conditions c)
          (print-patterns (conditional-reordered-conditions c) 'conditions))
      (when (conditional-conditions c)
        (print-patterns (conditional-conditions c) 'conditions)))
    (when (conditional-condacts c)
      (print-patterns (conditional-condacts c) 'condacts))
    (when (conditional-actions c)
      (print-patterns (conditional-actions c) 'actions))
    (when (conditional-map c)
      (format stream "~&    :MAP T"))
    (when (conditional-neural c)
      (format stream "~&    :NEURAL T"))
    (when (conditional-vector c)
      (format stream "~&    :VECTOR T"))
    (when (and (conditional-function-variable-names c) (not (conditional-forward-conditional c)))
      (format stream "~&    :FUNCTION-VARIABLE-NAMES '~S" (conditional-function-variable-names c)))
    (when (conditional-normal c)
      (format stream "~&    :NORMAL '~S" (conditional-normal c)))
    (when (conditional-learning-rate c)
      (format stream "~&    :LEARNING-RATE ~S" (conditional-learning-rate c))
      )
    (when (conditional-smoothing-parameter c)
      (format stream "~&    :SMOOTHING PARAMETER ~S" (conditional-smoothing-parameter c))
      )
    (when (and (conditional-function c) (not (conditional-forward-conditional c)))
      (format stream "~&    :FUNCTION ")
      (setq fun (if current-function (reusable-conditional-function-c c) (conditional-function c)))
      (unless (numberp fun)
        (format stream "'")
        )
      (format stream "~S" fun)
      )
    (when (conditional-forward-conditional c)
      (format stream "~&    :FORWARD-CONDITIONAL '~S" (conditional-forward-conditional c)))
    (when (conditional-exclude-forward-backward c)
      (format stream "~&    :EXCLUDE-FORWARD-BACKWARD T"))
    (format stream "~&    )~%~%")
    )
  )

; Determine if there are no empty incoming messages at a factor node
(defun no-empty-incoming-messages (node)
  (let ((empty nil))
    (dolist (l (node-links node))
      (when (and (link-var-content l) (not (plm-empty (link-var-content l))))
        (setq empty t)
        (return empty)
        )
      )
    empty)
  )

; Learn at a node via gradient descent
(defun update-via-gradient (node)
  (let (new
        (normald (node-normal node)) ; The dimension to normalize over, if any
        min-new ; Smallest value in weighted gradient
        (tgdl (trace-gdl node))
        wg ; Weighted gradient
        smooth ; The value to use for smoothing
        smoothed ; Whether have done smooothing
        surprise-function
        (vector (node-vector node)) ; A vector is being learned
        (neural (node-neural node)) ; A neural network is being learned
        pred ; Predicate for node
        pred-name ; Name of predicate
        known-function-node ; Function node for 'known' appraisal
        known-increment ; Increment to use for updating known
        ki-vs ; Active (universal) variables in known increment
        ki-original-vs ; Variables from original known-increment
        ki-active ; Active from known-increment
        uds ; Unique dimensions in gradient
        k
        )
    (setq smooth
          (if (node-smoothing-parameter node) ; Override
              (node-smoothing-parameter node)
            (if (and adaptive-smoothing normald)
                (/ .1 (+ (node-changes node) (if (numberp normald)
                                                 (stype-span (svariable-type (aref (node-variables node) normald)))
                                               (stype-spans (mapcar #'(lambda (x) (svariable-type (aref (node-variables node) x))) normald)))))
              smoothing-parameter)))
    (setq wg (cond (vector (compute-vector-gradient node smooth))
                   (neural (compute-neural-gradient node smooth))
                   (t (compute-weighted-gradient node smooth))
                   ))
    (unless (plm-empty wg)
      (setf (node-changes node) (1+ (node-changes node)))
      (setf pred (node-predicate node))
      ; Compute "known" appraisal
      (when (and compute-surprise pred (not (predicate-no-surprise pred)))
        (setf pred (node-predicate node))
        (setf pred-name (predicate-name pred))
        (setf known-function-node (predicate-function-node (node-known-predicate node)))
        (when trace-attention
          (format t "~&Gradient for input to KNOWN function for predicate ~S:~&" pred-name)
          (ps wg)
          (format t "~&Previous KNOWN function for predicate ~S:~&" pred-name)
          (ps (node-function known-function-node))
          )
        ; Determine which universal regions are to be changed based on gradient
        (setq known-increment wg)
        (setq uds (unique-dimensions known-increment t))
        (dolist (ud uds)
          (setq known-increment (maximize-plm known-increment ud))
          )
        ; Strip off inactive unique dimensions
        (setq ki-original-vs (plm-variables known-increment))
        (setq ki-active (plm-active known-increment))
        (setq ki-vs (init-vector (- (length ki-original-vs) (length uds))))
        (setq k 0) ; Counter of active variables
        (dotimes (i (length ki-original-vs))
          (when (aref ki-active i)
            (setf (aref ki-vs k) (aref ki-original-vs i))
            (setq k (1+ k))
            )
          )
        (setq known-increment (strip-and-reorder-plm (build-smap ki-vs (plm-variables wg)) ki-vs known-increment))
        (when trace-attention
          (format t "~&Increment for KNOWN function for predicate ~S:~&" pred-name)
          (ps known-increment)
          )
        ; Update known function
        (setf (node-function known-function-node) (combine-plms known-increment nil (node-function known-function-node) 'count))
        (when save-message-state
          (setf (aref (graph-changes cg) (node-number known-function-node)) t)
          )
        (when trace-attention
          (format t "~&New KNOWN function for predicate ~S:~&" pred-name)
          (ps (node-function known-function-node))
          )
        )
      (setq new (combine-plms (node-function node) nil wg (if exponential-product-gradient 'product 'sum)))
      (setq min-new (plm-extreme new #'< infinity))
      (when tgdl
        (format trace-stream "~&Updated function (unnormalized): ")
        (ps new)
        )
      (cond (vector ;only perform vector normalization, no need for smoothing and resmoothing 
             ; This would also now cause a problem for backprop, which uses vector gradients but shouldn't normalize
             ;(setq new (normalize-plm new normald t)) ;vector learning normalization required multiple iterations for learning, not normalizing learns quicker 
;             (when tgdl
;               (format trace-stream "~&Vector normalized updated function: ")
;               (ps new)
;               )
             )
            (neural
;             (when tgdl
;               (format trace-stream "~&Updated neural weight function: ")
;               (ps new)
;               )
             )
            (t ;default gradient descent case applies here if not processing vectors
             ; Need to shift whole gradient function up so that is above parameter (smoothing)
             (when (and (not neural) (< min-new smooth))
               (setq smoothed t)
                 (setq new (smooth-plm new smooth)) ; Shift up values below threshold
               (when tgdl
                 (format trace-stream "~&Smoothed (~S) unnormalized updated function: " smooth)
                 (ps new)
                 )
               )
             (when normald ; There are normalization dimensions
               ; Both here and below, must be able to use subtractive when no normalization dimension as well
               (if (and gdl-subtractive-normalization (not exponential-product-gradient))
                   (setq new (subtractive-normalize-plm new smooth normald))
                 (setq new (normalize-plm new normald t)))
               )
             (when tgdl
               (format trace-stream "~&Normalized ")
               (when smoothed
                 (format trace-stream "smoothed ")
                 )
               (format trace-stream "updated function: ")
               (ps new)
               )
             ; If normalization creates very small numbers (more than a factor of 10 smaller than parameter), redo smoothing and normalization
             (setq min-new (plm-extreme new #'< infinity))
             (when (and (not neural) (< min-new (* smooth .1))) ; Need to shift whole gradient function up so that is above parameter (smoothing)
               (setq new (smooth-plm new smooth))
               (when tgdl
                 (format trace-stream "~&Resmoothed updated function: ")
                 (ps new)
                 )
               (if normald ; There is a normalization dimension
                   (setq new (normalize-plm new normald t))
                 (setq new (normalize-over-all-dimensions-plm new t)))
               (when tgdl
                 (format trace-stream "~&Renormalized resmoothed updated function: ")
                 (ps new)
                 )
               )
             )
            )
      ; Constrain any regions that were 0 in the original function to remain 0
      (setq new (combine-plms (node-restriction node) nil new 'product))
      ; Update the surprise function (unless :no-surprise or not computing)
      (when (and (node-predicate node) (predicate-surprise-predicate (node-predicate node)))
        (setq surprise-function (hd-plms (node-function node) new (unique-dimensions (node-function node))))
        (setq surprise-function (strip-and-reorder-plm
                                 (build-smap (node-variables (predicate-perception (node-surprise-predicate node))) (plm-variables surprise-function))
                                 (node-variables (predicate-perception (node-surprise-predicate node))) surprise-function))
        (setq surprise-function (remove-unneeded-slices surprise-function))
        (when trace-attention
          (when surprise-function
            (format trace-stream "~&~%~S Surprise Map (Hellinger distance):~&" (predicate-name (node-predicate node)))
            (print-smart surprise-function t trace-stream)
            )
          )
        (setf (predicate-perception-temp-mem (node-surprise-predicate node)) surprise-function) ; Local surprise function
        )
      ; Update node function
      (setf (node-function node) new)
      ; Store the new function into all of the other CFF nodes that share the same function
      (dolist (shared (node-shared-functions node))
        (setf (node-function shared) new)
        )
      (when tgdl
        (format trace-stream "~&Function constrained by original zeros: ")
        (ps new)
        )
      (when save-message-state
        (setf (aref (graph-changes cg) (node-number node)) t)
        (when debug-descendants
          (format trace-stream "~&Changing ~S Function" (node-name node))
          )
        (dolist (shared (node-shared-functions node))
          (setf (aref (graph-changes cg) (node-number shared)) t)
          (when debug-descendants
            (format trace-stream "~&Changing ~S Function" (node-name shared))
            )
          )
        )
      )
    )
  )

; Compute the gradient for vector learning
(defun compute-vector-gradient (node smooth)
  (let* ((ls (node-links node))
         update ; Feedback to use in updating
         map
         ;(normal (node-normal node)) ; Index of the variable to normalize over, if any
         (nf (node-function node)) ; Previous factor function
         (vs (plm-variables nf))
         (tgdl (trace-gdl node))
         (lr (if (node-learning-rate node)
                 (node-learning-rate node)
               (if learning-rate-fraction-of-smoothing-parameter
                   (* learning-rate-fraction-of-smoothing-parameter smooth)
                 learning-rate)))
         (alr (if arousal (* arousal lr) lr))
         (wg (initialize-weighted-gradient vs alr))  ; Initialize plm with learning rate      
         ;(predicate-argument-unique-positions (init-vector (length vs))) ; Which variables (by position) are unique arguments for GDL in predicate
         ;(pred (node-predicate node)) ; Predicate for node
         )
    ; Set up update message, and map when necessary
    (if (= (length (node-links node)) 1)
        (progn
          (setq update (link-var-content (car ls))) ; Just use the incoming message
          (setq map (link-map (car ls)))
          )
      (setq update (links-function-update-message ls (node-variables node))))
    (when tgdl
      (format trace-stream "~&~%~%Vector gradient descent on node ~A with learning rate ~A" (node-name node) lr)
      (when arousal
        (format trace-stream " (and arousal rate of ~A, for an effective learning rate ~A)" arousal alr)
        )
      (format trace-stream ": ")
      (format trace-stream "~&Existing factor function: ")
      (pplm nf)
      (format trace-stream "~&~%Raw update function: ")
      (pplm update)
      (format trace-stream "~&~%")
      )
    ; Multiply gradient by learning rate
    (setq update (combine-plms update map wg 'product))
 
    (when tgdl
      (format trace-stream "~&Update function discounted by learning rate of ~S: " alr)
      (pplm update)
      )
    update
    )
)

; Is variable used uniquely in conditional?
(defun variable-unique-in-conditional (vn c)
  (or 
   (variable-unique-in-patterns vn (conditional-conditions c))
   (variable-unique-in-patterns vn (conditional-condacts c))
   (variable-unique-in-patterns vn (conditional-actions c)) ; Just added this.  Does it cause problems?
   )
  )

; Strip off any initial links at a variable node that are not to be used in product
(defun strip-front-useless-links (ls tol)
  (do ((link-list ls (cdr link-list)))
      ((null link-list) nil)
    (when (use-link-in-product fact-index (car link-list) tol) (return link-list))
    )
  )

; Alter graph so that there are no loops in condact paths across the listed conditionals
; Graph will not recirculate messages from one path along others
(defun single-logical-condact-path (cnames)
  (let (cpnames ; List of predicate names used in conditionals condacts
        cpvns ; List of WMVN nodes for condact predicates
        clinks ; List of links at NV node to condacts
        condn ; Conditional name for factor node on clink
        fn ; Factor node
        ocname ; Conditional associated with a condact on another link
        )
    (setq cpnames (reduce #'union (mapcar #'(lambda (cn) (mapcar #'car (conditional-condacts
                                                                        (let ((c (conditional-from-name cn)))
                                                                          (if c
                                                                              c
                                                                            (error "There is no conditional named ~S in SINGLE-LOGICAL-CONDACT-PATH!" cn))
                                                                          ))))
                                              cnames)))
    (setq cpvns (mapcar #'(lambda (cpn)
                            (let ((pred(predicate-from-name cpn)))
                              (if pred 
                                  (predicate-outgoing-vn pred)
                                (error "There is no predicate named ~S in SINGLE-LOGICAL-CONDACT-PATH!" (predicate-from-name cpn)))
                              )
                            )
                        cpnames))
    ; Process each WMVN
    (dolist (vn cpvns)
      (setq clinks nil)
      ; Determine links to condacts at WMVN
      (dolist (l (node-links vn))
        (when (eq (node-pattern-type (link-fact-node l)) 'condact)
          (push l clinks)
          )
        )
      ; Create lists of links not to include in computing the message on link l
      (dolist (l clinks)
        (setq condn (node-conditional-name (link-fact-node l)))
        (when (and condn (member condn cnames :test #'eq)) ; When there is a conditional name and it is in the list, look for exclusions
          ; Push all (condact) links for other conditionals on cnames onto exclude
          (dolist (ol clinks)
            (setq fn  (link-fact-node ol))
            (setq ocname (node-conditional-name fn))
            (when (and (member ocname cnames :test #'eq) ; Link from a conditional on list
                       (not (eq (node-conditional-name fn) condn)) ; Link from a different conditional
                       )
              (if (link-excludes l)
                  (push ol (aref (link-excludes l) fact-index))
                (setf (link-excludes l) (vector nil (list ol))))
              )
            )
          )
        )
      )
    )
  )

; List of names of factor nodes on excluded links from link
(defun link-exclusions (l index)
  (mapcar #'(lambda (e) (node-name (aref (link-nodes e) index))) (when (link-excludes l) (aref (link-excludes l) index)))
  )
(defun le (n1 n2 index)
  (link-exclusions (link-from-numbers n1 n2) index)
  )

; Process a message coming into a node
(defun incoming-message (m trace)
  (let* ((ml (message-link m)) ; Incoming messaage link
         (mi (message-index m)) ; Incoming message index
         (nmi (next-message-index mi)) ; Index for outgoing messages
         (in-from (link-node ml mi)) ; Source node for incoming message
         (in-to (link-node ml nmi)) ; Target node for incoming message
         out-to ; Target node for outgoing message
         out-message ; Outgoing message
         (wm-driven (message-wm-driven m)) ; Whether incoming message is wm-driven
         queue-message new-message
         (stale (aref (link-stale ml) mi))
        )
    (setq message-count (1+ message-count)) ; Increment global count of messages processed
    ; Compute message if not fresh and set it to fresh
    (when stale
      (setq new-message (outgoing-message m wm-driven trace))
      (setf (aref (link-stale ml) mi) nil)
      )
    (when (or (not stale) new-message)
      (when (or (eq trace t) ; If t, trace all messages
                (and trace ; If non-nil
                     (listp trace) ; and a list
                   ; And either node is in the list of nodes to trace
                     (or (member (node-name in-from) trace :test #'eq)
                         (member (node-number in-from) trace :test #'=)
                         (member (node-name in-to) trace :test #'eq)
                         (member (node-number in-to) trace :test #'=)
                         )
                     )
                )
        ; Then print the message
        (print-message (concatenate 'string
                                    (when trace-wm-driven (format nil "{~S}" (message-wm-driven m)))
                                    (format nil "<<~S<< " message-count)
                                    )
                       (message-link m) (message-index m) symbolic-trace t t trace-stream)
        )
      (dolist (l (node-links in-to)) ; Process all links at target node
        (setq out-to (link-node l mi)) ; Set target node for outgoing message
        (when (and
               (not (eq out-to in-from)) ; Not incoming link
               (link-content l nmi) ; The outgoing link is active in that direction
               (or (eq (node-subtype in-to) 'gamma) (not (node-assumption in-to)))
               (or (null (link-excludes l))
                   (null (aref (link-excludes l) mi))
                   (not (member ml (aref (link-excludes l) mi) :test #'eq)) ; Incoming message not on link excluded from output link
                   )
               )
          (setf (aref (link-stale l) nmi) t) ; Mark outgoing link as stale
          (setq queue-message (arc-in-queues l nmi (graph-queues cg)))
          (setq out-message (if queue-message (cadr queue-message) (make-message :link l :index nmi)))
          (setf (message-wm-driven m) wm-driven) ; If incoming messages is wm-driven, then so is outgoing
          (add-to-message-queue out-message)
          )
        )
      )
    t)
  )

; Compute depth of a link from a node in the graph
(defun compute-link-depth (l n q)
  (let* ((direction (if (variable-nodep n) 0 1))
         (opposite (- 1 direction))
         (links (node-links n))
         (depth -1) ; Depth when no incoming links should be 0
         il-depth ; Depth of incoming link
         depth+1
         ol
         )
    (unless (or (and (node-assumption n) (not (eq (node-subtype n) 'gamma))) ; Non-gamma assumption nodes
                (not (incoming-links n)) ; Nodes with no incoming links
                (= (length links) 1) ; If there is output, it can't be affected by input if there is only one link
                )
      (dolist (il links)
        (when (and (not (= (node-number (aref (link-nodes l) opposite)) ; Don't consider incoming message on link we're computing about
                           (node-number (aref (link-nodes il) opposite))))
                   (aref (link-contents il) opposite) ; Only consider active incoming links
                   (or
                    (null (link-excludes l))
                    (null (aref (link-excludes l) opposite))
                    (not (member il (aref (link-excludes l) opposite) :test #'eq)) ; Input link isn't being excluded from product
                    )
                   )
          (setq il-depth (aref (link-depths il) opposite))
          (if il-depth
              (when (> il-depth depth) (setq depth il-depth))
            (progn
              (setq depth nil)
              (return)
              )
            )
          )
        )
      ; Handle downward neural gamma link, which must be dependent on upward neural gamma link
      (when (and depth
                 (downward-neural-gamma-link l n) ; also check output on upward neural gamma link
                 )
        (setq ol (other-link n l))
        (when (aref (link-contents ol) direction) ; Only consider active upgoing outward link
          (setq il-depth (aref (link-depths ol) direction))
          (unless (and il-depth (<= il-depth depth))
            (setq depth il-depth)) ; Either a greater number or nil
          )
        )
      )
    (when depth
      (setq depth+1 (+ depth 1))
      (setf (aref (link-depths l) direction) depth+1)
      (when (> depth+1 (graph-depth cg)) (setf (graph-depth cg) depth+1))
      (add-to-queue (aref (link-nodes l) opposite) q)
      (when (and (eq (node-subtype n) 'gamma) ; Ths is a gamma node
                 (upward-neural-gamma-link l n) ; This is the message upward from the gamma node
                 )
        (add-to-queue n q) ; Add node back to queue so can check if can now assign depth to downward link
        )
      )
    q)
  )

; Create a delta factor
(defun create-delta-factor (c delta-vs c-varns alpha-memory previous-vn evidence-vn in out subsubtype pred not-equals)
  (let (split-delta-vs ; Rather than pairs of variables in each slot, a single vector with all variables
        vars ; List for split-delta-vs
        delta-node ; Delta node
        affines ; List of affine transforms from variable pairs
        v ; variable
        (v-count 0) ; variable count
        affine ; Affine used at position (may be a reused one from an earlier position)
        ai ; Index of reused from/pattern variable
        )
    (dotimes (i (length delta-vs))
      (setq v (aref delta-vs i))
      (when v
        (cond ((listp v)
               (cond ((symbolp (cadr v)) ; Reuse of a variable in a pattern
                      (when in
                        (error "Reuse of variables within a single condact or action, as in conditional ~S, is not currently allowed." (conditional-name c))
                        )
                      (setq vars (cons (car v) vars))
                      (setq ai (- v-count (position (cadr v) vars :key #'svariable-name)))
                      (dolist (a affines)
                        (when (= (affine-from a) ai)
                          (setq affine a)
                          )
                        )
                      (setf (affine-to affine) ; Add new WM/predicate variable to the "to" list (which is the predicate/WM variable)
                            (if (numberp (affine-to affine))
                                (list v-count (affine-to affine))
                              (cons v-count (affine-to affine)))
                            )
                      (setq v-count (+ v-count 1))
                      )
                     (t ; Map variable
                      (setq vars (cons (car v) (cons (cadr v) vars)))
                      (setq affines (cons (setq affine (make-affine :from v-count :to (+ v-count 1))) affines))
                      (setq v-count (+ v-count 2))
                      )
                     )
               )
              (t ; Variable stays the same
               (setq vars (cons v vars))
               (setq affines (cons (make-affine :to v-count) affines))
               (setq v-count (+ v-count 1))
               )
              )
        (when (and not-equals (aref not-equals i))
          (setf (affine-not-equal affine) (position (aref not-equals i) c-varns))
          )
        )
      )
    ; Add WM variables that aren't in patterns to list of variables (will be in variable node before delta)
;    (dotimes (i (length wmvs))
;      (unless (aref pattern-vs i)
;        (setq vars (cons (aref wmvs i) vars))
;        (setq affines (cons 
;        )
;      )
    (setq split-delta-vs (coerce (reverse vars) 'vector))
    ; Create delta factor for predicate pattern
    (when (or in out)
      (setq delta-node
            (init-factor-node (concat-symbols (cons (conditional-name c) (cons 'ADF c-varns)) t)
                              'affine
                              split-delta-vs
                              (list previous-vn alpha-memory)
                              (list evidence-vn) in out nil nil c subsubtype))
      (if pred
          (setf (node-region-pad delta-node) (if (open-world pred) 1 0))
        0)
      (setf (node-function delta-node) affines)
      (setf (node-pattern-type delta-node) (pattern-type in out))
      (setf (node-conditional-name delta-node) (conditional-name c))
      )
    delta-node)
  )

; Compile a conditional function
(defun compile-conditional-function (c g-node)
  (let* ((g-vars (node-variables g-node)) ; Variables in the gamma node
         (g-var-names (map 'list #'svariable-name g-vars)) ; Names of gamma variables
         (cf-var-names (conditional-function-variable-names c)) ; Names of function variables explicitly mentioned on conditional
         g-function ; Function for function factor
         (c-fun (conditional-function c)) ; The specified conditional function
         row-major ; Whether should use special row-major initialization (an all discrete function with a full set of values)
         (c-norm (conditional-normal c)) ; Variable name(s) for the dimension(s) over which to normalize, if any
         (normalize-function t) ; Set the default to normalize the function if there is a normalization variables
         other-c ; Conditional from which function is to be copied
         other-fn ; CFF with function to copy
         cf-index ; Index of gamma variable in conditional function variables, if any
         cf-map ; Map of gamma variables onto conditional function variables
         )
    ; Check for special case of a row-major function
    (when (and (consp c-fun) (eq (car c-fun) 'row-major))
      (setq c-fun (cdr c-fun))
      (setq row-major t)
      )
    ; General settings
    (setf (node-subsubtype g-node) 'function)
    (setf (node-assumption g-node) t)
    (setf (node-function-name g-node) (conditional-name c))
    (setf (conditional-function-node c) g-node)
    ; Map conditional variables onto gamma variables
    (dolist (gvn g-var-names)
      (setq cf-index (position gvn cf-var-names))
      (push (if cf-index cf-index nil) cf-map)
      )
    (setq cf-map (nreverse cf-map))
    ; If the conditional function is a number, use it as a constant PLM
    ; If the conditional function is a symbol, assume it is the name of another conditional whose function is to be shared
    ; Otherwise assume it specifies a full PLM
    (cond ((numberp c-fun)
           (setq g-function (init-plm g-vars c-fun 0 (init-vector (length g-vars) t)))
           (setf (plm-piecewise-constant g-function) t)
           )
          ((symbolp c-fun) ; Copy function from another conditional
           (setq normalize-function nil) ; Don't need to normalize function, as already normalized
           (setq other-c (conditional-from-name c-fun))
           ; If function variable names not specified in new conditional, copy from the old one
           (when (set-exclusive-or (conditional-function-variable-names c) (conditional-function-variable-names other-c)) ; Not same set of variables
             (error "Function variable names ~S in conditional ~S not the same as ~S in conditional to which function is to be tied ~S"
                    (conditional-function-variable-names c) (conditional-name c) (conditional-function-variable-names other-c) (conditional-name other-c))
             )
           (unless (equal (conditional-normal c) (conditional-normal other-c))
             (error "Function normal ~S in conditional ~S not the same as ~S in conditional to which function is to be tied ~S"
                    (conditional-normal c) (conditional-name c) (conditional-normal other-c) (conditional-name other-c))
             )
           (setq other-fn (conditional-function-node other-c))
           (unless other-fn
             (error "No CFF for conditional ~S as needed given specification in conditional ~S" c-norm (conditional-name c))
             )
                             ; Add shared function node, and all nodes it shares with to list of shared function nodes
           (setf (node-shared-functions g-node) (cons other-fn (node-shared-functions other-fn)))
                             ; Add current node to list of shared function nodes for node shared and everyone it shares with
           (setf (node-shared-functions other-fn) (cons g-node (node-shared-functions other-fn)))
           (dolist (cousin (node-shared-functions other-fn))
             (setf (node-shared-functions cousin) (cons g-node (node-shared-functions cousin)))
             )
           (setq g-function (copy-conditional-function (node-function other-fn) g-vars))
           )
          (row-major
           (setq c-fun (expand-conditional-function c-fun cf-map))
           (setq g-function (rml-plm c-fun g-vars t))
           (setf (plm-piecewise-constant g-function) t)
           )
          (t
           (setq c-fun (expand-conditional-function c-fun cf-map))
           (setq g-function (cpt-function-array-plm g-var-names c-fun g-vars
                                                    (if (conditional-function-default c) (conditional-function-default c) function-default) t))
           (setf (plm-piecewise-constant g-function) (notany #'(lambda (x) (consp (car x))) c-fun)) ; No linear regions in specification
           )
          )
    (setf (node-restriction g-node) (transform-plm #'boolean-function g-function)) ; Function that constrains learning to 0s where initial function is 0
    (setf (node-normal g-node) nil)
    (when c-norm
      (setf (node-normal g-node) (if (symbolp c-norm)
                                     (position c-norm g-var-names)
                                   (mapcar #'(lambda (cx) (position cx g-var-names)) c-norm)))
      (when normalize-function ; Avoid renormalizing when it is a function copied from another conditional
        (setq g-function (normalize-plm g-function (node-normal g-node) t))
        )
      )
    (setf (node-neural g-node) (conditional-neural c))
    (setf (node-function g-node) g-function)
    (setf (node-conditional-name g-node) (conditional-name c))
    (setf (node-changes g-node) 1) ; Initialize number of changes made to fucntion via learning to 0
    ; Set the node's learning rate
    (if (conditional-learning-rate c) ; Use value specified in conditional if specified
        (progn
          (unless (numberp (conditional-learning-rate c))
            (error "Learning rate specified for conditional ~S is not a number" (conditional-name c))
            )
          (if (not (or (conditional-normal c) learn-no-normal (e= (conditional-learning-rate c) 0)))
              (error "Learning rate specified for a conditional with no normal variable when learn-no-normal not T: ~S" (conditional-name c))
            (setf (node-learning-rate g-node) (conditional-learning-rate c)))
          )
      (when adaptive-learning-rate ; Otherwise use the adaptive rate if active
        (setf (node-learning-rate g-node) (/ 0.3 (function-span g-node)))
        ))
    ; Set the node's smoothing parameter
    (when (conditional-smoothing-parameter c) ; Use value specified in conditional if specified
      (unless (numberp (conditional-smoothing-parameter c))
        (error "Learning rate specified for conditional ~S is not a number" (conditional-name c))
        )
      (setf (node-smoothing-parameter g-node) (conditional-smoothing-parameter c))
      )
    g-function)
  )

; Compile conditional c into a graph
(defun compile-conditional (c)
  (let ((conditions (conditional-conditions c))
        (condacts (conditional-condacts c))
        (c-name (conditional-name c)) ; Conditional name
        caf-later-variable-names ; Variable names used in condacts, actions and function
        alpha-beta-memories ; The variable nodes to connect to the shared "gamma" factor node
        gamma-vars ; Variables to use in the gamma factor node
        gamma-node ; Gamma factor node that ties all of the pieces together
        condition-memory-l ; List of memory at end of condition chain
        forward-conditional ; If this conditional is part of a backward arc, this is the corresponding foward conditional
        forward-pregamma-vn ;  If this conditional is part of a backward arc, this is the variable node before the forward gamma
        forward-backward-link ; Link from forward chain to backward gamma
;        cross-adv ; Variable node after cross adf
;        delta-vs ; Variables for cross adf
        c-fun ; The specified conditional function
        other-c ; Conditional from which function is to be copied
        )
    ; Signal an error if try to redefine a conditional (reuse a conditional name)
    ; Can't presently delete a conditional from graph, so can't redefine
    ; Instead need to reload entire graph at this point
    (when (conditional-from-name c-name)
      (error "Conditional ~S already exists in graph." c-name)
      )
    ; Verify predicate and argument names in patterns
    (verify-patterns conditions "condition" c-name)
    (verify-patterns condacts "condact" c-name)
    (verify-patterns (conditional-actions c) "action" c-name)
    ; Determine list of conditional's function variable names if not specified explicitly
    (function-variable-names c)
    ; Determine lists of condition and condact variables for use in deciding how to do universal summarization
    (when conditions
      (setf (conditional-condition-variable-names c) (all-variable-names conditions))
      )
    (when condacts
      (setf (conditional-condact-variable-names c) (all-variable-names condacts))
      )
    ; Set up conditional function from tied conditional
    (when (conditional-forward-conditional c)
      (when (conditional-function c)
        (format t "Warning: Redefining function for conditional ~S with forward conditional ~S."
                c-name (conditional-name (conditional-from-name (conditional-forward-conditional c))))
        )
      (setf (conditional-function c) (conditional-forward-conditional c))
      )
    (setq c-fun (conditional-function c))
    ; Copy conditional variable names from tied function if there is one
    (when (and c-fun (symbolp c-fun))
      (setq other-c (conditional-from-name c-fun))
      (unless other-c
        (error "No conditional named ~S, as specified in the :FUNCTION for conditional ~S" c-fun (conditional-name c))
        )
      ; If function variable names not specified in new conditional, copy from the old one
      (unless (conditional-function-variable-names c)
        (setf (conditional-function-variable-names c) (conditional-function-variable-names other-c))
        )
      )
    ; Determine variable names used after conditions (i.e., in condacts, actions and function) 
    (setq caf-later-variable-names (conditional-function-variable-names c))
    (when (conditional-actions c)
      (setq caf-later-variable-names (union (union-lists (later-variable-names (conditional-actions c) nil t)) caf-later-variable-names))
      )
    (when (conditional-condacts c)
      (setq caf-later-variable-names (union (union-lists (later-variable-names (conditional-condacts c) nil t)) caf-later-variable-names))
      )
    ; Reorder conditions
    (when automatically-reorder-conditions
      (when (> (length conditions) 1)
        (setq conditions (reorder-conditions conditions caf-later-variable-names))
        )
      (setf (conditional-reordered-conditions c) conditions)
      )
    ; Record the conditional on the graph
    (setf (graph-conditionals cg) (cons c (graph-conditionals cg)))
    ; Compile the conditions, condacts and actions
    (when (conditional-conditions c)
      (setq alpha-beta-memories (compile-patterns c 'conditions conditions (later-variable-names conditions caf-later-variable-names) t nil))
      (setq condition-memory-l alpha-beta-memories) 
      )
    (setf (conditional-last-condition-memory c) (conditional-last-memory c))  ;VOLKAN conditional last condition memory is the last memory, not the last alpha memory. 
    (when (conditional-condacts c)
      (setq alpha-beta-memories (append (compile-patterns c 'condacts (conditional-condacts c) caf-later-variable-names t t) alpha-beta-memories))
      )
    (when (conditional-actions c)
      (setq alpha-beta-memories (append (compile-patterns c 'actions (conditional-actions c) caf-later-variable-names nil t) alpha-beta-memories))
      )
    ; If there is a forward conditional (i.e., we're in the backward portion of an NN or abductive arc)
    (when (conditional-forward-conditional c)
      (setq forward-conditional (conditional-from-name (conditional-forward-conditional c)))
      (unless forward-conditional (error "No forward conditional ~S in COMPILE-CONDITIONAL!" forward-conditional))
      (setq forward-pregamma-vn (pregamma-vn forward-conditional))
      ; Add variable node feeding forward gamma to backward gamma (assumes conditions but no condacts in forward rule)
      (setq alpha-beta-memories (cons forward-pregamma-vn alpha-beta-memories))
      )
    ; Create gamma node variable vector
    (setq gamma-vars (reduce #'merge-variable-vectors (mapcar #'node-variables alpha-beta-memories)))
    (when (and (conditional-function c) (symbolp (conditional-function c))) ; This is a tied function
      (setq gamma-vars (tied-variables c gamma-vars))
      )
    ; Create gamma node to connect all of the graph fragments together
    (setq gamma-node (init-factor-node (concat-symbols (cons c-name (cons 'GF (variable-names gamma-vars))) t)
                                       'gamma
                                       gamma-vars
                                       alpha-beta-memories
                                       condition-memory-l t t nil nil c))
    (setf (conditional-gamma-node c) gamma-node)
    ; If there is a forward conditionall (i.e., were in the backward portion of an NN or abductive arc)
    (when (conditional-forward-conditional c)
      ; Set cross link to be one directional toward backward gamma
      (setq forward-backward-link (link-with-vn gamma-node forward-pregamma-vn))
      (setf (link-in forward-backward-link) t)
      (setf (link-out forward-backward-link) nil)
      ; Exclude cross link in computing messages out of backward gamma if there is a backward gamma link and are excluding
      (when (conditional-exclude-forward-backward c)
        (dolist (l (node-links gamma-node))
          (when (eq (node-pattern-type (link-var-node l)) 'action) ; Downward link is an action
            (if (link-excludes l)
                (push forward-backward-link (aref (link-excludes l) var-index))
              (setf (link-excludes l) (vector (list forward-backward-link) nil)))
            )
          )
        )
      )
    (if (conditional-function c)
        (progn
          ; Determine gamma function based on the conditional function
          (setf (node-function gamma-node) (compile-conditional-function c gamma-node))
          ; Determine whether to use distribution or vector gradient
          (setf (node-vector gamma-node) (conditional-vector c))
          )
      ; Set gamma function to a uniform 1
      (setf (node-function gamma-node) (full-plm gamma-vars)))
    )
  )

; Create an outgoing message from a variable node
; Just compute product of incoming messages (except for from outgoing node)
; can exponential predicates be vector predicates? Exponential predicates are not checked for vector normalization x
(defun outgoing-var-fact-message (m wm-driven)
  (let* ((ml (message-link m))
         (lc (link-var-content ml)) ; Message to send to factor node
         (vn (link-var-node ml)) ; Variable node from which message is sent
         (fn (link-fact-node ml)) ; Factor node to which message is sent
         (ls (node-links vn)) ; Links from variable node to factor nodes
         prod
         new-message ; Whether generate new message
         product-computed
         )
    ; Dynamically reorder links for product optimization
    (when dynamic-variable-product-ordering
      (setq ls (reorder-variable-product ls))
      )
    ; Instead of initializing prod to the 1 array and doing an extra product,
    ; initialize prod to first PLM not from the "to" node and not nil (i.e., from an evidence factor)
    (setq ls (strip-front-useless-links ls ml))
    (when ls ; When there are inputs to include in the product (node has more than one neighbor)
      (setq prod (link-fact-content (car ls))) ; Initialize product to message from first input
      (setq ls (cdr ls))
      ; Compute the product of the messages in prod (always of same rank so no offset)
      (dolist (l ls)
        (when (use-link-in-product fact-index l ml)
          (setq prod
;                (remove-unneeded-slices
                 (combine-plms (link-fact-content l) nil prod 'product (wm-nodep vn)))
          (unless product-computed (setq product-computed t))
        )
;          )

        )
      ; Exponentiate messages out of WM VN nodes for exponential predicates
      (when (node-exponential vn)
        (when (node-normalize vn)
          (setq prod (normalize-plm prod nil product-computed))
          )
        (setq prod (transform-plm #'exponentiate-constant-times10-function prod nil nil product-computed))
        )
      ; Apply attention to messages out of shared WM VN nodes
      (when (and (node-apply-attention vn)
                 (not (eq (node-subtype fn) 'function)) ; Message not to LTMFN
                 (not (eq (node-subtype fn) 'wm))
                 )
        (setq prod (apply-attention (node-predicate vn) prod))
        )
      ; This normalizes most outgoing messages from WM VN nodes
      (when (and (node-normalize vn)
                 (not (and
                       (graph-selected-predicate cg) ; There is not a selected predicate
                       (equal (predicate-wm (graph-selected-predicate cg)) fn) ; or not going to the SELECTED WMFN (for decisions and impasse detection)
                       ))
                 (not (eq (node-subtype fn) 'pass-through)) ; This is not a predictive pass through node
                 (not (eq (node-subtype fn) 'function)) ; Don't normalize messages going to predicate function nodes
                 )
        (setq prod
              (if (node-vector vn) 
                  (normalize-plm prod nil product-computed t) 
                (normalize-plm prod nil product-computed))
              ) 
        ) 
      (setq prod (remove-unneeded-slices prod))
      ; If new PLM is epsilon-different from old
      (cond ((plm-e= lc prod arousal) ; Handle cases where initial message not changed, but it should be piecewise constant
             (unless (eq (plm-piecewise-constant lc) (plm-piecewise-constant prod))
               (setf (plm-piecewise-constant lc) (plm-piecewise-constant prod))
               )
             )
            (t
             (setf (aref (link-contents ml) var-index) prod)
             (unless (wm-nodep fn)
               (setf (message-wm-driven m) wm-driven) ; If incoming messages is wm-driven, then so is outgoing
               )
             (setq new-message t)
             )
            )
      )
    new-message)
  )

; Create an outgoing message from a factor node
; Need to multiply all incoming messages (except for from outgoing node)
; times the node's function and integrate/maximize out all but the outgoing variables.
; Integrates/maximizes out a variable right after the function has been multiplied by it
(defun outgoing-fact-var-message (m wm-driven)
  (let* ((ml (message-link m)) ; The link for the message
         (lc (link-fact-content ml)) ; Existing message to send to variable node
         (fn (link-fact-node ml)) ; Factor node from which message is sent
         (vn (link-var-node ml)) ; Variable node to which message is sent
         (vn-vars (node-variables vn)) ; The variables used at the variable node
         (sum-prod (node-function fn)) ; Cumulatively computed message content
         (steps (node-factor-steps fn)) ; The steps to be taken to generate message
         (skip-product (and (beta-factor fn) (plm-full (node-function fn)))) ; Skip inital product for beta factors with a function of 1 under right circumstances
         (first-product t) ; Whether product is first one performed
         new-message ; Whether generate a new message
         s
         )
    (cond ((eq (node-subtype fn) 'transform) ; Factor function is a function to be computed on input message rather than PLM
           (setq sum-prod (transform-message sum-prod (other-incoming-link fn ml) ml (node-normalize fn)))
           )
          ((eq (node-subtype fn) 'combine) ; Factor function does a one-way functional combination of inputs
           (setq sum-prod (combine-messages fn (node-subsubtype fn)))
           )
          ((eq (node-subtype fn) 'affine) ; Factor function computes a tranform on the PLM slices
            (setq sum-prod (affine-message sum-prod (other-link fn ml) ml (node-variables fn) (other-unidirectional-incoming-link fn (other-link fn ml)))) ;updated by VOLKAN
           )
          ((eq (node-subtype fn) 'explicit) ; Factor converts implicit functional value into an explicit domain value along new action variable
           (setq sum-prod (explicit-plm (aref (link-contents (other-incoming-link fn ml)) var-index) (cadr (node-function fn))))
           )
          ((eq (node-subtype fn) 'function) ; Function factor node that maintains a constant message across the decision
           (setq sum-prod lc)
           )
          ((eq (node-subtype fn) 'difference) ; Function factor node that computes the difference (derivative) of goal - state
           (setq sum-prod (combine-plms (aref (link-contents (cadr sum-prod)) var-index) ; Goal message plm
                                        nil
                                        (aref (link-contents (caddr sum-prod)) var-index) ; State message plm
                                        'difference))
           )
          ((and (eq (node-subtype fn) 'gamma) ; Downwards neural gamma link
                (downward-neural-gamma-link ml fn)
                )
           (setq sum-prod (neural-backwards-gamma-message ml fn))
           )
          (t
           ; Summarize across the product of the messages
           (do ((ss steps (cdr ss)))
               ((null ss))
             (setq s (car ss))
             (let ((sl (factor-step-argument s))) ; The link (product) or variable number (integrate/maximize) for this step
               (if (eq (factor-step-type s) 'product)
                   (when (use-link-in-product var-index sl ml)
                     (setq sum-prod
;                           (remove-unneeded-slices
                            (combine-plms (link-var-content sl) (link-map sl) sum-prod 'product)
;                            )
                           )
                     (setq first-product nil)
                     )
                 (unless (variables-members (aref (node-variables fn) sl) vn-vars) ; Variable is not part of target VN
                   (setq sum-prod
;                         (remove-unneeded-slices
                         (case (factor-step-type s)
                           ((integral) (integral-plm sum-prod sl))
                           ((product-integral) (product-integral-plm sum-prod sl))
                           (t (maximize-plm sum-prod sl))
                           )
;                          )
                         )
                   )
                 )
               )
             )
           ; Determine when to use an inverse filter (padding with 1s)
           (when (and (eq (node-subtype fn) 'filter)
                      (or (eq (node-subsubtype fn) 'inverse-both) ; Condacts that are are using the inverse filter in both directions
                          (and (eq (node-subsubtype fn) 'inverse-in) (member vn (node-evidence fn) :test #'eq)) ; Incoming condact or action (with open-actions-like-condacts T)
                          (and (eq (node-subsubtype fn) 'inverse-out) (not (member vn (node-evidence fn) :test #'eq))) ; Outgoing condition (with open-conditions-like-condacts T)
                          )
                      )
             (setq sum-prod (combine-plms sum-prod nil (node-inverse-function fn) 'por))
             )
           ; Copy node function if a beta factor and there are no incoming messages
           (when (and skip-product first-product)
             (setq sum-prod (copy-a-plm sum-prod))
             )
           ; Strip PLM of dimensions not part of PLM for variable node and reorder variables/dimensions as needed
           (unless (link-variables-same ml)
             (setq sum-prod (strip-and-reorder-plm (link-map ml) vn-vars sum-prod))
             )
           (setq sum-prod (remove-unneeded-slices sum-prod))
           )
          )
    ; If new PLM is epsilon-different from old, set it and add message to queue
    (cond ((plm-e= lc sum-prod arousal) ; Handle cases where initial message not changed, but it should be piecewise constant
           (unless (eq (plm-piecewise-constant lc) (plm-piecewise-constant sum-prod))
             (setf (plm-piecewise-constant lc) (plm-piecewise-constant sum-prod))
             )
           )
          (t
           (setf (aref (link-contents ml) fact-index) sum-prod)
           (setf (message-wm-driven m) wm-driven) ; If incoming messages is wm-driven, then so is outgoing
           (setq new-message t)
           )
          )
    new-message)
  )

; Set up a goal (and a progress, difference and attention predicates) for a predicate
(defun goal (pred-name goal)
  (let ((pred (predicate-from-name pred-name))
        (goal-predicate-name (concat-symbols (list pred-name '*goal)))
        )
    (setf (predicate-goal-predicate pred) (predicate goal-predicate-name
                                                     :world (predicate-world pred)
                                                     :unique (predicate-unique pred)
                                                     :select (predicate-select pred) 
                                                     :replace (predicate-replace pred)
                                                     :episodic (predicate-episodic pred)
                                                     :no-normalize (predicate-no-normalize pred)
                                                     :perception (predicate-perception pred)
                                                     :arguments (predicate-arguments pred)))
    ; Initialize goal function (which should be empty at this point)
    (unless (eq goal t) ; Just leave goal at 0 if specified as T
      (if (numberp goal)
          (evidence `((,goal-predicate-name ,goal)) nil t)
        (apply #'evidence (list goal nil t))
        )
      )
    (when compute-progress
      ; At this point, only compute progress and difference for distributions
      (when (and (predicate-unique pred) (not (predicate-no-normalize pred)))
      ; Create progress predicate
        (setf (predicate-progress-predicate pred)
              (if (closed-world pred)
                  (predicate (concat-symbols (list pred-name '*progress)) :perception t :arguments (meta-arguments pred)
                             :no-surprise t :no-normalize t :function 1
                             )
                (predicate (concat-symbols (list pred-name '*progress)) :perception t :arguments (meta-arguments pred)
                           :no-surprise t :function 1
                           )))
        ; Create difference predicate
        (setf (predicate-difference-predicate pred)
              (if (closed-world pred)
                  (predicate (concat-symbols (list pred-name '*difference)) :perception t :arguments (meta-arguments pred)
                             :no-surprise t :no-normalize t :function 1
                             )
                (predicate (concat-symbols (list pred-name '*difference)) :perception t :arguments (meta-arguments pred)
                           :no-surprise t :function 1
                           )))
        ; Set up global progress
        (setf (graph-progress-predicates cg) (cons pred (graph-progress-predicates cg)))
        (unless (graph-progress-distribution-predicate cg)
          (setf (graph-progress-distribution-predicate cg) (predicate 'progress*distribution :perception t :no-normalize t
                                                                      :arguments (if multiagent '((agent agent) (value predicate %)) '((value predicate %)))))
          (setf (graph-progress-predicate cg) (predicate 'progress :perception t :no-normalize t :arguments (if multiagent '((agent agent)) '())))
          )
        ; Set up global difference
        (setf (graph-difference-predicates cg) (cons pred (graph-difference-predicates cg)))
        (unless (graph-difference-distribution-predicate cg)
          (setf (graph-difference-distribution-predicate cg) (predicate 'difference*distribution :perception t :no-normalize t
                                                                        :arguments (if multiagent '((agent agent) (value predicate %)) '((value predicate %)))))
          (setf (graph-difference-predicate cg) (predicate 'difference :perception t :no-normalize t :arguments (if multiagent '((agent agent)) '())))
          )
        ; Create an attention predicate if not already one
        (when (and compute-attention (not (predicate-attention-predicate pred)))
          (setf (predicate-attention-predicate pred)
                (predicate (concat-symbols (list pred-name '*attention)) :no-surprise t :perception t :function 1
                           :arguments (meta-arguments pred)))
                 ; Set up global attention
          (setf (graph-attention-predicates cg) (cons pred (graph-attention-predicates cg)))
          (unless (graph-attention-distribution-predicate cg)
            (setf (graph-attention-distribution-predicate cg) (predicate 'attention*distribution :perception t :no-normalize t
                                                                         :arguments (if multiagent '((agent agent) (value predicate %)) '((value predicate %)))))
            (setf (graph-attention-predicate cg) (predicate 'attention :perception t :no-normalize t :arguments (if multiagent '((agent agent)) '())))
            )
          )
        )
      ; Create difference derivative predicate
      (setf (predicate-difference-derivative-predicate pred)
            (if (closed-world pred)
                (predicate (concat-symbols (list pred-name '*difference-d))
                           :arguments (if (predicate-select pred)
                                          (make-unique-arguments-distribution (predicate-arguments pred))
                                        (predicate-arguments pred))
                           :no-normalize t)
              (predicate (concat-symbols (list pred-name '*difference-d))
                         :arguments (predicate-arguments pred)
                         :no-normalize t)))
      )
    )
  )

; Processing to be done after all conditionals are defined
; Ideally this would be done incrementally, at least as much as possible, rather than redoing everything, but that is left to the future
(defun post-process-conditionals nil
  (let ((pre-time-init-node (get-internal-run-time))
        message-time
        )
    (add-difference-derivatives)
    (include-prediction-links)
    (excise-inactive-links)
    (when max-messages-links
      (setq max-messages (* max-messages-links (length (graph-links cg))))
      )
    (expand-densely-connected-variable-nodes) ; Replace densely connected nodes with binary trees of nodes
    (init-messages t) ; Initialize messages and compute depths
    (setq message-time (/ (- (get-internal-run-time) pre-time-init-node) 1000))
    (when debug-init-descendants
      (format trace-stream "~&Message and Queue Initialization took ~f seconds" message-time)
      )
    (init-descendants)
    (when debug-init-descendants
      (format trace-stream "~&Descendant Initialization took ~f seconds"
              (- (/ (- (get-internal-run-time) pre-time-init-node) 1000) message-time))
      )
    (excise-unneeded-factor-steps)
    (init-node-vector)
    (predicate-function-unique-arguments)
    (assign-variables-same)
    )
  )

; Create a transform factor (must be an inverse at this point)
(defun create-transform-factor (c type t-vars previous-vars next-vn previous-vn evidence-vn in out subsubtype)
  (unless (member type '(invert negate exponentiate sigmoid relu tanh exp) :test #'eq)
    (error "Unknown transform type ~S in conditional ~S." type (conditional-name c))
    )
  (let (transform-node)
    (when (or in out)
      (setq transform-node
            (init-factor-node (concat-symbols (list (conditional-name c) (case type
                                                                           ((invert) 'IF) ((negate) 'NF) ((exponentiate) 'EF)
                                                                           ((sigmoid) 'SF) ((relu) 'RF) ((tanh) 'THF)
                                                                           ((exp) 'XF))) t)
                              'transform
                              (transform-factor-variables previous-vars t-vars)
                              (if (consp previous-vn)
                                  (append previous-vn (list next-vn))
                                (list previous-vn next-vn))
                              (if (consp evidence-vn)
                                  evidence-vn
                                (list evidence-vn))
                              in out nil nil c subsubtype))
      (setf (node-function transform-node) (case type
                                             ((invert) #'invert-function)
                                             ((negate) #'negate-function)
                                             ((exponentiate) #'exponentiate-constant-times10-function)
                                             ((sigmoid) (list #'sigmoid-function (if (conditional-neural c) #'inverse-sigmoid-function #'derivative-sigmoid-function)))
                                             ((relu) (list #'relu-function (if (conditional-neural c) #'inverse-relu-function #'derivative-relu-function)))
                                             ((tanh) (list #'tanh-function (if (conditional-neural c) #'inverse-tanh-function #'derivative-tanh-function)))
                                             ((exp) (list #'exp-function (if (conditional-neural c) #'inverse-exp-function #'derivative-exp-function)))
                                             ))
      )
    ; For an exponential transform, normalize the result
; To work, needs some way of saying which variable to normalize over if there are only multiple variables.
;    (setf (node-normalize transform-node) (case type ((invert) nil) ((exponentiate) t)) )
    transform-node)
  )

; Create an alpha network for a pattern
(defun create-alpha-network (pp c in out)
  (let* ((p-name (car pp)) ; Predicate name for pattern
         (cond-name (conditional-name c))
         (pred (predicate-from-name p-name)) ; Predicate for pattern
         (open (open-world pred))
         (p-args (predicate-arguments pred)) ; Arguments of the predicate
         (wmfn (predicate-wm pred)) ; Predicate WM FN
         (wmvs (predicate-wm-variables pred)) ; Vector of predicate WM's variables
         iwmvs ; Vector of variables to use in a transformed alpha path
         wmvn ; Starts with WM VN, updated to variable node after constant tests
         reduced-wmvn ; Variable node at the output of a constant test
         shared-wmvn ; VN to share after a filter function
         offset-vn ;Volkan
         offset-fn ;Volkan
         wmvn-vs ; Variables in variable node prior to constant test
         (reduced-vs (copy-seq wmvs)) ; Copy of predicate WM's variables, to be updated as go through elements
         (wmvs-rank (length wmvs)) ; Rank of predicate
         evnum ; Index of element in WM FN
         ev ; WM variable corresponding to the element
         e-arg-name ; Argument name
         e-content ; Argument contents
         e-type ; Argument type
         e-rest ; Stuff after arguent name and content
         c-vn ; Name of variable in element, if it is a variable test
         c-v ; Variable for element, if it is a variable test
         c-varns ; List of variable names in pattern
         c-vars ; List of variables in pattern
         c-vars-v ; Vector of variables in pattern
         o-vars-v ;Volkan Vector of variables for the offset variable node
         (c-vars-i (init-vector wmvs-rank)) ; Vector for mapping from predicate variables to conditional variables
         (c-vars-d (init-vector wmvs-rank)) ; Boolean vector for which predicate variables used in delta factor
         (c-vars-e (init-vector wmvs-rank)) ; Boolean vector for which predicate variables have pattern elements (nil means ignored by pattern)
         (original-pp pp) ; Used for error message
         negated ; Whether pattern is negated
         delta-vs ; Variables for delta node
         aff
         affines
         affine-node
         (action-offset-variables '())
         action-from-variable;VOLKAN
         condact-variable-offset ;VOLKAN
         coef coef-position
         (filters (init-vector wmvs-rank)) ; Filters on elements of pattern
         (not-equals (init-vector wmvs-rank)) ; Not-equal tests on elements of pattern
         (variable-exists (init-vector wmvs-rank)) ; Which WM variables have conditional variables
         filter-fn filter-node
         afilter ; There is a filter along at least one dimension
         linear-filter ; There is a linear filter
         transform-node ; Transform factor node (for negated actions)
         explicit-node ; Explicit factor node (for making function explicit along a dimension in an action)
         outgoing-vn ; Shared link for finding WM VN for condacts
         share ; Conditions for sharing VN from conditions/condacts with actions
         exponential ; Whether this is an exponential condition
         sigmoid ; Whether this is a nonlinear (sigmoid) pattern
         relu ; Whether this is a nonlinear (relu) pattern
         tanh ; Whether this is a nonlinear (tanh) pattern
         exp ; Whether this is a nonlinear (exp) pattern
         action ; Whether pattern is for an action
         new-action-var ; Whether variable is a newly introduced action variable
         explicit-var ; Name of variable that is newly introduced in action (and not used elsewhere in action) and marked as :explicit
         beta-not-equal-vars ; Not-equal variable pairs when other variable is in a previous condition
         (pattern-type (pattern-type in out)) ; Whether is a condition, action or condact
         )
    ; Generate error if can't find predicate name among predicates
    (unless pred (error "Predicate ~S in conditional ~S is undefined." p-name cond-name))
    ; Strip off predicate name from predicate pattern
    (setq pp (cdr pp))
    ; Determine if pattern is negated (negation must come before alpha variable if exists)
    (when (and (listp pp) (eq (car pp) negation-symbol))
      (setq negated t)
      (setq pp (cdr pp))
      )
    ; Determine if pattern is an exponentiated condition
    (when (and (listp pp) (eq (car pp) exponential-symbol)
               out (not in))
      (setq exponential t)
      (setq pp (cdr pp))
      )
    ; Determine if pattern is nonlinearly transformed via a sigmoid
    (when (and (listp pp) (eq (car pp) sigmoid-symbol))
      (setq sigmoid t)
      (setq pp (cdr pp))
      )
    ; Determine if pattern is nonlinearly transformed via a relu
    (when (and (listp pp) (eq (car pp) relu-symbol))
      (setq relu t)
      (setq pp (cdr pp))
      )
    ; Determine if pattern is nonlinearly transformed via a tanh
    (when (and (listp pp) (eq (car pp) tanh-symbol))
      (setq tanh t)
      (setq pp (cdr pp))
      )
    ; Determine if pattern is nonlinearly transformed via an exp
    (when (and (listp pp) (eq (car pp) exp-symbol))
      (setq exp t)
      (setq pp (cdr pp))
      )
    (setq share (or (not (predicate-persistent pred)) ; Conditions for sharing VN from conditions/condacts with actions
                    (and (not (predicate-select pred))
                         (or (predicate-universal pred)
                             (predicate-cumulative pred)
                             )
                         )
                    ))
    ; Deal with sharing
    (cond ((or (and in
                    (or out ; Condact sharing
                        (and open-actions-like-condacts open) ; Sharing open-world actions when behave like condacts
                        )
                    )
               (and out open-conditions-like-condacts) ; Treat open-world conditions like condacts for connectivity
               )
           ; If there is a WM VN attached to the WM FN, use it; otherwise create a new one
           (setq outgoing-vn (predicate-outgoing-vn pred))
           (if outgoing-vn
               (progn
                 (setq wmvn outgoing-vn)
                 (setf (node-pattern-type outgoing-vn) 'condact)
                 )
             (progn
               (setq wmvn (create-wm-variable-node pred wmfn
                                                   (concat-symbols `(,p-name shared wm-vn) t)
                                                   ; condacts-change-wm
                                                   nil
                                                   wmvs t t nil out (predicate-exponential pred))
                     )
               )
             )
           )
          ((and in (not out)) ; Actions
           (setq wmvn (add-action pred wmfn ; Will return a list of two nodes for closed-world selection predicate
                                  (if share 'shared 'action)
                                  (concat-symbols (list cond-name p-name) t)
                                  (concat-symbols (elements-constants pp) t)
                                  negated (predicate-exponential pred) wmvs c
                                  share
                                  (predicate-persistent pred)
                                  open))
           (setq action t)
           )
          ((and (not in) out) ; Conditions
           (setq wmvn (add-condition pred wmfn
                                             (concat-symbols `(,p-name shared wm-vn) t)
                                             (concat-symbols (list cond-name p-name) t)
                                             (concat-symbols (elements-constants pp) t)
                                             wmvs c (predicate-exponential pred)
                                             share)
                 )
           )
          )
    ; If condact is negated, error
    (when (and in out negated)
      (error "Negation is not allowed in condacts, as used in pattern ~S of conditional ~S." pp (conditional-name c))
      )
    ; If action is negated, insert transform (inversion or negation) factor and variable node
    (when (and in (not out) negated)
      (setq iwmvs (transform-variables wmvs)) ; Replace WM vars with transformed/inverse vars
      (setq reduced-wmvn
            (init-variable-node
             (concat-symbols (list cond-name p-name 'action-neg-wm-vn) t)
             'inversion nil nil iwmvs t pattern-type))
      (setq transform-node (create-transform-factor c (if (or (predicate-vector pred) (predicate-no-normalize pred)) 'negate 'invert) iwmvs wmvs reduced-wmvn wmvn wmvn in out nil))
      (setf (node-pattern-type transform-node) pattern-type)
      (setf (node-conditional-name transform-node) cond-name)
      (when (and (eq p-name 'selected) ; Action for selected
                 in ; With next, detect that this is an action
                 (not out)
                 )
        (setf (graph-negative-preferences cg) (cons transform-node (graph-negative-preferences cg)))
        )
      (setq wmvn reduced-wmvn) ; Make new variable node the one to build on for rest of alpha path
      (setq wmvs iwmvs) ; Use inverted vars for rest of alpha path
      (setq reduced-vs (copy-seq iwmvs)) ; Copy negated vars for use in alpha path
      )
    ; If pattern is exponentiated, insert transform/exponentiate factor and variable node
    (when exponential
      (setq iwmvs (transform-variables reduced-vs)) ; Replace WM vars with transformed vars
      (setq reduced-wmvn
            (init-variable-node
             (concat-symbols (list cond-name p-name 'EV) t)
             'exponential nil nil iwmvs t pattern-type))
      (setq transform-node (create-transform-factor c 'exponentiate iwmvs reduced-vs reduced-wmvn wmvn wmvn in out nil))
      (setf (node-pattern-type transform-node) pattern-type)
      (setf (node-conditional-name transform-node) cond-name)
      (when (predicate-unique pred)
        (setf (node-normalize transform-node) t)
        )
      (setq wmvn reduced-wmvn) ; Make new variable node the one to build on for rest of alpha path
      (setq wmvs iwmvs)
;      (setq wmvs (transform-variables wmvs)) ; Use transformed vars for rest of alpha path [Use this form rather than previous if after dolist on elements]
      (setq reduced-vs iwmvs) ; Transformed vars for use in rest of alpha path
      )
    ; If pattern is sigmoid, insert transform/sigmoid factor and variable node
    (when sigmoid
      (setq iwmvs (transform-variables reduced-vs)) ; Replace WM vars with transformed vars
      (setq reduced-wmvn
            (init-variable-node
             (concat-symbols (list cond-name p-name 'SV) t)
             'sigmoid nil nil iwmvs t pattern-type))
      (setq transform-node (create-transform-factor c 'sigmoid iwmvs reduced-vs reduced-wmvn wmvn wmvn in out nil))
      (setf (node-pattern-type transform-node) pattern-type)
      (setf (node-conditional-name transform-node) cond-name)
      (when (predicate-unique pred)
        (setf (node-normalize transform-node) nil)
        )
      (setq wmvn reduced-wmvn) ; Make new variable node the one to build on for rest of alpha path
      (setq wmvs iwmvs)
      (setq reduced-vs iwmvs) ; Transformed vars for use in rest of alpha path
      )
    ; If pattern is relu, insert transform/relu factor and variable node
    (when relu
      (setq iwmvs (transform-variables reduced-vs)) ; Replace WM vars with transformed vars
      (setq reduced-wmvn
            (init-variable-node
             (concat-symbols (list cond-name p-name 'RV) t)
             'relu nil nil iwmvs t pattern-type))
      (setq transform-node (create-transform-factor c 'relu iwmvs reduced-vs reduced-wmvn wmvn wmvn in out nil))
      (setf (node-pattern-type transform-node) pattern-type)
      (setf (node-conditional-name transform-node) cond-name)
      (when (predicate-unique pred)
        (setf (node-normalize transform-node) nil)
        )
      (setq wmvn reduced-wmvn) ; Make new variable node the one to build on for rest of alpha path
      (setq wmvs iwmvs)
      (setq reduced-vs iwmvs) ; Transformed vars for use in rest of alpha path
      )
    ; If pattern is tanh, insert transform/tanh factor and variable node
    (when tanh
      (setq iwmvs (transform-variables reduced-vs)) ; Replace WM vars with transformed vars
      (setq reduced-wmvn
            (init-variable-node
             (concat-symbols (list cond-name p-name 'THV) t)
             'tanh nil nil iwmvs t pattern-type))
      (setq transform-node (create-transform-factor c 'tanh iwmvs reduced-vs reduced-wmvn wmvn wmvn in out nil))
      (setf (node-pattern-type transform-node) pattern-type)
      (setf (node-conditional-name transform-node) cond-name)
      (when (predicate-unique pred)
        (setf (node-normalize transform-node) nil)
        )
      (setq wmvn reduced-wmvn) ; Make new variable node the one to build on for rest of alpha path
      (setq wmvs iwmvs)
      (setq reduced-vs iwmvs) ; Transformed vars for use in rest of alpha path
      )
    ; If pattern is exp, insert transform/exp factor and variable node
    (when exp
      (setq iwmvs (transform-variables reduced-vs)) ; Replace WM vars with transformed vars
      (setq reduced-wmvn
            (init-variable-node
             (concat-symbols (list cond-name p-name 'XV) t)
             'exp nil nil iwmvs t pattern-type))
      (setq transform-node (create-transform-factor c 'exp iwmvs reduced-vs reduced-wmvn wmvn wmvn in out nil))
      (setf (node-pattern-type transform-node) pattern-type)
      (setf (node-conditional-name transform-node) cond-name)
      (when (predicate-unique pred)
        (setf (node-normalize transform-node) nil)
        )
      (setq wmvn reduced-wmvn) ; Make new variable node the one to build on for rest of alpha path
      (setq wmvs iwmvs)
      (setq reduced-vs iwmvs) ; Transformed vars for use in rest of alpha path
      )


    ; Process elements of pattern, handling constant and variable tests (and affine transforms)
    (dolist (element pp)
      (setq e-rest (element-rest element)) ; Stuff after element argument and content
      (when (and e-rest ; Syntax error if stuff after argument and content, and
                 (or (not (weight-list (car e-rest))) ; First part of rest is not a weight vector, or
                     (cdr e-rest))) ; There is even more after the weight vector
        (error "Extra stuff in element ~S of pattern ~S in conditional ~S"
               element original-pp (conditional-name c)))
      (setq evnum (element-wm-index element pred)) ; Index of element in predicate WM
      (unless evnum
        (error "Argument ~S not defined in predicate ~S, but used in conditional ~S"
               (car element) p-name (conditional-name c))
        )
      (setq ev (aref wmvs evnum)) ; WM variable corresponding to the element
      (setq wmvn-vs (node-variables wmvn)) ; Variables in variable node prior to constant test
      (setq e-arg-name (element-argument-name element)) ; Argument name
      (setq e-content (element-content element)) ; Argument contents
      (setq e-type (type-from-predicate-argument e-arg-name pred)) ; Argument type
      (cond ((variable-element e-content) ; Process a variable element
             (setf (aref variable-exists evnum) t)
             ; Get name of variable from element
             (setq c-vn (car e-content))
             ; Get existing conditional variable for name if it exists
             (setq c-v (variable-from-name c-vn (conditional-variables c)))
             ; Signal error if type of condtional variable not same as type of pattern argument
             (when (and c-v (not (equal (stype-name e-type) (stype-name (svariable-type c-v)))))
               (error "Type mismatch in conditional ~S; Variable ~S used for both types ~S and ~S!"
                      cond-name (svariable-name c-v) (stype-name e-type) (stype-name (svariable-type c-v)))
               )
             (unless c-v ; Conditional variable does not already exist, so create it
               (setq c-v (make-svariable :name c-vn
                                         :type e-type
                                         :unique (variable-unique-in-conditional c-vn c)
                                         ))
               (setf (conditional-variables c) (cons c-v (conditional-variables c)))
               (when action
                 (setq new-action-var t)
                 )
               )
             (cond ((member c-vn c-varns :test #'eq) ; Conditional variable already used in pattern (within pattern variable equality testing)
                    (setf (aref c-vars-i (position e-arg-name p-args :key #'car)) c-vn) ; Signal that predicate variable maps to existing pattern variable
                    (setq new-action-var nil)
                    )
                   (t ; Conditional variable is new for this pattern
                    (setq c-varns (cons c-vn c-varns)) ; List of conditional variable names in pattern
                    (setq c-vars (cons c-v c-vars)) ; List of conditional variables in pattern 
                    (setf (aref c-vars-i (position e-arg-name p-args :key #'car)) c-v)
                    )
                   )
             (setf (aref c-vars-d evnum) t)
             (setf (aref c-vars-e evnum) t)
             (dolist (variable-modifier (cdr e-content))
               (cond ((numberp variable-modifier) ; There is an offset (special case of an affine transform)
                      ; Convert offset into affine
                      (setq aff (make-affine :offset variable-modifier :to c-vn :from c-vn))
                      (setq affines (cons aff affines))
                      )
                     ((affine-listp variable-modifier) ; An affine transform
                      (setq coef-position (position ':coefficient variable-modifier))
                      (when coef-position
                        (setq coef (nth (+ coef-position 1) variable-modifier))
                        (when (= coef 0)
                          (error "Coefficient in affine transform in conditional ~S must not be 0."
                                 (conditional-name c))
                          )
                        )
                      (setq aff (apply #'make-affine variable-modifier))
                      (setf (affine-to aff) c-vn)
                     
                     (if (affine-from aff)
                          ;(when (and in (not out)) ; An action
                         (when in ;not condition
                            (error "Use the condact/condition variable ~S directly in condact/action ~S of conditional ~S instead of using a new condact/action variable (~S) in conjunction with :FROM ~S."
                                   (affine-from aff) pp (conditional-name c) (affine-to aff) (affine-from aff))      ;  Volkan: Allowing different :from and :to variables create problems while applying coefficients                              
                            )
                        (setf (affine-from aff) (affine-to aff)) ; "From" defaults to "to"
                        )
                      (unless (eq e-type ; Type of :to variable
                                  (svariable-type (variable-from-name (affine-to aff) c-vars))
                                  )
                        (error "Mismatched variable types ~S and ~S in affine transform ~S in conditional ~S."
                               (stype-name e-type)
                               (stype-name (svariable-type (variable-from-name (affine-to aff) c-vars)))
                               aff (conditional-name c))
                        )
                       (when (and (affine-offset aff) (symbolp (affine-offset aff))) ; A variable is used as the offset
                        (if (and (not in) out) ; This is a condition ;VOLKAN
                            (error "Variables not currently allowed as offsets in conditions: Pattern ~A in conditional ~S." original-pp (conditional-name c))
                          (progn  
                            (setq action-offset-variables (append action-offset-variables (list (variable-from-name (affine-offset aff) (conditional-variables c)))))
                            (setq action-from-variable (variable-from-name (affine-from aff) (conditional-variables c)))
                            (if (and in out) (setf condact-variable-offset t)) ;this is a condact and there is a variable offset
                            )                                              
                          )
                        ) 
                       
                       (setq affines (cons aff affines))
                      )
                     ((filter-listp variable-modifier) ; A filter on the variable
                      (setf (aref filters evnum) (create-element-plm ev (cdr variable-modifier) 0)) ; Add filter in its place in the vector
                      (setq afilter t)
                      (unless (e= (filter-coef variable-modifier) 0)
                        (setq linear-filter t)
                        )
                      )
                     ((eq variable-modifier ':explicit)
                      (if new-action-var
                          (setq explicit-var (svariable-name ev))
                        (error "Attempt to generate an explicit distribution over variable (~S) that isn't a new action variable in pattern ~S of conditional ~S"
                               c-vn original-pp (conditional-name c))
                        )
                      )
                     ((not-equal-listp variable-modifier) ; A not-equal (<>) test on a prior variable
                      (unless (stype-discrete e-type)
                        (error "Argument ~S is not discrete in a not-equal (<>) test in pattern ~S in conditional ~S"
                               e-arg-name original-pp cond-name)
                        )
                      (when (member (cadr variable-modifier) c-varns :test #'eq) ; This is a within-pattern not-equal (<>) test
                        (unless (eq e-type (type-from-predicate-argument (argument-from-variable (cadr variable-modifier) pp) pred))
                          (error "Variables ~S (~S) and ~S (~S) are not of the same type in a not-equal (<>) test in pattern ~S in conditional ~S"
                                 c-vn (stype-name e-type) (cadr variable-modifier)
                                 (stype-name (type-from-predicate-argument (argument-from-variable (cadr variable-modifier) pp) pred))
                                 original-pp cond-name)
                          )
                        (unless (not in)
                          (error "Within-pattern not-equal (<>) test in an action or condact (~S) in conditional ~S"
                                 original-pp cond-name)
                          )
                        )
                      (if (member (cadr variable-modifier) c-varns :test #'eq)
                          (setf (aref not-equals evnum) (cadr variable-modifier)) ; Unequal to a variable in the same pattern
                        (setq beta-not-equal-vars (cons (list c-vn (cadr variable-modifier)) beta-not-equal-vars))) ; Unequal to a variable in a previous pattern
                      )
                     ((eq variable-modifier not-equal-symbol)
                      (error "Not-equal symbol (<>) is not first element of a list after variable ~S in pattern ~S of conditional ~S" c-vn original-pp cond-name)
                      )
                     )
               )
             )
            ((and (or in out) (constant-element e-content)) ; Process a constant/filter element if messages are passing
             (setf (aref filters evnum) (create-element-plm ev (if (filter-listp e-content) ; A filter
                                                                   (cdr e-content) ; Remove :filter from front
                                                                 (list (list e-content 1 0))) ; Turn into filter data
                                                            )
                   )
             (setf (aref c-vars-e evnum) t)
             (setq afilter t)
             (when (and (filter-listp e-content)
                        (not (e= (filter-coef e-content) 0)))
               (setq linear-filter t)
               )
             ; Remove WM variable for current constant from list
             (setq reduced-vs (remove ev reduced-vs))
             )
            )
      )
    (when (or afilter ; There is at least one variable with a filter
              (position nil c-vars-e)) ; or there is at least one WM variable not mentioned in the pattern
      (setq filter-fn (create-filter-function wmvs filters)) ; Create filter function
      (dotimes (i wmvs-rank)
        (unless (aref variable-exists i) ; There is no conditional variable for this WM variable
          (setq reduced-vs (remove (aref wmvs i) reduced-vs)) ; Don't include variable in resulting variable node
          )
        )
      ; If sharing filters, and this is a condition, see if there is an existing one to share
      (when (and share-condition-tests out (not in))
        (setq shared-wmvn (shared-filter-wmvn wmvn filter-fn reduced-vs))
        )
      ; If there is a shared filter, simply pick up with its subsequent variable node
      (if shared-wmvn
          (setq wmvn shared-wmvn)
        (progn
          ; Create variable node for output from filter test (and make it WM variable for next iteration)
          (setq reduced-wmvn
                (init-variable-node
                 (concat-symbols (list cond-name p-name 'fiv) t)
                 'filter nil nil reduced-vs t pattern-type))
          ; Create factor node for filter test
          (setq filter-node
                (init-factor-node (concat-symbols (list cond-name p-name 'fif) t)
                                  'filter
                                  wmvn-vs                  
                                  (list wmvn reduced-wmvn)
                                  (list wmvn) in out nil nil c))
          (setf (node-pattern-type filter-node) pattern-type)
          (setf (node-conditional-name filter-node) cond-name)
          (setf (node-function filter-node) filter-fn)
          ; Mark node has having a linear filter if linear-filter is true
          (when linear-filter
            (setf (node-linear-filter filter-node) t)
            )
          ; Set things up to invert open-world predicates through filters
          ; Always do for messages through condacts to WM
          ; Do for open-world actions if open-actions-like-condacts it T
          ; Do for messages through condacts from WM if all-condact-filters-pad-1 is T
          ; Do for open-world conditions of open-conditions-like-condacts and all-condact-filters-pad-1 are both T   
          (when (and open ; Open world
                     (or (and in out) ; condact
                         (and in open-actions-like-condacts) ; Open-world action to be treated like incoming part of condact
                         (and out open-conditions-like-condacts) ; Open-world condition to be treated like outgoing part of condact
                         )
                     )
            (setf (node-inverse-function filter-node) (transform-plm #'invert-function-variant filter-fn))
            (setf (node-subsubtype filter-node)
                  (cond ((and in out) ; condact
                         (if all-condact-filters-pad-1 'inverse-both 'inverse-in))
                        ((and in open-actions-like-condacts) ; Open-world action to be treated like incoming part of condact
                         'inverse-in)
                        ((and out open-conditions-like-condacts) ; Open-world condition to be treated like outgoing part of condact
                         'inverse-out)))
            )
          ; Set things up for processing after filter test(s)
          (setq wmvn reduced-wmvn)
          ))
      )
    ; If condition is negated, insert transform/inversion factor and variable node
    (when (and out (not in) negated)
      (setq iwmvs (transform-variables reduced-vs)) ; Replace WM vars with transformed/inverse vars
      (setq shared-wmvn (if share-condition-tests (shared-negation-wmvn wmvn) nil))
      (if shared-wmvn
          (setq wmvn shared-wmvn)
        (progn
          (setq reduced-wmvn
                (init-variable-node
                 (concat-symbols (list cond-name p-name 'wm-neg-vn) t)
                 'inversion nil nil iwmvs t pattern-type))
          (setq transform-node (create-transform-factor c 'invert iwmvs reduced-vs reduced-wmvn wmvn wmvn in out nil))
          (setf (node-pattern-type transform-node) pattern-type)
          (setf (node-conditional-name transform-node) cond-name)
          (setq wmvn reduced-wmvn) ; Make new variable node the one to build on for rest of alpha path
          ))
      (setq wmvs (transform-variables wmvs)) ; Use inverted vars for rest of alpha path
      (setq reduced-vs iwmvs) ; Negated vars for use in rest of alpha path
      )
    (when explicit-var
      (setq reduced-wmvn
            (init-variable-node (concat-symbols (cons (conditional-name c) (cons p-name (cons 'XV c-varns))) t)
                                'explicit nil nil reduced-vs t pattern-type))
      (setq explicit-node (init-factor-node (concat-symbols (cons (conditional-name c) (cons p-name (cons 'XF c-varns))) t)
                                              'explicit reduced-vs (list wmvn reduced-wmvn) (list wmvn) in out nil t c))
      (setf (node-function explicit-node) (list ':explicit (position explicit-var reduced-vs :key #'svariable-name)))
      (setf (node-pattern-type explicit-node) pattern-type)
      (setf (node-conditional-name explicit-node) cond-name)
      (setq wmvn reduced-wmvn)
      )
    (when c-vars ; If there are any variables create a delta factor
      ; Reverse the lists so get in good reading order for people (original order)
      (setq c-varns (reverse c-varns))
      (setq c-vars (reverse c-vars))
      ; Convert list of variables to vector of variables
      (setq c-vars-v (coerce c-vars 'vector))
      ; Create variables for delta node (paired previous/WM and pattern vars)
      (setq delta-vs (delta-variables (length (node-variables wmvn)) c-vars-d c-vars-i wmvs))
      (setq shared-wmvn (if (and share-condition-tests out (not in)) (shared-delta-wmvn wmvn c-vars-v) nil))
      (if shared-wmvn
          (setq wmvn shared-wmvn)
        (progn
          ; Create new memory for pattern after delta
          (setq reduced-wmvn
                (init-variable-node (concat-symbols (cons (conditional-name c) (cons p-name (cons 'ADV c-varns))) t)
                                    'delta nil nil c-vars-v t (pattern-type in out)))
          ; The new memory is currently the last memory in the conditional
          (setf (conditional-last-memory c) reduced-wmvn)
          ; Create delta factor
          (create-delta-factor c delta-vs c-varns reduced-wmvn wmvn wmvn in out 'match pred not-equals)
          (setq wmvn reduced-wmvn)
          ))
      )
    (when affines ; There is at least one variable with a transform
      
      ; If there is a separate from variable in an action, add it to the variables
       ; (when action-from-variable
      (when (not (eql (affine-from aff) (affine-to aff))) ; when there is a separate to affine-from variable, add it to the variables
        (setq c-vars-v (extend-vector c-vars-v action-from-variable))
        ;(setf (aref c-vars-v (position (affine-to aff) c-vars-v :key #'svariable-name)) action-from-variable)
        )

       ; If there is an offset variable in an action, add it to the variables
      (when (> (length action-offset-variables) 0)
        (setq c-vars-v (extend-vector c-vars-v action-offset-variables))
        )
    
      ; Convert :from and :to fields from pattern variable name to pattern variable numbers
      (dolist (aff affines)
        (setf (affine-from aff) (position (affine-from aff) c-vars-v :key #'svariable-name))
        (setf (affine-to aff) (position (affine-to aff) c-vars-v :key #'svariable-name))
        )
     
      ; Create variable node after transform factor node
      (setq reduced-wmvn
            (init-variable-node
             (concat-symbols (list cond-name p-name 'av) t)
             'affine nil nil 
             (if (eq (affine-to aff) (affine-from aff)) c-vars-v (reduce-vector c-vars-v (aref c-vars-v (affine-to aff))))
             t pattern-type))

      ; Create an offset-variable node and offset-factor node to handle in the variable offsets in condacts ;Volkan
      (when condact-variable-offset
        (setq o-vars-v c-vars-v)
         (dolist (aff affines)
           (setq o-vars-v (reduce-vector o-vars-v (aref c-vars-v (affine-to aff))))
           (setq o-vars-v (reduce-vector o-vars-v (aref c-vars-v (affine-from aff)))) ;VOLKAN need to check whether this works all the time.
           )  
         ;only unique variables are the offset variables
         (dotimes (i (length o-vars-v))
           (setf (svariable-unique (aref o-vars-v i)) nil)
           )
        (dotimes (i (length o-vars-v))
          (dolist (aff affines)
            (when (eq (svariable-name (aref o-vars-v i)) (affine-offset aff)) (setf (svariable-unique (aref o-vars-v i)) t))
            )
          )
        
        ;Handling offset variables in condact patterns ;Volkan
        ;create a variable node - factor node duo. Offset variable node is connected to the affine factor node 
        ;and the offset factor node. Offset factor node is connected to the last condition memory of the conditional
        ;and the offset variable node.

        ;create offset variable node
        (setq offset-vn 
            (init-variable-node
             (concat-symbols (list cond-name p-name 'ov) t)
             'offset nil nil o-vars-v t (pattern-type t nil)))
        ;create offset factor node
        (setq offset-fn (init-factor-node (concat-symbols (list cond-name p-name 'of) t) 
                                          'beta
                                          (node-variables (conditional-last-condition-memory c))
                                          (list  (conditional-last-condition-memory c) offset-vn)
                                          nil t nil nil nil c))
      (setf (node-pattern-type offset-fn) (pattern-type t nil))
      (setf (node-conditional-name explicit-node) cond-name)
        (when (predicate-unique pred)
          (setf (node-normalize offset-fn) t)
          )
        (setf (node-function offset-fn)   (full-plm (node-variables (conditional-last-condition-memory c))))  
       )
      ; Create transform factor node
      (setq affine-node
            (init-factor-node (concat-symbols (list cond-name p-name 'af) t)
                              'affine
                              c-vars-v
                              (if condact-variable-offset (list wmvn reduced-wmvn offset-vn) (list wmvn reduced-wmvn)) ;Volkan
                              (list wmvn) in out nil nil c))
      (setf (node-region-pad affine-node) (if (affine-pad aff) (affine-pad aff) (if open 1 0)))
      (setf (node-pattern-type affine-node) pattern-type)
      (setf (node-conditional-name affine-node) cond-name)
      (when (predicate-unique pred)
        (setf (node-normalize affine-node) t)
        )
      (setf (node-function affine-node) affines)
      (setq wmvn reduced-wmvn)
      )
    (unless (eq (node-subtype wmvn) 'wm)
      (setf (node-subtype wmvn) 'alpha)
      )
    (setf (node-subsubtype wmvn) (if in (if out 'condact 'action) 'condition))
    (setf (conditional-alpha-memories c) (cons wmvn (conditional-alpha-memories c)))
    (cons wmvn beta-not-equal-vars)
    )
  )

; Compute progress/desirability and attention for predicates
(defun compute-goal-progress nil
  (dolist (pred (graph-predicates cg))
    (let ((goal-pred (predicate-goal-predicate pred))
          (progress-pred (predicate-progress-predicate pred))
          (difference-pred (predicate-difference-predicate pred))
          (surprise-pred (predicate-surprise-predicate pred))
          (attention-pred (predicate-attention-predicate pred))
          pred-function ; Function used for predicate in computing progress
          goal-function ; Predicates goal function
          divisor-function ; Used to scale progress and difference by total mass of goal (so total is fraction of complete goal achievement)
          constraint-function ; Used in determing which portions of difference to ignore depending on parts of goals don't care about (i.e., parts that are 0)
          progress-function difference-function attention-function goal-based-function surprise-function
          bc-plm ; BC coefficient
          hd-plm ; HD coefficient
          unique-ds ; Unique dimensions of predicate
          nvs
          )
      (when (and progress-pred (not (predicate-vector pred))) ; Temporary until add similarity/difference measures for vector predicates
        (setq unique-ds (unique-dimensions-predicate pred)) 
        (setq pred-function (if (open-world pred) (vn-posterior pred) (node-function (predicate-wm pred))))
        (setq goal-function (node-function (predicate-wm goal-pred)))
        (when trace-attention
          (when goal-function
            (format trace-stream "~&~%~S Goal Function:~&" (predicate-name pred))
            (print-smart goal-function t trace-stream)
            )
          (when pred-function
            (format trace-stream "~&~%~S Working Memory Function:~&"(predicate-name pred))
            (print-smart pred-function t trace-stream)
            )
          )
        (setq divisor-function (summarize-plm goal-function (all-but-agent-and-state-dimensions-plm goal-pred) 'integral #'vector-sum))
        ; Compute similarity/progress
        (setq bc-plm (bc-plms pred-function goal-function unique-ds))     
        ; Compute difference
        (setq hd-plm (hd-from-bc-plm bc-plm))
        (when trace-attention
          (when bc-plm
            (format trace-stream "~&~%~S Progress (Bhattacharyya Coefficient):~&" (predicate-name pred))
            (print-smart bc-plm t trace-stream)
            )
          (when hd-plm
            (format trace-stream "~&~%~S Difference (Hellinger Distance):~&" (predicate-name pred))
            (print-smart hd-plm t trace-stream)
            )
          )
         ; Constrain differences to 0 when no goal in region
        (setq constraint-function (integral-plm goal-function unique-ds))
        (setq hd-plm (combine-plms hd-plm nil constraint-function 'constrain-by-0))
        (when trace-attention
          (when constraint-function
            (format trace-stream "~&~%~S Constraint function that enables ignoring 0s in the Goal:~&" (predicate-name pred))
            (print-smart constraint-function t trace-stream)
            )
          (when hd-plm
            (format trace-stream "~&~%~S Asymmetric difference ignoring 0s in the Goal:~&" (predicate-name pred))
            (print-smart hd-plm t trace-stream)
            )
          (when divisor-function
            (format trace-stream "~&~%~S Goal mass (for scaling progress and difference):~&" (predicate-name pred))
            (print-smart divisor-function t trace-stream)
            )
          )
        ; Set up progress function
        (setq nvs (node-variables (predicate-perception progress-pred)))
        (setq progress-function
              (strip-and-reorder-plm (build-smap nvs (plm-variables bc-plm))
                                     nvs
                                     (combine-plms bc-plm ; Could be cheaper
                                                   nil
                                                   divisor-function
                                                   'divide-0)))
        (setq progress-function (remove-unneeded-slices progress-function))
        (when trace-attention
          (when progress-function
            (format trace-stream "~&~%~S Progress Map:~&" (predicate-name pred))
            (print-smart progress-function t trace-stream)
            )
          )
        (setf (predicate-perception-temp-mem progress-pred) progress-function) ; Local progress function
        ; Set up difference function
        (setq nvs (node-variables (predicate-perception difference-pred)))
        (setq difference-function
              (strip-and-reorder-plm (build-smap nvs (plm-variables hd-plm))
                                     nvs
                                     (combine-plms hd-plm ; Could be cheaper
                                                   nil
                                                   divisor-function
                                                   'divide-0)))
        (setq difference-function (remove-unneeded-slices difference-function))
        (when trace-attention
          (when difference-function
            (format trace-stream "~&~%~S Difference Map:~&" (predicate-name pred))
            (print-smart difference-function t trace-stream)
            )
          )
        (setf (predicate-perception-temp-mem difference-pred) difference-function) ; Local difference function
        )
      ; Compute attention
      (when (and attention-pred (not (predicate-vector pred))) ; Temporary until add similarity/difference measures for vector predicates
        ; Use normalized difference or progress function, if exists, depending on whether predicate is closed or open world
        (when (and (closed-world pred) difference-function)
          (setq goal-based-function difference-function)
          (when (and (predicate-arguments difference-pred) (not (predicate-no-normalize difference-pred)))
            (setq goal-based-function (normalize-plm goal-based-function))
            )
          (when trace-attention
            (when goal-based-function
              (format trace-stream "~&~%~S Difference Map (Normalized):~&" (predicate-name pred))
              (print-smart goal-based-function t trace-stream)
              )
            )
          )
        (when (and (open-world pred) progress-function)
          (setq goal-based-function progress-function)
          (when (and (predicate-arguments progress-pred) (not (predicate-no-normalize progress-pred)))
            (setq goal-based-function (normalize-plm goal-based-function))
            )
          (when trace-attention
            (when goal-based-function
              (format trace-stream "~&~%~S Progress Map (Normalized):~&" (predicate-name pred))
              (print-smart goal-based-function t trace-stream)
              )
            )
          )
        (if surprise-pred
            (if (setq surprise-function (predicate-perception-temp-mem surprise-pred)) ; Check that there is a perceived surprise function
                (progn
                  (setq surprise-function (normalize-plm surprise-function))
                  (when trace-attention
                    (when surprise-function
                      (format trace-stream "~&~%~S Surprise Map (Hellinger distance) (Normalized):~&" (predicate-name pred))
                      (print-smart surprise-function t trace-stream)
                      )
                    )
                  (setq attention-function (if goal-based-function
                                               (combine-plms surprise-function nil goal-based-function 'por)
                                             surprise-function))
                  )
              (setq attention-function goal-based-function))
          (setq attention-function goal-based-function))
        (when attention-function
          (setq attention-function (remove-unneeded-slices attention-function))
          (when trace-attention
            (when attention-function
              (format trace-stream "~&~%~S Attention Map ~S:~&"
                      (predicate-name pred)
                      (if surprise-pred
                          (if progress-pred
                              (if (closed-world pred)
                                  "Surprise POR Difference"
                                "Surprise POR Progress")
                            "Surprise")
                        (if (closed-world pred)
                            "Difference"
                          "Progress")))
              (print-smart attention-function t trace-stream)
              )
            )
          (setf (predicate-perception-temp-mem attention-pred) attention-function) ; Local attention function
          )
        )
      )
    )
  )

; Test functions
; --------------

; Replication of feedforward network from http://www.doc.ic.ac.uk/~sgc/teaching/pre2012/v231/lecture13.html
; Full arc with learning
(defun tff1arc-init ()
  (init)
  (setq compute-progress t)
  (learn '(:gd))
  (setq learning-rate .05)
;  (setq trace-messages '(20))
;  (setq trace-gdl t)
;  (setq trace-messages t)
;  (setq trace-transform t)

  (new-type 'n2 :numeric t :discrete t :min 0 :max 2)
  (new-type 'n3 :numeric t :discrete t :min 0 :max 3)

  (predicate 'input :world 'open :arguments '((arg n3 [])) :no-normalize t :perception t)
  (predicate 'hidden :world 'open :arguments '((arg n2 [])) :no-normalize t)
  (predicate 'output :world 'open :arguments '((arg n2 [])) :no-normalize t :goal t)

  ; Predicates for abductive reasoning
;  (predicate 'input*abduct :arguments '((arg n3 [])) :no-normalize t)
  (predicate 'hidden*abduct :arguments '((arg n2 [])) :no-normalize t)

  (conditional 'c-layer1
               :conditions '((input (arg (i))))
               :actions '((hidden s (arg (h))))
               :vector t
               :function-variable-names '(i h)
               :function `((,(random-weight) 0 0) (,(random-weight) 0 1)
                           (,(random-weight) 1 0) (,(random-weight) 1 1)
                           (,(random-weight) 2 0) (,(random-weight) 2 1))
               )

  (conditional 'c-layer2
               :conditions '((hidden (arg (h))))
               :actions '((output s (arg (o))))
               :vector t
               :function-variable-names '(h o)
               :function `((,(random-weight) 0 0) (,(random-weight) 0 1)
                           (,(random-weight) 1 0) (,(random-weight) 1 1))
               )

  (conditional 'c-layer2-i
               :conditions '((output*difference-d (arg (o)))
                             (output s (arg (o))))
               :actions '((hidden*abduct (arg (h))))
               :vector t
               :forward-conditional 'c-layer2
               :exclude-forward-backward t
               )

  (conditional 'c-layer1-i
               :conditions '((hidden*abduct (arg (h)))
                             (hidden s (arg (h))))
;               :actions '((input*abduct (arg (i))))
               :vector t
               :forward-conditional 'c-layer1
               :exclude-forward-backward t
               )

  t)

; Train tff1rc over multiple examples
(defun train-tff1arc (cycles)
  (tff1arc-init)
  (let ((plv (vector
              '(perceive '((input .1 (arg 0)) (input -.3 (arg 1)) (input .2 (arg 2))))
              '(perceive '((input -.2 (arg 0)) (input .1 (arg 1)) (input .3 (arg 2))))
              '(perceive '((input -.5 (arg 0)) (input .6 (arg 1)) (input -.3 (arg 2))))
              '(perceive '((input -.7 (arg 0)) (input -.5 (arg 1)) (input -.8 (arg 2))))
              '(perceive '((input .8 (arg 0)) (input .6 (arg 1)) (input .2 (arg 2))))
              '(perceive '((input -.9 (arg 0)) (input .9 (arg 1)) (input -.9 (arg 2))))
              '(perceive '((input .9 (arg 0)) (input -.9 (arg 1)) (input .9 (arg 2))))
              ))
        (elv (vector
              '(evidence '((output*goal 0.65742726 (arg 0)) (output*goal 0.91868157 (arg 1))))
              '(evidence '((output*goal 0.6506142 (arg 0)) (output*goal 0.90189445 (arg 1))))
              '(evidence '((output*goal 0.63657547 (arg 0)) (output*goal 0.86628587 (arg 1))))
              '(evidence '((output*goal 0.6150871 (arg 0)) (output*goal 0.8301885 (arg 1))))
              '(evidence '((output*goal 0.6573182 (arg 0)) (output*goal 0.9086708 (arg 1))))
              '(evidence '((output*goal 0.5958128 (arg 0)) (output*goal 0.75859404 (arg 1))))
              '(evidence '((output*goal 0.6925252 (arg 0)) (output*goal 0.9579128 (arg 1))))
              ))
        )
    (setq trace-decisions nil)
    (setq trace-performance nil)
    (dotimes (i (1- cycles))
      (dotimes (i (length plv))
        (setq perceive-list (list (aref plv i)))
        (eval (aref elv i)) ; Set output goal
        (d 1)
        )
      )
    (setq trace-gdl t)
    (dotimes (i (length plv))
      (setq perceive-list (list (aref plv i))) ; Initiate input
      (eval (aref elv i)) ; Set output goal
      (d 1)
      (format t "~&~%~%------------------")
      (format t "~&Input:")
      (pwmb 'input t)
      (format t "~&~%Desired output:")
      (pwmb 'output*goal t)
      (format t "~&~%Computed output:")
      (pwmb 'output t)
      (format t "~&~%Output error derivative:")
      (pwmb 'output*difference-d t)
      (format t "~&------------------")
      )
    )
  t)

; Replication of feedforward network from http://www.doc.ic.ac.uk/~sgc/teaching/pre2012/v231/lecture13.html
; Full arc with learning, but test with tanh rather than sigmoid
(defun tff1h-init ()
  (init)
  (setq compute-progress t)
  (learn '(:gd))
  (setq learning-rate .05)
;  (setq trace-messages '(20))
;  (setq trace-gdl t)
;  (setq trace-messages t)
;  (setq trace-transform t)

  (new-type 'n2 :numeric t :discrete t :min 0 :max 2)
  (new-type 'n3 :numeric t :discrete t :min 0 :max 3)

  (predicate 'input :world 'open :arguments '((arg n3 [])) :no-normalize t :perception t)
  (predicate 'hidden :world 'open :arguments '((arg n2 [])) :no-normalize t)
  (predicate 'output :world 'open :arguments '((arg n2 [])) :no-normalize t
             :goal t)

  ; Predicates for abductive reasoning
;  (predicate 'input*abduct :arguments '((arg n3 [])) :no-normalize t)
  (predicate 'hidden*abduct :arguments '((arg n2 [])) :no-normalize t)

  (conditional 'c-layer1
               :conditions '((input (arg (i))))
               :actions '((hidden t (arg (h))))
               :vector t
               :function-variable-names '(i h)
               :function `((,(random-weight) 0 0) (,(random-weight) 0 1)
                           (,(random-weight) 1 0) (,(random-weight) 1 1)
                           (,(random-weight) 2 0) (,(random-weight) 2 1))
               )

  (conditional 'c-layer2
               :conditions '((hidden (arg (h))))
               :actions '((output t (arg (o))))
               :vector t
               :function-variable-names '(h o)
               :function `((,(random-weight) 0 0) (,(random-weight) 0 1)
                           (,(random-weight) 1 0) (,(random-weight) 1 1))
               )

  (conditional 'c-layer2-i
               :conditions '((output*difference-d (arg (o)))
                             (output t (arg (o))))
               :actions '((hidden*abduct (arg (h))))
               :vector t
               :forward-conditional 'c-layer2
               :exclude-forward-backward t
               )

  (conditional 'c-layer1-i
               :conditions '((hidden*abduct (arg (h)))
                             (hidden t (arg (h))))
;               :actions '((input*abduct (arg (i))))
               :vector t
               :forward-conditional 'c-layer1
               :exclude-forward-backward t
               )

  t)

; Train tff1rc over multiple examples
(defun train-tff1h (cycles)
  (tff1h-init)
  (let ((plv (vector
              '(perceive '((input .1 (arg 0)) (input -.3 (arg 1)) (input .2 (arg 2))))
              '(perceive '((input -.2 (arg 0)) (input .1 (arg 1)) (input .3 (arg 2))))
              '(perceive '((input -.5 (arg 0)) (input .6 (arg 1)) (input -.3 (arg 2))))
              '(perceive '((input -.7 (arg 0)) (input -.5 (arg 1)) (input -.8 (arg 2))))
              '(perceive '((input .8 (arg 0)) (input .6 (arg 1)) (input .2 (arg 2))))
              '(perceive '((input -.9 (arg 0)) (input .9 (arg 1)) (input -.9 (arg 2))))
              '(perceive '((input .9 (arg 0)) (input -.9 (arg 1)) (input .9 (arg 2))))
              ))
        (elv (vector
              '(evidence '((output*goal 0.65742726 (arg 0)) (output*goal 0.91868157 (arg 1))))
              '(evidence '((output*goal 0.6506142 (arg 0)) (output*goal 0.90189445 (arg 1))))
              '(evidence '((output*goal 0.63657547 (arg 0)) (output*goal 0.86628587 (arg 1))))
              '(evidence '((output*goal 0.6150871 (arg 0)) (output*goal 0.8301885 (arg 1))))
              '(evidence '((output*goal 0.6573182 (arg 0)) (output*goal 0.9086708 (arg 1))))
              '(evidence '((output*goal 0.5958128 (arg 0)) (output*goal 0.75859404 (arg 1))))
              '(evidence '((output*goal 0.6925252 (arg 0)) (output*goal 0.9579128 (arg 1))))
              ))
        )
    (setq trace-decisions nil)
    (setq trace-performance nil)
    (dotimes (i (1- cycles))
      (dotimes (i (length plv))
        (setq perceive-list (list (aref plv i)))
        (eval (aref elv i)) ; Set output goal
        (d 1)
        )
      )
    (setq trace-gdl t)
    (dotimes (i (length plv))
      (setq perceive-list (list (aref plv i))) ; Initiate input
      (eval (aref elv i)) ; Set output goal
      (d 1)
      (format t "~&~%~%------------------")
      (format t "~&~Input:")
      (pwmb 'input t)
      (format t "~&~%Desired output:")
      (pwmb 'output*goal t)
      (format t "~&~%Computed output:")
      (pwmb 'output t)
      (format t "~&~%Output error derivative:")
      (pwmb 'output*difference-d t)
      (format t "~&------------------")
      )
    )
  t)

; Replication of feedforward network from http://www.doc.ic.ac.uk/~sgc/teaching/pre2012/v231/lecture13.html
; Full arc with learning, but test with RELU rather than sigmoid
(defun tff1r-init ()
  (init)
  (setq compute-progress t)
  (learn '(:gd))
  (setq learning-rate .05)
;  (setq trace-messages '(20))
;  (setq trace-gdl t)
;  (setq trace-messages t)
;  (setq trace-transform t)

  (new-type 'n2 :numeric t :discrete t :min 0 :max 2)
  (new-type 'n3 :numeric t :discrete t :min 0 :max 3)

  (predicate 'input :world 'open :arguments '((arg n3 [])) :no-normalize t :perception t)
  (predicate 'hidden :world 'open :arguments '((arg n2 [])) :no-normalize t)
  (predicate 'output :world 'open :arguments '((arg n2 [])) :no-normalize t
             :goal t)

  ; Predicates for abductive reasoning
;  (predicate 'input*abduct :arguments '((arg n3 [])) :no-normalize t)
  (predicate 'hidden*abduct :arguments '((arg n2 [])) :no-normalize t)

  (conditional 'c-layer1
               :conditions '((input (arg (i))))
               :actions '((hidden r (arg (h))))
               :vector t
               :function-variable-names '(i h)
               :function `((,(random-weight) 0 0) (,(random-weight) 0 1)
                           (,(random-weight) 1 0) (,(random-weight) 1 1)
                           (,(random-weight) 2 0) (,(random-weight) 2 1))
               )

  (conditional 'c-layer2
               :conditions '((hidden (arg (h))))
               :actions '((output r (arg (o))))
               :vector t
               :function-variable-names '(h o)
               :function `((,(random-weight) 0 0) (,(random-weight) 0 1)
                           (,(random-weight) 1 0) (,(random-weight) 1 1))
               )

  (conditional 'c-layer2-i
               :conditions '((output*difference-d (arg (o)))
                             (output r (arg (o))))
               :actions '((hidden*abduct (arg (h))))
               :vector t
               :forward-conditional 'c-layer2
               :exclude-forward-backward t
               )

  (conditional 'c-layer1-i
               :conditions '((hidden*abduct (arg (h)))
                             (hidden r (arg (h))))
;               :actions '((input*abduct (arg (i))))
               :vector t
               :forward-conditional 'c-layer1
               :exclude-forward-backward t
               )

  t)

; Train tff1rc over multiple examples
(defun train-tff1r (cycles)
  (tff1r-init)
  (let ((plv (vector
              '(perceive '((input .1 (arg 0)) (input -.3 (arg 1)) (input .2 (arg 2))))
              '(perceive '((input -.2 (arg 0)) (input .1 (arg 1)) (input .3 (arg 2))))
              '(perceive '((input -.5 (arg 0)) (input .6 (arg 1)) (input -.3 (arg 2))))
              '(perceive '((input -.7 (arg 0)) (input -.5 (arg 1)) (input -.8 (arg 2))))
              '(perceive '((input .8 (arg 0)) (input .6 (arg 1)) (input .2 (arg 2))))
              '(perceive '((input -.9 (arg 0)) (input .9 (arg 1)) (input -.9 (arg 2))))
              '(perceive '((input .9 (arg 0)) (input -.9 (arg 1)) (input .9 (arg 2))))
              ))
        (elv (vector
              '(evidence '((output*goal 0.65742726 (arg 0)) (output*goal 0.91868157 (arg 1))))
              '(evidence '((output*goal 0.6506142 (arg 0)) (output*goal 0.90189445 (arg 1))))
              '(evidence '((output*goal 0.63657547 (arg 0)) (output*goal 0.86628587 (arg 1))))
              '(evidence '((output*goal 0.6150871 (arg 0)) (output*goal 0.8301885 (arg 1))))
              '(evidence '((output*goal 0.6573182 (arg 0)) (output*goal 0.9086708 (arg 1))))
              '(evidence '((output*goal 0.5958128 (arg 0)) (output*goal 0.75859404 (arg 1))))
              '(evidence '((output*goal 0.6925252 (arg 0)) (output*goal 0.9579128 (arg 1))))
              ))
        )
    (setq trace-decisions nil)
    (setq trace-performance nil)
    (dotimes (i (1- cycles))
      (dotimes (i (length plv))
        (setq perceive-list (list (aref plv i)))
        (eval (aref elv i)) ; Set output goal
        (d 1)
        )
      )
    (setq trace-gdl t)
    (dotimes (i (length plv))
      (setq perceive-list (list (aref plv i))) ; Initiate input
      (eval (aref elv i)) ; Set output goal
      (d 1)
      (format t "~&~%~%------------------")
      (format t "~&~Input:")
      (pwmb 'input t)
      (format t "~&~%Desired output:")
      (pwmb 'output*goal t)
      (format t "~&~%Computed output:")
      (pwmb 'output t)
      (format t "~&~%Output error derivative:")
      (pwmb 'output*difference-d t)
      (format t "~&------------------")
      )
    )
  t)

; Version of tff1 with one conditional per link and a full arc learning
(defun tff1x-init ()
  (init)
  (setq compute-progress t)
  (learn '(:gd))
  (setq learning-rate .05)
;  (setq trace-messages '(20))
;  (setq trace-gdl t)
;  (setq trace-messages t)
;  (setq trace-transform t)
  (new-type 'n1 :numeric t :discrete t :min 0 :max 1)

  (predicate 'i0 :world 'open :arguments '((arg n1 [])) :no-normalize t :perception t)
  (predicate 'i1 :world 'open :arguments '((arg n1 [])) :no-normalize t :perception t)
  (predicate 'i2 :world 'open :arguments '((arg n1 [])) :no-normalize t :perception t)

  (predicate 'h0 :world 'open :arguments '((arg n1 [])) :no-normalize t)
  (predicate 'h1 :world 'open :arguments '((arg n1 [])) :no-normalize t)

  (predicate 'h0-s :world 'open :arguments '((arg n1 [])) :no-normalize t)
  (predicate 'h1-s :world 'open :arguments '((arg n1 [])) :no-normalize t)

  (predicate 'o0 :world 'open :arguments '((arg n1 [])) :no-normalize t)
  (predicate 'o1 :world 'open :arguments '((arg n1 [])) :no-normalize t)

  (predicate 'o0-s :world 'open :arguments '((arg n1 [])) :no-normalize t :goal t)
  (predicate 'o1-s :world 'open :arguments '((arg n1 [])) :no-normalize t :goal t)

  ; Predicates for abductive reasoning
  (predicate 'h0-s*abduct :arguments '((arg n1 [])) :no-normalize t)
  (predicate 'h1-s*abduct :arguments '((arg n1 [])) :no-normalize t)


  ; Forward layer 1 links
  (conditional 'c-i0-h0
               :conditions '((i0 (arg (i))))
               :actions '((h0 (arg (h))))
               :function-variable-names '(i h)
               :vector t
               :function (random-weight)
               )

  (conditional 'c-i0-h1
               :conditions '((i0 (arg (i))))
               :actions '((h1 (arg (h))))
               :function-variable-names '(i h)
               :vector t
               :function (random-weight)
               )

  (conditional 'c-i1-h0
               :conditions '((i1 (arg (i))))
               :actions '((h0 (arg (h))))
               :function-variable-names '(i h)
               :vector t
               :function (random-weight)
               )

  (conditional 'c-i1-h1
               :conditions '((i1 (arg (i))))
               :actions '((h1 (arg (h))))
               :function-variable-names '(i h)
               :vector t
               :function (random-weight)
               )

  (conditional 'c-i2-h0
               :conditions '((i2 (arg (i))))
               :actions '((h0 (arg (h))))
               :function-variable-names '(i h)
               :vector t
               :function (random-weight)
               )

  (conditional 'c-i2-h1
               :conditions '((i2 (arg (i))))
               :actions '((h1 (arg (h))))
               :function-variable-names '(i h)
               :vector t
               :function (random-weight)
               )

  ; Forward layer 1 sigmoids
  (conditional 'c-h0-s
               :conditions '((h0 (arg 0)))
               :actions '((h0-s s (arg 0)))
               )

  (conditional 'c-h1-s
               :conditions '((h1 (arg 0)))
               :actions '((h1-s s (arg 0)))
               )

  ; Forward layer 2 links
  (conditional 'c-h0-o0
               :conditions '((h0-s (arg (h))))
               :actions '((o0 (arg (o))))
               :function-variable-names '(h o)
               :vector t
               :function (random-weight)
               )

  (conditional 'c-h0-o1
               :conditions '((h0-s (arg (h))))
               :actions '((o1 (arg (o))))
               :function-variable-names '(h o)
               :vector t
               :function (random-weight)
               )

  (conditional 'c-h1-o0
               :conditions '((h1-s (arg (h))))
               :actions '((o0 (arg (o))))
               :vector t
               :function-variable-names '(h o)
               :function (random-weight)
               )

  (conditional 'c-h1-o1
               :conditions '((h1-s (arg (h))))
               :actions '((o1 (arg (o))))
               :function-variable-names '(h o)
               :vector t
               :function (random-weight)
               )

  ; Forward layer 2 sigmoids
  (conditional 'c-o0-s
               :conditions '((o0 (arg 0)))
               :actions '((o0-s s (arg 0)))
               )

  (conditional 'c-o1-s
               :conditions '((o1 (arg 0)))
               :actions '((o1-s s (arg 0)))
               )

  ; Backward layer 2 links and sigmoids
  (conditional 'c-h0-o0-i
               :conditions '((o0-s*difference-d (arg (o)))
                             (o0-s s (arg (o))))
               :actions '((h0-s*abduct (arg (h))))
               :vector t
               :forward-conditional 'c-h0-o0
               :exclude-forward-backward t               )

  (conditional 'c-h0-o1-i
               :conditions '((o1-s*difference-d (arg (o)))
                             (o1-s s (arg (o))))
               :actions '((h0-s*abduct (arg (h))))
               :vector t
               :forward-conditional 'c-h0-o1
               :exclude-forward-backward t
               )

  (conditional 'c-h1-o0-i
               :conditions '((o0-s*difference-d (arg (o)))
                             (o0-s s (arg (o))))
               :actions '((h1-s*abduct (arg (h))))
               :vector t
               :forward-conditional 'c-h1-o0
               :exclude-forward-backward t
               )

  (conditional 'c-h1-o1-i
               :conditions '((o1-s*difference-d (arg (o)))
                             (o1-s s (arg (o))))
               :actions '((h1-s*abduct (arg (h))))
               :vector t
               :forward-conditional 'c-h1-o1
               :exclude-forward-backward t
               )

  ; Backward layer 1 links and sigmoids
  (conditional 'c-i0-h0-i
               :conditions '((h0-s*abduct (arg (h)))
                             (h0-s s (arg (h))))
               :vector t
               :forward-conditional 'c-i0-h0
               :exclude-forward-backward t
               )

  (conditional 'c-i0-h1-i
               :conditions '((h1-s*abduct (arg (h)))
                             (h1-s s (arg (h))))
               :vector t
               :forward-conditional 'c-i0-h1
               :exclude-forward-backward t
               )

  (conditional 'c-i1-h0-i
               :conditions '((h0-s*abduct (arg (h)))
                             (h0-s s (arg (h))))
               :vector t
               :forward-conditional 'c-i1-h0
               :exclude-forward-backward t
               )

  (conditional 'c-i1-h1-i
               :conditions '((h1-s*abduct (arg (h)))
                             (h1-s s (arg (h))))
               :vector t
               :forward-conditional 'c-i1-h1
               :exclude-forward-backward t
               )

  (conditional 'c-i2-h0-i
               :conditions '((h0-s*abduct (arg (h)))
                             (h0-s s (arg (h))))
               :vector t
               :forward-conditional 'c-i2-h0
               :exclude-forward-backward t
               )

  (conditional 'c-i2-h1-i
               :conditions '((h1-s*abduct (arg (h)))
                             (h1-s s (arg (h))))
               :vector t
               :forward-conditional 'c-i2-h1
               :exclude-forward-backward t
               )

  t)

; Train tff1x over multiple examples
(defun train-tff1x (cycles)
  (tff1x-init)
  (let ((plv (vector
              '(perceive '((i0 .1 (arg 0)) (i1 -.3 (arg 0)) (i2 .2 (arg 0))))
              '(perceive '((i0 -.2 (arg 0)) (i1 .1 (arg 0)) (i2 .3 (arg 0))))
              '(perceive '((i0 -.5 (arg 0)) (i1 .6 (arg 0)) (i2 -.3 (arg 0))))
              '(perceive '((i0 -.7 (arg 0)) (i1 -.5 (arg 0)) (i2 -.8 (arg 0))))
              '(perceive '((i0 .8 (arg 0)) (i1 .6 (arg 0)) (i2 .2 (arg 0))))
              '(perceive '((i0 -.9 (arg 0)) (i1 .9 (arg 0)) (i2 -.9 (arg 0))))
              '(perceive '((i0 .9 (arg 0)) (i1 -.9 (arg 0)) (i2 .9 (arg 0))))
              ))
        (elv (vector
              '(evidence '((o0-s*goal 0.65742726 (arg 0)) (o1-s*goal 0.91868157 (arg 0))))
              '(evidence '((o0-s*goal 0.6506142 (arg 0)) (o1-s*goal 0.90189445 (arg 0))))
              '(evidence '((o0-s*goal 0.63657547 (arg 0)) (o1-s*goal 0.86628587 (arg 0))))
              '(evidence '((o0-s*goal 0.6150871 (arg 0)) (o1-s*goal 0.8301885 (arg 0))))
              '(evidence '((o0-s*goal 0.6573182 (arg 0)) (o1-s*goal 0.9086708 (arg 0))))
              '(evidence '((o0-s*goal 0.5958128 (arg 0)) (o1-s*goal 0.75859404 (arg 0))))
              '(evidence '((o0-s*goal 0.6925252 (arg 0)) (o1-s*goal 0.9579128 (arg 0))))
              ))
        )
    (setq trace-decisions nil)
    (setq trace-performance nil)
    (dotimes (i (1- cycles))
      (dotimes (i (length plv))
        (setq perceive-list (list (aref plv i)))
        (eval (aref elv i)) ; Set output goal
        (d 1)
        )
      )
;    (setq trace-gdl t)
    (dotimes (i (length plv))
      (setq perceive-list (list (aref plv i))) ; Initiate input
      (eval (aref elv i)) ; Set output goal
      (d 1)
      (format t "~&~%~%------------------")
      (format t "~&Input:")
      (pwmb 'i0 t) (pwmb 'i1 t) (pwmb 'i2 t)
      (format t "~&~%Desired output:")
      (pwmb 'o0-s*goal t) (pwmb 'o1-s*goal t)
      (format t "~&~%Computed output:")
      (pwmb 'o0-s t) (pwmb 'o1-s t)
      (format t "~&~%Output error derivative:")
      (pwmb 'o0-s*difference-d t) (pwmb 'o1-s*difference-d t)
      (format t "~&------------------")
      )
    )
  t)

; Replication of feedforward AND network from http://www.dataminingmasters.com/uploads/studentProjects/NeuralNetworks.pdf
; A complete arc for learning
(defun tffand-init ()
  (init)
  (setq compute-progress t)
  (learn '(:gd))
  (setq learning-rate .05)
  (setq trace-empty t)
;  (setq trace-messages '(20))
;  (setq trace-gdl t)
;  (setq trace-messages t)
;  (setq trace-transform t)

  (new-type 'n2 :numeric t :discrete t :min 0 :max 2)
  (new-type 'n1 :numeric t :discrete t :min 0 :max 1)

  (predicate 'input :world 'open :arguments '((arg n2 [])) :no-normalize t :perception t)
  (predicate 'hidden :world 'open :arguments '((arg n2 [])) :no-normalize t)
  (predicate 'output :world 'open :arguments '((arg n1 [])) :no-normalize t
             :goal t)

  ; Predicates for abductive reasoning
  (predicate 'input*abduct :arguments '((arg n2 [])) :no-normalize t)
  (predicate 'hidden*abduct :arguments '((arg n2 [])) :no-normalize t)

  (conditional 'c-layer1
               :conditions `((input (arg (i))))
               :actions `((hidden s (arg (h))))
               :vector t
               :function-variable-names '(i h)
               :function `((,(random-weight 1.0) 0 0) (,(random-weight 1.0) 0 1) (,(random-weight 1.0) 1 0) (,(random-weight 1.0) 1 1))
               )

  (conditional 'c-layer2
               :conditions `((hidden (arg (h))))
               :actions `((output s (arg (o))))
               :vector t
               :function-variable-names '(h o)
               :function `((,(random-weight 1.0) 0 0) (,(random-weight 1.0) 1 0))
               )

  (conditional 'c-layer2-i
               :conditions '((output*difference-d (arg (o)))
                             (output s (arg (o))))
               :actions '((hidden*abduct (arg (h))))
               :vector t
               :forward-conditional 'c-layer2
               :exclude-forward-backward t
               )

  (conditional 'c-layer1-i
               :conditions '((hidden*abduct (arg (h)))
                             (hidden s (arg (h))))
               :actions '((input*abduct (arg (i))))
               :vector t
               :forward-conditional 'c-layer1
               :exclude-forward-backward t
               )
  t)

; Train tffand over multiple examples
(defun train-tffand (cycles)
  (tffand-init)
  (let ((plv (vector
              '(perceive '((input 1 (arg 0)) (input 1 (arg 1))))
              '(perceive '((input 1 (arg 0)) (input 0 (arg 1))))
              '(perceive '((input 0 (arg 0)) (input 1 (arg 1))))
              '(perceive '((input 0 (arg 0)) (input 0 (arg 1))))
              ))
        (elv (vector
              '(evidence '((output*goal 1 (arg 0))))
              '(evidence '((output*goal 0 (arg 0))))
              '(evidence '((output*goal 0 (arg 0))))
              '(evidence '((output*goal 0 (arg 0))))
              )))
    (setq trace-decisions nil)
    (setq trace-performance nil)
    (dotimes (i (1- cycles))
      (dotimes (i (length plv))
        (setq perceive-list (list (aref plv i)))
        (eval (aref elv i)) ; Set output goal
        (d 1)
        )
      )
    (setq trace-gdl t)
    (dotimes (i (length plv))
      (setq perceive-list (list (aref plv i))) ; Initiate input
      (eval (aref elv i)) ; Set output goal
      (d 1)
      (format t "~&~%~%------------------")
      (format t "~&~%Desired output:")
      (pwmb 'output*goal t)
      (format t "~&~%Computed output:")
      (pwmb 'output t)
      (format t "~&~%Output error derivative:")
      (pwmb 'output*difference-d t)
      )
    t)
  )

; Test XOR via NNs with one hidden layer and bias weights
; Arc variant of xor6
(defun xor7-init ()
  (init)
  (setq compute-progress t)
  (learn '(:gd))
;  (setq trace-gdl t)
;  (setq trace-messages t)
;  (setq trace-transform t)
  (setq trace-empty t)

  (new-type 'n4 :numeric t :discrete t :min 0 :max 4)
  (new-type 'n3 :numeric t :discrete t :min 0 :max 3)
  (new-type 'n1 :numeric t :discrete t :min 0 :max 1)

  (predicate 'input :world 'open :arguments '((arg n3 [])) :no-normalize t :perception t)
  (predicate 'hidden :world 'open :arguments '((arg n4 [])) :no-normalize t)
  (predicate 'output :world 'open :arguments '((arg n1 [])) :no-normalize t
              :goal t)

  ; Predicates for abductive reasoning
  (predicate 'input*abduct :arguments '((arg n3 [])) :no-normalize t)
  (predicate 'hidden*abduct :arguments '((arg n4 [])) :no-normalize t)

  (conditional 'c-layer1
               :conditions '((input (arg (i))))
               :actions '((hidden s (arg (h))))
               :function-variable-names '(i h)
               :vector t
               :function `(
                           (,(random-weight 1.0) 0 0) (,(random-weight 1.0) 0 1) (,(random-weight 1.0) 0 2) (0 0 3)
                           (,(random-weight 1.0) 1 0) (,(random-weight 1.0) 1 1) (,(random-weight 1.0) 1 2) (0 1 3)
                           (,(random-weight 1.0) 2 0) (,(random-weight 1.0) 2 1) (,(random-weight 1.0) 2 2) (0 2 3)
                           )
               )

  (conditional 'c-layer2
               :conditions '((hidden (arg (h))))
               :actions '((output s (arg (o))))
               :function-variable-names '(h o)
               :vector t
               :function `(
                           (,(random-weight 1.0) 0 0) (,(random-weight 1.0) 1 0) (,(random-weight 1.0) 2 0) (,(random-weight 1.0) 3 0)
                           )
               )

  (conditional 'c-layer2-i
               :conditions '((output*difference-d (arg (o)))
                             (output s (arg (o))))
               :actions '((hidden*abduct (arg (h))))
               :vector t
               :forward-conditional 'c-layer2
               :exclude-forward-backward t
               )

  (conditional 'c-layer1-i
               :conditions '((hidden*abduct (arg (h)))
                             (hidden s (arg (h))))
               :actions '((input*abduct (arg (i))))
               :vector t
               :forward-conditional 'c-layer1
               :exclude-forward-backward t
               )

  t)

; Train xor7 over multiple examples
(defun train-xor7 (cycles)
  (xor7-init)
  (let ((plv (vector
              '(perceive '((input 0 (arg 0)) (input 0 (arg 1)) (input 1 (arg 2))))
              '(perceive '((input 0 (arg 0)) (input 1 (arg 1)) (input 1 (arg 2))))
              '(perceive '((input 1 (arg 0)) (input 0 (arg 1)) (input 1 (arg 2))))
              '(perceive '((input 1 (arg 0)) (input 1 (arg 1)) (input 1 (arg 2))))
              ))
        (elv (vector
              '(evidence '((output*goal 0 (arg 0))))
              '(evidence '((output*goal 1 (arg 0))))
              '(evidence '((output*goal 1 (arg 0))))
              '(evidence '((output*goal 0 (arg 0))))
              ))
        )
    (setq trace-decisions nil)
    (setq trace-performance nil)
    (dotimes (i (1- cycles))
      (dotimes (i (length plv))
        (setq perceive-list (list (aref plv i)))
        (eval (aref elv i)) ; Set output goal
        (d 1)
        )
      )
    (setq trace-gdl t)
    (dotimes (i (length plv))
      (setq perceive-list (list (aref plv i))) ; Initiate input
      (eval (aref elv i)) ; Set output goal
      (d 1)
      (format t "~&~%~%------------------")
      (format t "~&Input:")
      (pwmb 'input t)
      (format t "~&~%Desired output:")
      (pwmb 'output*goal t)
      (format t "~&~%Computed output:")
      (pwmb 'output t)
      (format t "~&~%Output error derivative:")
      (pwmb 'output*difference-d t)
      (format t "~&------------------")
      )
    )
  t)

; Test XOR via NNs with a smaller hidden layer and no bias weights
; Arc variant of xor4
(defun xor8-init ()
  (init)
  (setq compute-progress t)
  (learn '(:gd))
;  (setq trace-gdl t)
;  (setq trace-messages t)
;  (setq trace-transform t)
  (setq trace-empty t)

  (new-type 'n2 :numeric t :discrete t :min 0 :max 2)
  (new-type 'n1 :numeric t :discrete t :min 0 :max 1)

  (predicate 'input :world 'open :arguments '((arg n2 [])) :no-normalize t :perception t)
  (predicate 'hidden :world 'open :arguments '((arg n2 [])) :no-normalize t)
  (predicate 'output :world 'open :arguments '((arg n1 [])) :no-normalize t
              :goal t)

  ; Predicates for abductive reasoning
  (predicate 'input*abduct :arguments '((arg n2 [])) :no-normalize t)
  (predicate 'hidden*abduct :arguments '((arg n2 [])) :no-normalize t)

  (conditional 'c-layer1
               :conditions '((input (arg (i))))
               :actions '((hidden s (arg (h))))
               :function-variable-names '(i h)
               :vector t
               :function `(
                           (,(random-weight 1.0) 0 0) (,(random-weight 1.0) 0 1)
                           (,(random-weight 1.0) 1 0) (,(random-weight 1.0) 1 1)
                           )
               )

  (conditional 'c-layer2
               :conditions '((hidden (arg (h))))
               :actions '((output s (arg (o))))
               :function-variable-names '(h o)
               :vector t
               :function `(
                           (,(random-weight 1.0) 0 0) (,(random-weight 1.0) 1 0)
                           )
               )

  (conditional 'c-layer2-i
               :conditions '((output*difference-d (arg (o)))
                             (output s (arg (o))))
               :actions '((hidden*abduct (arg (h))))
               :vector t
               :forward-conditional 'c-layer2
               :exclude-forward-backward t
               )

  (conditional 'c-layer1-i
               :conditions '((hidden*abduct (arg (h)))
                             (hidden s (arg (h))))
               :actions '((input*abduct (arg (i))))
               :vector t
               :forward-conditional 'c-layer1
               :exclude-forward-backward t
               )

  t)

; Train xor7 over multiple examples
(defun train-xor8 (cycles)
  (xor8-init)
  (let ((plv (vector
              '(perceive '((input 0 (arg 0)) (input 0 (arg 1))))
              '(perceive '((input 0 (arg 0)) (input 1 (arg 1))))
              '(perceive '((input 1 (arg 0)) (input 0 (arg 1))))
              '(perceive '((input 1 (arg 0)) (input 1 (arg 1))))
              ))
        (elv (vector
              '(evidence '((output*goal 0 (arg 0))))
              '(evidence '((output*goal 1 (arg 0))))
              '(evidence '((output*goal 1 (arg 0))))
              '(evidence '((output*goal 0 (arg 0))))
              ))
        )
    (setq trace-decisions nil)
    (setq trace-performance nil)
    (dotimes (i (1- cycles))
      (dotimes (i (length plv))
        (setq perceive-list (list (aref plv i)))
        (eval (aref elv i)) ; Set output goal
        (d 1)
        )
      )
    (setq trace-gdl t)
    (dotimes (i (length plv))
      (setq perceive-list (list (aref plv i))) ; Initiate input
      (eval (aref elv i)) ; Set output goal
      (d 1)
      (format t "~&~%~%------------------")
      (format t "~&Input:")
      (pwmb 'input t)
      (format t "~&~%Desired output:")
      (pwmb 'output*goal t)
      (format t "~&~%Computed output:")
      (pwmb 'output t)
      (format t "~&~%Output error derivative:")
      (pwmb 'output*difference-d t)
      (format t "~&------------------")
      )
    )
  t)

; A simple one layer network that is supposed to learn weights of .25 and -.75
(defun tffs-init ()
  (init)
  (setq compute-progress t)
  (learn '(:gd))
  (setq learning-rate .05)
  (setq trace-empty t)
;  (setq trace-messages '(20))
;  (setq trace-gdl t)
;  (setq trace-messages t)
;  (setq trace-transform t)

  (new-type 'n2 :numeric t :discrete t :min 0 :max 2)
  (new-type 'n1 :numeric t :discrete t :min 0 :max 1)

  (predicate 'input :world 'open :arguments '((arg n2 [])) :no-normalize t :perception t)
  (predicate 'output :world 'open :arguments '((arg n1 [])) :no-normalize t :perception t :goal t)

  ; Predicates for abductive reasoning
  (predicate 'input*abduct :arguments '((arg n2 [])) :no-normalize t)

  (conditional 'c-layer
               :conditions `((input (arg (i))))
               :actions `((output s (arg (o))))
               :vector t
               :function-variable-names '(i o)
               :function `((,(random-weight 1.0) 0 0) (,(random-weight 1.0) 1 0))
               )

  (conditional 'c-layer-i
               :conditions '((output*difference-d (arg (o)))
                             (output s (arg (o))))
               :actions '((input*abduct (arg (i))))
               :vector t
               :forward-conditional 'c-layer
               :exclude-forward-backward t
               )
  t)

; Train tffs over multiple examples
(defun train-tffs (cycles)
  (tffs-init)
  (let ((plv (vector
              '(perceive '((input 1 (arg 0)) (input 1 (arg 1))))
              '(perceive '((input 1 (arg 0)) (input 0 (arg 1))))
              '(perceive '((input 0 (arg 0)) (input 1 (arg 1))))
              '(perceive '((input 0 (arg 0)) (input 0 (arg 1))))
              ))
        (elv (vector ; These are the logistics of -.5, -.25, -.75, 0
              '(perceive '((output*goal 0.37754068 (arg 0))))
              '(perceive '((output*goal 0.4378235 (arg 0))))
              '(perceive '((output*goal 0.3208213 (arg 0))))
              '(perceive '((output*goal 0.5 (arg 0))))
              )))
    (setq trace-decisions nil)
    (setq trace-performance nil)
    (dotimes (i (1- cycles))
      (dotimes (i (length plv))
        (setq perceive-list (list (aref plv i)))
        (eval (aref elv i)) ; Set output goal
        (d 1)
        )
      )
    (setq trace-gdl t)
    (dotimes (i (length plv))
      (setq perceive-list (list (aref plv i))) ; Initiate input
      (eval (aref elv i)) ; Set output goal
      (d 1)
      (format t "~&~%~%------------------")
      (format t "~&~%Input:")
      (pwmb 'input t)
      (format t "~&~%Desired output:")
      (pwmb 'output*goal t)
      (format t "~&~%Computed output:")
      (pwmb 'output t)
      (format t "~&~%Output error derivative:")
      (pwmb 'output*difference-d t)
      )
    t)
  )

; A simple one layer network that is supposed to learn weights of .25 and -.75
(defun tsm-init ()
  (init)
  (setq compute-progress t)
  (learn '(:gd))
  (setq learning-rate .05)
  (setq trace-empty t)
;  (setq trace-messages '(20))
;  (setq trace-gdl t)
;  (setq trace-messages t)
;  (setq trace-transform t)

  (new-type 'n2 :numeric t :discrete t :min 0 :max 2)

  (predicate 'input :world 'open :arguments '((arg n2 [])) :no-normalize t :perception t)
  (predicate 'output :world 'open :arguments '((arg n2 [])) :perception t :goal t)


  (conditional 'c-layer
               :conditions `((input (arg (i))))
               :actions `((output s (arg (o))))
               :vector t
               :function-variable-names '(i o)
               :function `((,(random-weight 1.0) 0 0) (,(random-weight 1.0) 0 1)
                           (,(random-weight 1.0) 1 0) (,(random-weight 1.0) 1 1))
               )

  (conditional 'c-layer-i
               :conditions '((output*difference-d (arg (o)))
                             (output e (arg (o))))
               :vector t
               :forward-conditional 'c-layer
               :exclude-forward-backward t
               )
  t)

; Train tsm over multiple examples
(defun train-tsm (cycles)
  (tsm-init)
  (let ((plv (vector
              '(perceive '((input 0 (arg 0)) (input 0 (arg 1))))
              '(perceive '((input 0 (arg 0)) (input 1 (arg 1))))
              '(perceive '((input 1 (arg 0)) (input 0 (arg 1))))
              '(perceive '((input 1 (arg 0)) (input 1 (arg 1))))
              ))
        (elv (vector
              '(perceive '((output*goal .5 (arg 0)) (output*goal .5 (arg 1))))
              '(perceive '((output*goal 2.459603 (arg 0)) (output*goal 1.105171 (arg 1))))
              '(perceive '((output*goal 2.2255409 (arg 0)) (output*goal 1.2214028 (arg 1))))
              '(perceive '((output*goal 5.4739475 (arg 0)) (output*goal 1.3498589 (arg 1))))
              )))
    (setq trace-decisions nil)
    (setq trace-performance nil)
    (dotimes (i (1- cycles))
      (dotimes (i (length plv))
        (setq perceive-list (list (aref plv i)))
        (eval (aref elv i)) ; Set output goal
        (d 1)
        )
      )
    (setq trace-gdl t)
    (dotimes (i (length plv))
      (setq perceive-list (list (aref plv i) (aref elv i))) ; Initiate input and goal
      (d 1)
      (format t "~&~%~%------------------")
      (format t "~&~%Input:")
      (pwmb 'input t)
      (format t "~&~%Desired output:")
      (pwmb 'output*goal t)
      (format t "~&~%Computed output:")
      (pwmb 'output t)
      (format t "~&~%Output error derivative:")
      (pwmb 'output*difference-d t)
      )
    t)
  )

; A simple size 2, step 1, convolutional layer followed by a fully connected layer over a 1D three-element input
(defun conv-init nil
  (init)
  (setq compute-progress t)
  (learn '(:gd))
  (setq learning-rate .05)
  (setq trace-empty t)
;  (setq trace-messages '(20))
;  (setq trace-gdl t)
;  (setq trace-messages t)
;  (setq trace-transform t)

  (new-type 'n1 :numeric t :discrete t :min 0 :max 1)

  (predicate 'i0 :world 'open :arguments '((arg n1 [])) :no-normalize t :perception t)
  (predicate 'i1 :world 'open :arguments '((arg n1 [])) :no-normalize t :perception t)
  (predicate 'i2 :world 'open :arguments '((arg n1 [])) :no-normalize t :perception t)

  (predicate 'f0 :world 'open :arguments '((arg n1 [])) :no-normalize t)
  (predicate 'f1 :world 'open :arguments '((arg n1 [])) :no-normalize t)

  (predicate 'f0-s :world 'open :arguments '((arg n1 [])) :no-normalize t)
  (predicate 'f1-s :world 'open :arguments '((arg n1 [])) :no-normalize t)

  (predicate 'o :world 'open :arguments '((arg n1 [])) :no-normalize t)

  (predicate 'o-s :world 'open :arguments '((arg n1 [])) :no-normalize t :goal t)

  ; Predicates for abductive reasoning
  (predicate 'f0-s*abduct :arguments '((arg n1 [])) :no-normalize t)
  (predicate 'f1-s*abduct :arguments '((arg n1 [])) :no-normalize t)


  ; Forward layer 1 links
  (conditional 'i0-f0
               :conditions '((i0 (arg (i))))
               :actions '((f0 (arg (f))))
               :function-variable-names '(i f)
               :vector t
               :function (random-weight)
               )

  (conditional 'i1-f0
               :conditions '((i1 (arg (i))))
               :actions '((f0 (arg (f))))
               :function-variable-names '(i f)
               :vector t
               :function (random-weight)
               )

  (conditional 'i1-f1
               :conditions '((i1 (arg (i))))
               :actions '((f1 (arg (f))))
               :function-variable-names '(i f)
               :vector t
               :function 'i0-f0
               )

  (conditional 'i2-f1
               :conditions '((i2 (arg (i))))
               :actions '((f1 (arg (f))))
               :function-variable-names '(i f)
               :vector t
               :function 'i1-f0
               )

  ; Forward layer 1 sigmoids
  (conditional 'f0-s
               :conditions '((f0 (arg 0)))
               :actions '((f0-s s (arg 0)))
               )

  (conditional 'f1-s
               :conditions '((f1 (arg 0)))
               :actions '((f1-s s (arg 0)))
               )

  ; Forward layer 2 links
  (conditional 'f0-o
               :conditions '((f0-s (arg (f))))
               :actions '((o (arg (o))))
               :function-variable-names '(f o)
               :vector t
               :function (random-weight)
               )

  (conditional 'f1-o
               :conditions '((f1-s (arg (f))))
               :actions '((o (arg (o))))
               :function-variable-names '(f o)
               :vector t
               :function (random-weight)
               )

  ; Forward layer 2 sigmoids
  (conditional 'c-o-s
               :conditions '((o (arg 0)))
               :actions '((o-s s (arg 0)))
               )

  ; Backward layer 2 links and sigmoids
  (conditional 'o-f0
               :conditions '((o-s*difference-d (arg (o)))
                             (o-s s (arg (o))))
               :actions '((f0-s*abduct (arg (f))))
               :vector t
               :forward-conditional 'f0-o
               :exclude-forward-backward t
               )

  (conditional 'o-f1
               :conditions '((o-s*difference-d (arg (o)))
                             (o-s s (arg (o))))
               :actions '((f1-s*abduct (arg (f))))
               :vector t
               :forward-conditional 'o-f0
               :exclude-forward-backward t
               )

  ; Backward layer 1 links and sigmoids
  (conditional 'f0-i0
               :conditions '((f0-s*abduct (arg (f)))
                             (f0-s s (arg (f))))
               :vector t
               :forward-conditional 'i0-f0
               :exclude-forward-backward t
               )

  (conditional 'f0-i1
               :conditions '((f0-s*abduct (arg (f)))
                             (f0-s s (arg (f))))
               :vector t
               :forward-conditional 'i1-f0
               :exclude-forward-backward t
               )

  (conditional 'f1-i1
               :conditions '((f1-s*abduct (arg (f)))
                             (f1-s s (arg (f))))
               :vector t
               :forward-conditional 'i1-f1
               :exclude-forward-backward t
               )

  (conditional 'f1-i2
               :conditions '((f1-s*abduct (arg (f)))
                             (f1-s s (arg (f))))
               :vector t
               :forward-conditional 'i2-f1
               :exclude-forward-backward t
               )
  t)

; Given inputs, compute outputs for conv assuming want to learn the weights .3, -.2 and .6 -.3 at the two layers
(defun conv-o (i0 i1 i2)
  (let* ((f0 (logistic (+ (* .3 i0) (* -.2 i1))))
         (f1 (logistic (+ (* .3 i1) (* -.2 i2))))
         )
    (logistic (+ (* .6 f0) (* -.3 f1)))
    )
  )

; Train conv over multiple examples
(defun train-conv (cycles)
  (conv-init)
  (let ((plv (vector
              '(perceive '((i0 .1 (arg 0)) (i1 -.3 (arg 0)) (i2 .2 (arg 0))))
              '(perceive '((i0 -.2 (arg 0)) (i1 .1 (arg 0)) (i2 .3 (arg 0))))
              '(perceive '((i0 -.5 (arg 0)) (i1 .6 (arg 0)) (i2 -.3 (arg 0))))
              '(perceive '((i0 -.7 (arg 0)) (i1 -.5 (arg 0)) (i2 -.8 (arg 0))))
              '(perceive '((i0 .8 (arg 0)) (i1 .6 (arg 0)) (i2 .2 (arg 0))))
              '(perceive '((i0 -.9 (arg 0)) (i1 .9 (arg 0)) (i2 -.9 (arg 0))))
              '(perceive '((i0 .9 (arg 0)) (i1 -.9 (arg 0)) (i2 .9 (arg 0))))
              ))
        (elv (vector
              '(evidence '((o-s*goal 0.5431989 (arg 0))))
              '(evidence '((o-s*goal 0.5350067 (arg 0))))
              '(evidence '((o-s*goal 0.48280898 (arg 0))))
              '(evidence '((o-s*goal 0.533143 (arg 0))))
              '(evidence '((o-s*goal 0.5392927 (arg 0))))
              '(evidence '((o-s*goal 0.5126035 (arg 0))))
              '(evidence '((o-s*goal 0.5623176 (arg 0))))
              ))
        )
    (setq trace-decisions nil)
    (setq trace-performance nil)
    (dotimes (i (1- cycles))
      (dotimes (i (length plv))
        (setq perceive-list (list (aref plv i)))
        (eval (aref elv i)) ; Set output goal
        (d 1)
        )
      )
;    (setq trace-gdl t)
    (dotimes (i (length plv))
      (setq perceive-list (list (aref plv i))) ; Initiate input
      (eval (aref elv i)) ; Set output goal
      (d 1)
      (format t "~&~%~%------------------")
      (format t "~&Input:")
      (pwmb 'i0 t) (pwmb 'i1 t) (pwmb 'i2 t)
      (format t "~&~%Desired output:")
      (pwmb 'o-s*goal t)
      (format t "~&~%Computed output:")
      (pwmb 'o-s t)
      (format t "~&~%Output error derivative:")
      (pwmb 'o-s*difference-d t)
      (format t "~&------------------")
      )
    )
  t)

; A simple size 2, step 1, convolutional layer over a 1D three-element input followed by a max pooling layer
(defun cm-init nil
  (init)
  (setq compute-progress t)
  (learn '(:gd))
  (setq learning-rate .05)
  (setq trace-empty t)
;  (setq trace-messages '(20))
;  (setq trace-gdl t)
;  (setq trace-messages t)
;  (setq trace-transform t)

  (new-type 'n1 :numeric t :discrete t :min 0 :max 1)

  (predicate 'i0 :world 'open :arguments '((arg n1 [])) :no-normalize t :perception t)
  (predicate 'i1 :world 'open :arguments '((arg n1 [])) :no-normalize t :perception t)
  (predicate 'i2 :world 'open :arguments '((arg n1 [])) :no-normalize t :perception t)

  (predicate 'f0 :world 'open :arguments '((arg n1 [])) :no-normalize t)
  (predicate 'f1 :world 'open :arguments '((arg n1 [])) :no-normalize t)

  (predicate 'f0-s :world 'open :arguments '((arg n1 [])) :no-normalize t)
  (predicate 'f1-s :world 'open :arguments '((arg n1 [])) :no-normalize t)

  (predicate 'o :world 'open :arguments '((arg n1)) :no-normalize t :goal t) ; Max pooling

  ; Predicates for abductive reasoning
  (predicate 'f0-s*abduct :arguments '((arg n1 [])) :no-normalize t)
  (predicate 'f1-s*abduct :arguments '((arg n1 [])) :no-normalize t)


  ; Forward layer 1 links
  (conditional 'i0-f0
               :conditions '((i0 (arg (i))))
               :actions '((f0 (arg (f))))
               :function-variable-names '(i f)
               :vector t
               :function (random-weight)
               )

  (conditional 'i1-f0
               :conditions '((i1 (arg (i))))
               :actions '((f0 (arg (f))))
               :function-variable-names '(i f)
               :vector t
               :function (random-weight)
               )

  (conditional 'i1-f1
               :conditions '((i1 (arg (i))))
               :actions '((f1 (arg (f))))
               :function-variable-names '(i f)
               :vector t
               :function 'i0-f0
               )

  (conditional 'i2-f1
               :conditions '((i2 (arg (i))))
               :actions '((f1 (arg (f))))
               :function-variable-names '(i f)
               :vector t
               :function 'i1-f0
               )

  ; Forward layer 1 sigmoids
  (conditional 'f0-s
               :conditions '((f0 (arg 0)))
               :actions '((f0-s s (arg 0)))
               )

  (conditional 'f1-s
               :conditions '((f1 (arg 0)))
               :actions '((f1-s s (arg 0)))
               )

  ; Forward layer 2 links
  (conditional 'f0-o
               :conditions '((f0-s (arg (f))))
               :actions '((o (arg (o))))
               )

  (conditional 'f1-o
               :conditions '((f1-s (arg (f))))
               :actions '((o (arg (o))))
               )

  ; Backward layer 2 links and sigmoids
  (conditional 'o-f0
               :conditions '((o*difference-d (arg (o)))
                             (o s (arg (o))))
               :actions '((f0-s*abduct (arg 0))
                          (f1-s*abduct (arg 0)))
               )

  ; Backward layer 1 links and sigmoids
  (conditional 'f0-i0
               :conditions '((f0-s*abduct (arg (f)))
                             (f0-s s (arg (f))))
               :vector t
               :forward-conditional 'i0-f0
               :exclude-forward-backward t
               )

  (conditional 'f0-i1
               :conditions '((f0-s*abduct (arg (f)))
                             (f0-s s (arg (f))))
               :vector t
               :forward-conditional 'i1-f0
               :exclude-forward-backward t
               )

  (conditional 'f1-i1
               :conditions '((f1-s*abduct (arg (f)))
                             (f1-s s (arg (f))))
               :vector t
               :forward-conditional 'i1-f1
               :exclude-forward-backward t
               )

  (conditional 'f1-i2
               :conditions '((f1-s*abduct (arg (f)))
                             (f1-s s (arg (f))))
               :vector t
               :forward-conditional 'i2-f1
               :exclude-forward-backward t
               )
  t)

; Train cm over multiple examples
(defun train-cm (cycles)
  (cm-init)
  (let ((plv (vector
              '(perceive '((i0 .1 (arg 0)) (i1 -.3 (arg 0)) (i2 .2 (arg 0))))
              '(perceive '((i0 -.2 (arg 0)) (i1 .1 (arg 0)) (i2 .3 (arg 0))))
              '(perceive '((i0 -.5 (arg 0)) (i1 .6 (arg 0)) (i2 -.3 (arg 0))))
              '(perceive '((i0 -.7 (arg 0)) (i1 -.5 (arg 0)) (i2 -.8 (arg 0))))
              '(perceive '((i0 .8 (arg 0)) (i1 .6 (arg 0)) (i2 .2 (arg 0))))
              '(perceive '((i0 -.9 (arg 0)) (i1 .9 (arg 0)) (i2 -.9 (arg 0))))
              '(perceive '((i0 .9 (arg 0)) (i1 -.9 (arg 0)) (i2 .9 (arg 0))))
              ))
        (elv (vector
              '(evidence '((o*goal 0.5431989 (arg 0))))
              '(evidence '((o*goal 0.5350067 (arg 0))))
              '(evidence '((o*goal 0.48280898 (arg 0))))
              '(evidence '((o*goal 0.533143 (arg 0))))
              '(evidence '((o*goal 0.5392927 (arg 0))))
              '(evidence '((o*goal 0.5126035 (arg 0))))
              '(evidence '((o*goal 0.5623176 (arg 0))))
              ))
        )
    (setq trace-decisions nil)
    (setq trace-performance nil)
    (dotimes (i (1- cycles))
      (dotimes (i (length plv))
        (setq perceive-list (list (aref plv i)))
        (eval (aref elv i)) ; Set output goal
        (d 1)
        )
      )
;    (setq trace-gdl t)
    (dotimes (i (length plv))
      (setq perceive-list (list (aref plv i))) ; Initiate input
      (eval (aref elv i)) ; Set output goal
      (d 1)
      (format t "~&~%~%------------------")
      (format t "~&Input:")
      (pwmb 'i0 t) (pwmb 'i1 t) (pwmb 'i2 t)
      (format t "~&~%Desired output:")
      (pwmb 'o*goal t)
      (format t "~&~%Computed output:")
      (pwmb 'o t)
      (format t "~&~%Output error derivative:")
      (pwmb 'o*difference-d t)
      (format t "~&------------------")
      )
    )
  t)





